#PYTHON_ARGCOMPLETE_OK
#general imports
import os
import sys
import random
import numpy as np

import argparse, argcomplete

#custom import
from schiffli_control import GameControl
from schiffli_util import GameConfig

parser=argparse.ArgumentParser()

parser.add_argument("nplayers", help="The number of players, up to 4 are possible", type=int)
parser.add_argument("--nogui", help="Set this if no GUI should be displayed", action="store_true")
parser.add_argument("--noanimation", help="Set this to supress all animations", action="store_true")
parser.add_argument("--nocache", help="Set this to force the program to rescale all images, ignoring recently cached images", action="store_true")
parser.add_argument("--displayfieldnr", help="Use this to display field numbers as labels. Usefull for debugging", action="store_true")
parser.add_argument("--seed", help="The random seed to be used", type=int, default=-1)
parser.add_argument("--devmode", help="This enables developer mode. It allows to call funtions of control manually. Also enables undo over random states.", action="store_true")

argcomplete.autocomplete(parser)
args=parser.parse_args()

game_config = GameConfig()
game_config.nplayers = args.nplayers
game_config.devmode = args.devmode

gui = not args.nogui
animation = not args.noanimation
cache=not args.nocache
display_field_nr = args.displayfieldnr

#not working
if args.seed!=-1:
    random.seed(args.seed)
    np.random.seed(args.seed)


#next line is needed cause gui for some reason cares about sys.argv and will complain otherwise
sys.argv=sys.argv[:1]

#here comes the game logic definitions
#initialize the master
master = GameControl(game_config)

#TODO working directory is broken as this moved (change this maybe)
#NOTE: This is for running on a single computer, so we only start one gui and register it for all players!
if gui:
    from schiffli_gui import MainWindow
    from schiffli_util.tkimports import tk
    #TODO maybe have MainWindow start app?
    app=tk.Tk()
    #TODO issue is that mainwindow already needs players to be initialized, at the same time newGame needs observers already 
    mainwindow = MainWindow(master, app, useanimation = animation, usecache = cache, displayfieldnumber=display_field_nr, responsiblePlayers = master._players)
    master.newGame()
    #here we are ready (prevents exit)
    app.mainloop()
else:
    print("Implementation without GUI incomplete (TODO)")
    sys.exit(0)
