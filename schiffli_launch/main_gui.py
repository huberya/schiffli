#PYTHON_ARGCOMPLETE_OK
#general imports
import os
from tkinter import Tk
from typing import Any, Callable, Optional, Tuple
from schiffli_util.tkimports import tk
import sys
import random
import numpy as np
from PIL import Image
import requests
from requests.auth import HTTPBasicAuth

#custom import
from schiffli_control.control import GameControl
from schiffli_util.util import GameConfig, point_to_pixel
from schiffli_gui import MainWindow
from schiffli_web.client import ControlClientInterface
from schiffli_web.client.functions import InvalidResponse, check_request_response

#TODO prevent loading if window just gets closed
def createAppAndStartGame():
    init_app = InitApp()
    init_app.game_start_func()

class InitApp:
    def __init__(self) -> None:
        self.game_config = GameConfig()
        #default gui values
        self.animation = True #False #TODO setting
        self.cache = True
        self.display_field_nr = False

        self.init_app = tk.Tk()
        self.windowxsize = 0.4*self.init_app.winfo_screenwidth()
        self.windowysize = 0.6*self.init_app.winfo_screenheight()
        self.init_app.geometry('%dx%d+%d+%d' %
                                    (self.windowxsize, self.windowysize, 0, 0))

        self.game_start_func: Optional[Callable[[], Any]] = None

        #buttons
        btn_offset = point_to_pixel(28)
        btn = tk.Button(self.init_app, width=26, height=2, command=self.startLocalGame, text="Lokales Spiel starten", font=14)
        btn.place(x=0.5*self.windowxsize, y=60, in_=self.init_app, anchor=tk.CENTER)
        btn = tk.Button(self.init_app, width=26, height=2, command=self.loadLocalGame, text="Lokales Spiel laden", font=14)
        btn.place(x=0.5*self.windowxsize, y=60+btn_offset, in_=self.init_app, anchor=tk.CENTER)
        btn = tk.Button(self.init_app, width=26, height=2, command=self.startOnlineGame, text="Multiplayer Spiel starten", font=14)
        btn.place(x=0.5*self.windowxsize, y=60+2*btn_offset, in_=self.init_app, anchor=tk.CENTER)
        btn = tk.Button(self.init_app, width=26, height=2, command=self.joinOnlineGame, text="Multiplayer Spiel beitrete", font=14)
        btn.place(x=0.5*self.windowxsize, y=60+3*btn_offset, in_=self.init_app, anchor=tk.CENTER)
        btn = tk.Button(self.init_app, width=26, height=2, command=self.openOption, text="Optionen", font=14)
        btn.place(x=0.5*self.windowxsize, y=60+4*btn_offset, in_=self.init_app, anchor=tk.CENTER)
        self.init_app.mainloop()

    def startLocalGame(self) -> None:
        def startGame():
            app, _ = self._startLocalControl()
            #here we are ready (prevents exit)
            app.mainloop()
        self.init_app.destroy()
        self.game_start_func = startGame

    def loadLocalGame(self) -> None:
        filename = tk.filedialog.askopenfilename()
        if filename == '':
            return
        def startGame():
            app, master = self._startLocalControl()
            master.load(filename)
            #here we are ready (prevents exit)
            app.mainloop()
        self.init_app.destroy()
        self.game_start_func = startGame

    def _onlineGameTemplate(self, new_game = False):
        login = tk.Toplevel()
        login.geometry('%dx%d+%d+%d' % (self.windowxsize, self.windowysize, 0, 0))

        tb_offset = point_to_pixel(14)
        username_l = tk.Label(login, width=26, font=tk.font.Font(size=14), text="Benutzername")
        username_l.place(x=0.5*self.windowxsize, y=60, in_=login, anchor=tk.CENTER)
        username_tk = tk.Entry(login, width=26, font=tk.font.Font(size=14))
        username_tk.place(x=0.5*self.windowxsize, y=60+1*tb_offset, in_=login, anchor=tk.CENTER)

        username_l = tk.Label(login, width=26, font=tk.font.Font(size=14), text="Passwort")
        username_l.place(x=0.5*self.windowxsize, y=60+2*tb_offset, in_=login, anchor=tk.CENTER)
        password_tk = tk.Entry(login, width=26, font=tk.font.Font(size=14))
        password_tk.place(x=0.5*self.windowxsize, y=60+3*tb_offset, in_=login, anchor=tk.CENTER)


        username_l = tk.Label(login, width=26, font=tk.font.Font(size=14), text="Spielname")
        username_l.place(x=0.5*self.windowxsize, y=60+4*tb_offset, in_=login, anchor=tk.CENTER)
        game_name_tk = tk.Entry(login, width=26, font=tk.font.Font(size=14))
        game_name_tk.place(x=0.5*self.windowxsize, y=60+5*tb_offset, in_=login, anchor=tk.CENTER)
        
        def checkLoginAndContinue():
            self.game_public_key = game_name_tk.get()
            self.username = username_tk.get()
            self.password = password_tk.get()
            #TODO check request response & catch
            try:
                check_request_response(requests.post("http://localhost:8000/rest/login_user/", data={"username": self.username, "password": self.password}, auth=HTTPBasicAuth(self.username, self.password)))
                if new_game:
                    if requests.get(f"http://localhost:8000/rest/game/{self.game_public_key}/", auth=HTTPBasicAuth(self.username, self.password)).status_code != 404:
                        return
                else:
                    check_request_response(requests.get(f"http://localhost:8000/rest/game/{self.game_public_key}/", auth=HTTPBasicAuth(self.username, self.password)))
                login.destroy()
            except InvalidResponse:
                return

        btn = tk.Button(login, width=26, height=2, command=checkLoginAndContinue, text="Start", font=14)
        btn.place(x=0.5*self.windowxsize, y=60+7*tb_offset, in_=login, anchor=tk.CENTER)
        self.init_app.wait_window(login)
    
    def startOnlineGame(self) -> None:
        self._onlineGameTemplate(True)

        def startGame():
            app, master = self._startOnlineControl(self.game_public_key, self.username, self.password)
            master.save(new_game=True)
            #here we are ready (prevents exit)
            app.mainloop()
        self.init_app.destroy()
        self.game_start_func = startGame

    def joinOnlineGame(self) -> None:
        self._onlineGameTemplate(False)

        def startGame():
            app, master = self._startOnlineControl(self.game_public_key, self.username, self.password)
            master.load()
            #here we are ready (prevents exit)
            app.mainloop()
        self.init_app.destroy()
        self.game_start_func = startGame


    def openOption(self) -> None:
        print("Not implemented yet (TODO)")
        #TODO create popupwindow where options can be set (number of players, type of players etc.)

    def _startLocalControl(self) -> Tuple[Tk, GameControl]:
        #TODO I think something with oberservers is wrong here
        #initialize the master
        master=GameControl(self.game_config)
        #TODO maybe have MainWindow start app?
        app=tk.Tk()
        mainwindow = MainWindow(master, app, useanimation = self.animation, usecache = self.cache, displayfieldnumber=self.display_field_nr, responsiblePlayers = master._players)
        master.newGame()
        return app, master

    def _startOnlineControl(self, game_public_key, username, password) -> Tuple[Tk, GameControl]:
        #TODO switch observers
        master=ControlClientInterface(game_public_key, username, password, self.game_config)
        #TODO maybe have MainWindow start app?
        app=tk.Tk()
        #TODO this will have to setup multiple observers depending on config (which is not flexible enough yet either)
        mainwindow = MainWindow(master, app, useanimation = self.animation, usecache = self.cache, displayfieldnumber=self.display_field_nr, responsiblePlayers = master._players)
        master.newGame()
        return app, master

if __name__ == "__main__":
    #next line is needed cause gui for some reason cares about sys.argv and will complain otherwise
    sys.argv=sys.argv[:1]

    createAppAndStartGame()
