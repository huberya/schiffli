# Schiffli

implementation attempt of schiffli spiel

# Installation
## For Ubuntu
Clone or download the repository
run the install script: 'source install_ubuntu.sh'


# Running
## For Ubuntu
Switch to the repositories main folder (i.e. where this file is)
activate the virtual environment: 'source ../virtualenv/bin/activate
Run main.py: python main.py arguments

# Dependencies
numpy
tkinter
pillow
Shapely

# Structure
main.py is the main executeable containing the gui interface
control.py contains the main game logic
the remaining files implement a specific part of the game


# Coding Guidelines
    -Functions that pass and object, like setOwner, (almost) ALWAYS pass the uuid of the object and not the object itself
    -Call to parent classes should be made explicitly  (i.e. not using super())
    -Functions should use lowerCaseCamelCase
    -Variables should be all snake_case
    -member variables should be considered private when starting with _
    -generally member variables should be declared private whenever possible
    -To indicate failure/success of a function dont return a bool, but rather throw exceptions
    -Actions should be noexcept