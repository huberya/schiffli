import unittest
import random

from typing import cast
from schiffli_test.test_util import startNewTest, test_game_config, TestObserver
from schiffli_util import allrtypes, startressources, verkaufvalues, ankaufvalues
from schiffli_control import Player
from schiffli_control.control import GameControl

class RessourceHandlingTest(unittest.TestCase):
    def testAddAndRemoveRessource(self):
        startNewTest("test add and remove ressource")
        master=GameControl(test_game_config)
        #add testobserver for printing
        test_observer = TestObserver(master, master._players[0])
        master.newGame()
        player = master.getPlayers()[0]
        playerobj = cast(Player, master.getObjectByUuid(player))
        for r in allrtypes:
            #precondition is == startressources (if key else 0)
            if r in startressources:
                ninit = 1
            else:
                ninit = 0
            self.assertEqual(playerobj.getResAmount(r), ninit)
            #add n (random)
            n = random.randint(1, 10)
            master.addRessourcesTo(r, player, num = n)
            #postcondition is += n
            self.assertEqual(playerobj.getResAmount(r), ninit+n)
            #remove m<n (random)
            m = random.randint(1, n)
            master.removeRessourcesFrom(r, player, num=m)
            #postcondition is += n-m
            self.assertEqual(playerobj.getResAmount(r), ninit+n-m)


    def testBuyRessource(self):
        startNewTest("test buy ressource")
        master=GameControl(test_game_config)
        #add testobserver for printing
        test_observer = TestObserver(master, master._players[0])
        master.newGame()
        playerobj = cast(Player, master.getObjectByUuid(master.getPlayers()[0]))
        self.assertEqual(playerobj.getTmpPoints("ressource"), 0)
        #add points so we can buy something
        initpoints = 500
        playerobj.addTmpPoints("ressource", initpoints)
        self.assertEqual(playerobj.getTmpPoints("ressource"), initpoints)
        totalcost = 0
        for r in allrtypes:
            #precondition is == startressources (if key else 0) and 0 ressource points
            if r in startressources:
                ninit = 1
            else:
                ninit = 0
            self.assertEqual(playerobj.getResAmount(r), ninit)
            #buy one ressource
            master.buyRessource(r)
            #post condition is +1 res and - cost tmpoints
            totalcost += verkaufvalues[r]
            self.assertEqual(playerobj.getResAmount(r), ninit + 1)
            self.assertEqual(playerobj.getTmpPoints("ressource"), initpoints - totalcost)

    def testSellRessource(self):
        startNewTest("test sell ressource")
        master=GameControl(test_game_config)
        #add testobserver for printing
        test_observer = TestObserver(master, master._players[0])
        master.newGame()
        playerobj = cast(Player, master.getObjectByUuid(master.getPlayers()[0]))
        self.assertEqual(playerobj.getTmpPoints("ressource"), 0)
        totalcost = 0
        for r in allrtypes:
            #precondition is == startressources (if key else 0) and 0 ressource points
            if r in startressources:
                ninit = 1
            else:
                ninit = 0
            self.assertEqual(playerobj.getResAmount(r), ninit)
            #add ressource and sell it
            playerobj.addRessource(r, 1)
            master.sellRessource(r)
            #post condition is +1 res and - cost tmpoints
            totalcost += ankaufvalues[r]
            self.assertEqual(playerobj.getResAmount(r), ninit)
            self.assertEqual(playerobj.getTmpPoints("ressource"), totalcost)
