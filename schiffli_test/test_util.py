import random

from schiffli_control import GameControlInterface, AbstractAction, StateObserverI
from schiffli_control.util import GameConfig
from schiffli_control.control import GameControl
from schiffli_util import bcolors, InvalidConfiguration

from typing import List, Any, Optional

#TODO imports

test_game_config = GameConfig()
test_game_config.nplayers = 1
test_game_config.devmode = False

def initializeNewTestGame(nplayers = 4):
    master = GameControl(nplayers)
    #Do some initialization stuff (and probably should validate)

    return master

def startNewTest(name: str):
    print("")
    print("-----------------------------------------------------------")
    print("Starting test "+name)
    print("-----------------------------------------------------------")

class TestObserver(StateObserverI):
    def __init__(self, control: GameControl, responsiblePlayer: str, output_info: bool = True):
        self._control = control
        self._responsiblePlayer = responsiblePlayer
        self._control.addStateObserver(self)
        self.last_selected = None
        self.last_init = None
        self._output_info = output_info

    def updateState(self, action: Optional[AbstractAction] = None, actions: Optional[List[AbstractAction]] = None, new_game: bool = False):
        if not (action is None or actions is None):
            raise InvalidConfiguration
        elif not action is None:
            if action.getType() == "init":
                self.last_init = action.getArgs()["uuid"]
        elif not actions is None:
            for action in actions:
                if action.getType() == "init":
                    self.last_init = action.getArgs()["uuid"]

    #TODO implement more as needed
    def requestAdditionalArgument(self, options: List[Any]) -> Any:
        r = random.randint(0, len(options) -1)
        self.last_selected = options[r]
        return options[r]

    #required for testcases
    def getLastGivenArgument(self) -> Any:
        return self.last_selected

    def getResponsiblePlayers(self) -> List[str]:
        return [self._responsiblePlayer]

    def printLog(self, text: str) -> None:
        #TODO fix this (idk if we want to print this, probably rather write to file)
        print("LOG: " + text)

    def printInfo(self, text: str) -> None:
        if self._output_info:
            print(bcolors.OKGREEN + text + bcolors.ENDC)

    def printWarning(self, text: str) -> None:
        print(bcolors.WARNING + text + bcolors.ENDC)

    def printError(self, text: str) -> None:
        print(bcolors.FAIL + text + bcolors.ENDC)

    def printDev(self, text: str) -> None:
        print(bcolors.OKBLUE + text + bcolors.ENDC)