import unittest
import random

from typing import cast
from schiffli_test.test_util import startNewTest, TestObserver, test_game_config
from schiffli_control import Player
from schiffli_control.control import GameControl


class EffectsTests(unittest.TestCase):
    startNewTest("effects")
    master = GameControl(test_game_config)
    # add testobserver for printing
    test_observer = TestObserver(master, master._players[0])
    master.newGame()
    playerobj = master.getActivePlayerObj()
    # TODO test effects are doing what they are supposed to
    #loop add effect to (maybe random) with target?? and check effect
