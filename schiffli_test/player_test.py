import unittest

from typing import cast
from schiffli_test.test_util import startNewTest, test_game_config, TestObserver
from schiffli_control import Player
from schiffli_control.control import GameControl

class SimplePlayerTest(unittest.TestCase):
    def testSinglePlayerCreation(self):
        startNewTest("single player creation")
        master=GameControl(test_game_config)
        self.assertEqual(len(master._players), 1)
        #add testobserver for printing
        test_observer = TestObserver(master, master._players[0])
        master.newGame()
        self.assertEqual(len(master._players), 1)
        playerobj = cast(Player, master.getObjectByUuid(master._players[0]))

        self.assertEqual(len(playerobj.getFleets()), 3)
        #TODO more (follow very simple creation stuff like placing settlements and make sure behaves as expected)
