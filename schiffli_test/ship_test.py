#test creation  of ships as well as their movement
import unittest
import random

from typing import cast
from schiffli_test.test_util import startNewTest, test_game_config, TestObserver
from schiffli_util import shipcost, battleshiptypes, tradingshiptypes, allrtypes, startressources, globalworld, buildtime, shipspeed, NotEnoughRessources, InvalidTarget, TemporaryInvalidAction, shipcapacity, NotEnoughSpace
from schiffli_control import Player
from schiffli_mapobjects import AbstractMapObject, TradingShip
from schiffli_control.control import GameControl

class ShipTests(unittest.TestCase):
    def testBuyShipInstant(self):
        startNewTest("buy ship instant")
        master=GameControl(test_game_config)
        #add testobserver for printing
        test_observer = TestObserver(master, master._players[0])
        master.newGame()

        self.assertRaises(NotEnoughRessources, master.buyShip, tradingshiptypes[1], tags=["tradingship", "clickable"])
        #add ressources
        playerobj = cast(Player, master.getObjectByUuid(master.getPlayers()[0]))
        nressources = {}
        for r in allrtypes:
            #precondition is == startressources (if key else 0) and 0 ressource points
            if r in startressources:
                nressources[r] = 1
            else:
                nressources[r] = 0
            #add ressource so we can buy stuff afterwards
            playerobj.addRessource(r, 100)
            nressources[r]+=100
        for k,v in shipcost.items():
            buildtime_cache = buildtime[k]
            buildtime[k] = 0
            #buy ship, check ownership, location and remaining ressources
            if k in tradingshiptypes:
                master.buyShip(k, tags=["tradingship", "clickable"])
            elif k in battleshiptypes:
                master.buyShip(k, tags=["battleship", "clickable"])
            else:
                #TODO better solution here
                self.assertTrue(False)
            #check for ownership
            newship = cast(AbstractMapObject, master._taggedobjects[-1])
            self.assertEqual(newship.getOwner(), playerobj.getUuid())
            if k in tradingshiptypes:
                self.assertEqual(newship.getPos(), globalworld.getHomeTown(playerobj))
                self.assertEqual(newship.getUuid(), playerobj.getTradingShips()[-1])
            elif k in battleshiptypes:
                self.assertEqual(newship.getPos(), -1)
                self.assertEqual(newship.getUuid(), playerobj.getBattleShips()[-1])
            buildtime[k] = buildtime_cache

    def testBuyShipDelayed(self):
        startNewTest("buy ship delayed")
        master=GameControl(test_game_config)
        #add testobserver for printing
        test_observer = TestObserver(master, master._players[0])
        master.newGame()

        self.assertRaises(NotEnoughRessources, master.buyShip, tradingshiptypes[1], tags=["tradingship", "clickable"])
        #add ressources
        playerobj = cast(Player, master.getObjectByUuid(master.getPlayers()[0]))
        nressources = {}
        for r in allrtypes:
            #precondition is == startressources (if key else 0) and 0 ressource points
            if r in startressources:
                nressources[r] = 1
            else:
                nressources[r] = 0
            #add ressource so we can buy stuff afterwards
            playerobj.addRessource(r, 100)
            nressources[r]+=100
        for k,v in shipcost.items():
            if k in tradingshiptypes:
                master.buyShip(k, tags=["tradingship", "clickable"])
            elif k in battleshiptypes:
                master.buyShip(k, tags=["battleship", "clickable"])
            else:
                self.assertTrue(False)
            #check for ownership
            newship = cast(AbstractMapObject, master._taggedobjects[-1])
            self.assertEqual(newship.getOwner(), playerobj.getUuid())
            self.assertFalse(newship.isAvailable())
            if k in tradingshiptypes:
                self.assertEqual(newship.getUuid(), playerobj.getTradingShips()[-1])
            elif k in battleshiptypes:
                self.assertEqual(newship.getUuid(), playerobj.getBattleShips()[-1])
            #make sure available changes after buildtime rounds
            for i in range(buildtime[k]):
                self.assertFalse(newship.isAvailable())
                master.nextTurn()
            self.assertTrue(newship.isAvailable()) #error happens here

    def testChargeAndMoveShip(self):
        startNewTest("charge and move ship")
        master=GameControl(test_game_config)
        #add testobserver for printing
        test_observer = TestObserver(master, master._players[0])
        master.newGame()

        #add ressources
        playerobj = cast(Player, master.getObjectByUuid(master.getPlayers()[0]))
        nressources = {}
        for r in allrtypes:
            #precondition is == startressources (if key else 0) and 0 ressource points
            if r in startressources:
                nressources[r] = 1
            else:
                nressources[r] = 0
            #add ressource so we can buy stuff afterwards
            playerobj.addRessource(r, 100)
            nressources[r]+=100
        #buy a ship
        for s in tradingshiptypes:
            master.buyShip(s, tags=["tradingship", "clickable"])
            newship = cast(TradingShip, master._taggedobjects[-1])
            assert(isinstance(newship, TradingShip))

            self.assertRaises(TemporaryInvalidAction, master.moveRessourceFromTo, allrtypes[0], playerobj.getUuid(), newship.getUuid())
            self.assertRaises(TemporaryInvalidAction, master.moveMapObject, newship.getUuid(), target=newship.getPos()-1)
            #should not have any valid targets yet
            for p in newship.getValidTargets():
                self.assertTrue(False)

            #wait until available (buildtime)
            for i in range(buildtime[s]):
                master.nextTurn()
            self.assertTrue(newship.isAvailable())
            self.assertRaises(InvalidTarget, master.moveMapObject, newship.getUuid(), target=newship.getPos()-8)
            #should still be invalid, as we have to move first!
            self.assertRaises(TemporaryInvalidAction, master.moveRessourceFromTo, allrtypes[0], playerobj.getUuid(), newship.getUuid())
            #move once so we can trade
            newship.computeValidTargets()
            while newship.getCurrRange() > 0:
                target = newship.getValidTargets()[0]
                master.moveMapObject(newship.getUuid(), target)
                self.assertEqual(newship.getPos(), target)
            #try to charge -> success
            #move mapobject seems somehow broken...
            for rtype in allrtypes:
                self.assertRaises(NotEnoughRessources, master.moveRessourceFromTo, rtype, newship.getUuid(), playerobj.getUuid())
                for i in range(shipcapacity[s]):
                    master.moveRessourceFromTo(rtype, playerobj.getUuid(), newship.getUuid())
                self.assertRaises(NotEnoughSpace, master.moveRessourceFromTo, rtype, playerobj.getUuid(), newship.getUuid())
                #TODO check for enough ressources on ship

                #empty again for next iteration
                for i in range(shipcapacity[s]):
                    master.moveRessourceFromTo(rtype, newship.getUuid(), playerobj.getUuid())
            while not newship.getCurrTradingTown() is None:
                master.completeCurrTradingTown(newship.getUuid())
            #TODO also try to charge when not at harbour

            #next turn so we can move again
            master.nextTurn()
            #test movement (range etc)
            nturntries = 50 #having a larger number here also checks the connectivity a bit (at least for missing entries)
            for ntry in range(nturntries):
                #TODO check not available target
                newship.computeValidTargets()
                #TODO somehow we do not get valid targets
                while newship.getCurrRange() > 0:
                    #select target randomly from available
                    p = random.randint(0, len(newship.getValidTargets())-1)
                    target = newship.getValidTargets()[p] #TODO random selection
                    master.moveMapObject(newship.getUuid(), target)
                    self.assertEqual(newship.getPos(), target)
                #issue with this is that ships can get boost (need to somehow check for that)
                #for p in newship.getValidTargets():
                #    self.assertTrue(False)
                #TODO think about how to/if its smarter for getvalidtargets to raise temporaryinvalid if maximal movement is reached
                #self.assertRaises(InvalidTarget, master.moveMapObject, newship.getUuid(), newship.getPos()-1) same as above applies here
                if not newship.getCurrTradingTown() is None:
                    self.assertRaises(TemporaryInvalidAction, master.nextTurn)
                #TODO this is super ugly and should somehow be automatic/enforced
                while not newship.getCurrTradingTown() is None:
                    master.completeCurrTradingTown(newship.getUuid())
                master.nextTurn()