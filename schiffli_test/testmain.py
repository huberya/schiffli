import unittest

import argparse
import argcomplete

#import all testcases
from schiffli_test import *

if __name__ == '__main__':
    parser=argparse.ArgumentParser()

    parser.add_argument("-v", "--verbose", help="Output more verbose information while testing", action="store_true")
    
    argcomplete.autocomplete(parser)
    args=parser.parse_args()
    
    #DO SOMETHING WITH VERBOSITY FLAG s.t. tests have access to it

    unittest.main()