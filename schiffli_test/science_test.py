import unittest
import random

from typing import cast
from schiffli_test.test_util import startNewTest, TestObserver, test_game_config
from schiffli_util import allrtypes, startressources, verkaufvalues, NotEnoughRessources, TemporaryInvalidAction, sciencetier_from_cost
from schiffli_control import Player
from schiffli_control.control import GameControl

class ScienceTests(unittest.TestCase):
    def testBuyScience(self):
        startNewTest("buy science")
        master=GameControl(test_game_config)
        #add testobserver for printing
        test_observer = TestObserver(master, master._players[0])
        master.newGame()
        playerobj = master.getActivePlayerObj()
        self.assertRaises(NotEnoughRessources, master.buyScience, "T1")
        master.addTmpPoints(master.getActivePlayer(), "science", 50)
        master.buyScience("T1")
        #check for sciencecard
        self.assertEqual(len(playerobj.getScienceCards()), 1)
        self.assertEqual(master.getObjectByUuid(playerobj.getScienceCards()[-1]).getTier(), "T1")
        self.assertFalse(playerobj.canBuyScience())
        self.assertRaises(TemporaryInvalidAction, master.buyScience, "T1") #should not be allowed to buy 2 in one round.
        #next turn should allow us to buy again
        master.nextTurn()
        #note points should be deleted after turn!
        self.assertRaises(NotEnoughRessources, master.buyScience, "T2")
        master.addTmpPoints(master.getActivePlayer(), "science", 25)
        master.buyScience("T2")
        self.assertEqual(len(playerobj.getScienceCards()), 2)
        self.assertEqual(master.getObjectByUuid(playerobj.getScienceCards()[-1]).getTier(), "T2")
        #test has one more sciencecard
        #next turn should allow us to buy again
        master.nextTurn()
        master.addTmpPoints(master.getActivePlayer(), "science", 50)
        master.buyScience("T3")
        self.assertEqual(len(playerobj.getScienceCards()), 3)
        self.assertEqual(master.getObjectByUuid(playerobj.getScienceCards()[-1]).getTier(), "T3")



    def testActivateScience(self):
        #TODO deduplicate this with science definitions
        startNewTest("activate science")
        #add points
        master = GameControl(test_game_config)
        #add testobserver as we need to request additional arguments
        test_observer = TestObserver(master, master._players[0])
        master.newGame()
        player_obj = master.getActivePlayerObj()
        master.addTmpPoints(master.getActivePlayer(), "science", 99999)
        #hack because we dont care about buy limitations here
        player_obj._tmppoints["nBuyableScienceCards"] = 9999
        #try randomized as buy process is random, repeat to get all of them
        ntries = 200
        for i in range(ntries):
            amount = random.randint(1,10)
            for j in range(amount):
                science_cost = list(sciencetier_from_cost.keys())[j%3]
                master.buyScience(sciencetier_from_cost[science_cost])
            self.assertEqual(len(player_obj.getScienceCards()), amount)
            for j in range(amount):
                science_card_obj = master.getObjectByUuid(player_obj.getScienceCards()[0])
                old_science_points = player_obj.getTmpPoints("science")
                old_nbuyable_sc = player_obj.getTmpPoints("nBuyableScienceCards")
                old_res_points = player_obj.getTmpPoints("ressource")
                old_politics_points = player_obj.getTmpPoints("politics")
                master.activateScienceCard(player_obj.getScienceCards()[0])
                #TODO move value definitions to somewhere outside instead of copypaste (dict or something)
                if science_card_obj._type=="W1":
                    self.assertEqual(master.getObjectByUuid(test_observer.last_init).getType(), "windrad")
                    self.assertTrue(test_observer.last_init in player_obj.getEffects(inheritableonly=True))
                    self.assertTrue(test_observer.last_init in player_obj.getEffects(inheritableonly=False))
                    #for inheritance
                    self.assertTrue(test_observer.last_init in master.getObjectByUuid(player_obj.getTradingShips()[0]).getEffects(inheritableonly=True))
                    #TODO check duration + effect values based on tier
                elif science_card_obj._type=="W2":
                    self.assertEqual(master.getObjectByUuid(test_observer.last_init).getType(), "protection")
                    if science_card_obj.getTier()=="T3":
                        self.assertTrue(test_observer.last_init in player_obj.getEffects(inheritableonly=True))
                        self.assertTrue(test_observer.last_init in player_obj.getEffects(inheritableonly=False))
                        #for inheritance
                        self.assertTrue(test_observer.last_init in master.getObjectByUuid(player_obj.getTradingShips()[0]).getEffects(inheritableonly=True))
                    else:
                        selected_uuid = test_observer.getLastGivenArgument()
                        self.assertTrue(test_observer.last_init in master.getObjectByUuid(selected_uuid).getEffects(inheritableonly=False))
                    #TODO check duration + effect values based on tier
                elif science_card_obj._type=="W4":
                    #TODO to be implemented
                    pass
                    #TODO check duration + effect values based on tier
                elif science_card_obj._type=="W5":
                    self.assertEqual(master.getObjectByUuid(test_observer.last_init).getType(), "feinmechanik")
                    self.assertTrue(test_observer.last_init in player_obj.getEffects(inheritableonly=True))
                    self.assertTrue(test_observer.last_init in player_obj.getEffects(inheritableonly=False))
                    #for inheritance
                    self.assertTrue(test_observer.last_init in master.getObjectByUuid(player_obj.getTradingShips()[0]).getEffects(inheritableonly=True))
                    #TODO check duration + effect values based on tier
                elif science_card_obj._type=="W6":
                    self.assertEqual(master.getObjectByUuid(test_observer.last_init).getType(), "seuche")
                    selected_uuid = test_observer.getLastGivenArgument()
                    self.assertTrue(test_observer.last_init in master.getObjectByUuid(selected_uuid).getEffects(inheritableonly=False))
                    #TODO check duration + effect values based on tier
                elif science_card_obj._type=="W7":
                    if science_card_obj.getTier()=="T1":
                        point_per_tier = 5
                    elif science_card_obj.getTier()=="T2":
                        point_per_tier = 10
                    elif science_card_obj.getTier()=="T3":
                        point_per_tier = 20
                    self.assertEqual(player_obj.getTmpPoints("science"), old_science_points + point_per_tier)
                    self.assertEqual(player_obj.getTmpPoints("nBuyableScienceCards"), old_nbuyable_sc + 1)
                elif science_card_obj._type=="W8":
                    if science_card_obj.getTier()=="T1":
                        point_per_tier = 8
                    elif science_card_obj.getTier()=="T2":
                        point_per_tier = 12
                    elif science_card_obj.getTier()=="T3":
                        point_per_tier = 16
                    self.assertEqual(player_obj.getTmpPoints("ressource"), old_res_points + point_per_tier)
                elif science_card_obj._type=="W9":
                    self.assertEqual(master.getObjectByUuid(test_observer.last_init).getType(), "defense")
                    if science_card_obj.getTier()=="T1":
                        selected_uuid = test_observer.getLastGivenArgument()
                        self.assertTrue(test_observer.last_init in master.getObjectByUuid(selected_uuid).getEffects(inheritableonly=False))
                        self.assertTrue(test_observer.last_init in master.getObjectByUuid(selected_uuid).getEffects(inheritableonly=True))
                        #TODO check duration + effect values based on tier
                    #TODO check inheritance
                    else:
                        #TODO based on tier this should apply to either target only or to player
                        self.assertTrue(test_observer.last_init in player_obj.getEffects(inheritableonly=False))
                        self.assertTrue(test_observer.last_init in player_obj.getEffects(inheritableonly=True))
                        #TODO check duration + effect values based on tier
                elif science_card_obj._type=="W10":
                    if science_card_obj.getTier()=="T1":
                        point_per_tier = 2
                    elif science_card_obj.getTier()=="T2":
                        point_per_tier = 4
                    elif science_card_obj.getTier()=="T3":
                        point_per_tier = 6
                    self.assertEqual(player_obj.getTmpPoints("politics"), old_politics_points + point_per_tier)
                elif science_card_obj._type=="W11":
                    #TODO
                    pass
                else:
                    self.assertTrue(False)
            self.assertEqual(len(player_obj.getScienceCards()), 0)
