#I think this is no longer needed

#IDEA: recieves data (as JSON) from players: 
#Data contains: player_id, verification_code, function_name, *args, **kwargs
#There should be exactly ONE interface running on the server that distributes data between 
#all running controls (also it should eventually be used to launch and close running controls)

#important note: All function should NEVER fail independent of input

from typing import List, Dict, Any

from schiffli_control import GameControlInterface


class ControlWebInterface:
    def __init__(self) -> None:
        self.running_control_instances: List[GameControlInterface] = []
        self.player2control:  Dict[str, GameControlInterface] = {}
        self.player2identification: Dict[str, str] = {}

    #TODO return HTTP error codes
    def recieve_data(self, json):
        #verification
        try:
            player_id = json["player_id"]
        except:
            #TODO error logging
            return
        try:
            verification_code = json["verification_code"]
        except:
            #TODO error logging
            return
        try:
            if not self.player2identification[player_id] == verification_code:
                #TODO log invalid credentials
                return
        except:
            #TODO log not registered player
            return
        try:
            corresponding_control = self.player2control[player_id]
        except:
            #TODO log no corresponding control
            return
        try:
            function_name = json["function_name"]
        except:
            #TODO error logging
            return
        try:
            args: List[Any] = json["args"]
        except:
            args: List[Any] = []
        try:
            kwargs: Dict[Any, Any] = json["kwargs"]
        except:
            kwargs: Dict[Any, Any] = {}
        try:
            func = getattr(corresponding_control, function_name)
        except:
            #TODO log not found function
            return
        try:
            response = func(*args, **kwargs)
        except:
            #TODO this is somewhat tricky as response should somehow contain error information as it might be caught by client
            response = []
        return response # TODO pass by HTTP instead of return