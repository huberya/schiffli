from django.apps import AppConfig


class LiveCommunicationAppConfig(AppConfig):
    name = 'live_communication_app'
