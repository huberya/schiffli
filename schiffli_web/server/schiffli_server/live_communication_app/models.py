import yaml

from django.db import models
from django.template import Context, loader
from django.utils import timezone

from django.contrib.auth.models import User
from django.db.models import (Model, TextField, DateTimeField, ForeignKey,
                              CASCADE)

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer

from typing import Dict

from schiffli_control import GameControlInterface

from rest_interface.models import ParticipatingPlayer

   

class FunctionCallModel(Model):
    """
    This class represents a function call of the server (as json).
    Besides the actual data it has a sender, a game and a timestamp

    """

    participating_player = ForeignKey(ParticipatingPlayer, on_delete=CASCADE, verbose_name='participating_player',
                      related_name='from_participating_player', db_index=True)
    timestamp = DateTimeField('timestamp', auto_now_add=True, editable=False,
                              db_index=True)
    update_data = TextField('update_data')

    def __str__(self):
        return str(self.id)


    def notify_ws_clients(self):
        """
        Inform client there is a new message.
        """
        notification = {
            'type': 'recieve_update_message',
            'update_id': '{}'.format(self.id)
        }

        channel_layer = get_channel_layer()
        print("notifying {}".format(self.participating_player.game_id.public_key))

        async_to_sync(channel_layer.group_send)("{}".format(self.participating_player.game_id.public_key), notification)
        print("notified ws clients")

    def save(self, *args, **kwargs):
        """
        Trims white spaces, saves the message and notifies the participating_player via WS
        if the message is new.
        """
        new = self.id
        #Make sure the text is valid json (easiest is just do convert it to dict and back, also standardizes format)
        tmp = yaml.safe_load(self.update_data)
        self.update_data = yaml.safe_dump(tmp)
        super().save(*args, **kwargs)
        if new is None:
            self.notify_ws_clients()

    # Meta
    class Meta:
        app_label = 'live_communication_app'
        verbose_name = 'command'
        verbose_name_plural = 'commands'
        ordering = ('-timestamp',)

        