from . import views

from django.urls import path, include
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from rest_framework.routers import DefaultRouter
from .api import FunctionCallModelViewSet, GameModelViewSet

router = DefaultRouter()
router.register(r'message', FunctionCallModelViewSet, basename='message-api')
router.register(r'game', GameModelViewSet, basename='game-api')


# IMPORTANT: ORDER THEM BY MORE SPECIALIZED
urlpatterns = [
    path(r'<str:game_name>/api/v1/', include(router.urls)),
    path('<str:game_name>/', login_required(
        TemplateView.as_view(template_name='core/chat.html')), name='home'),

]
