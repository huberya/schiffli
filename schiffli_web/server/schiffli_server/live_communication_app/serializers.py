from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework.serializers import ModelSerializer, CharField

from rest_interface.models import ParticipatingPlayer

from .models import FunctionCallModel

class FunctionCallModelSerializer(ModelSerializer):
    participating_player = CharField(source='participating_player.id')

    def create(self, validated_data):
        print(validated_data)
        #realistically we get:
        #player id, game id, auth key
        #for now we just use participating player id
        participating_player = get_object_or_404(
            ParticipatingPlayer, id=validated_data['participating_player']['id'])
        msg = FunctionCallModel(participating_player=participating_player,
                           update_data=validated_data['update_data'])
        msg.save()
        return msg

    class Meta:
        model = FunctionCallModel
        fields = ('id', 'participating_player', 'timestamp', 'update_data')

