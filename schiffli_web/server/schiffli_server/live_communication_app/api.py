from django.db.models import Q
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated

from schiffli_server import settings
from rest_interface.models import Game
from rest_interface.serializers import GameSerializer

from .serializers import FunctionCallModelSerializer
from .models import FunctionCallModel


class CsrfExemptSessionAuthentication(SessionAuthentication):
    """
    SessionAuthentication scheme used by DRF. DRF's SessionAuthentication uses
    Django's session framework for authentication which requires CSRF to be
    checked. In this case we are going to disable CSRF tokens for the API.
    """

    def enforce_csrf(self, request):
        return


class FunctionCallPagination(PageNumberPagination):
    """
    Limit message prefetch to one page.
    """
    page_size = settings.MESSAGES_TO_LOAD


class FunctionCallModelViewSet(ModelViewSet):
    queryset = FunctionCallModel.objects.all()
    serializer_class = FunctionCallModelSerializer
    allowed_methods = ('GET', 'POST', 'HEAD', 'OPTIONS')
    #default is IsAuthenticated which is for now the easiest to do for us
    #w/o this this works in python:
    #requests.post("http://localhost:8000/live/test1/api/v1/message/", auth=HTTPBasicAuth('yannick', '8linkin5'), json={"participati: ng_player":"test1","update_data": json.dumps({"test":1})}).json()
    #with it web posting works
    #authentication_classes = (CsrfExemptSessionAuthentication,)
    pagination_class = FunctionCallPagination

    def list(self, request, *args, **kwargs):
        #print(request.__dict__)
        #self.queryset = self.queryset.filter(Q(recipient=request.user) |
        #                                     Q(user=request.user))
        self.queryset = self.queryset.filter(Q(participating_player__game_id__public_key=kwargs["game_name"])) #filter is very wrong here
        #Do we need this?
        #target = self.request.query_params.get('target', None) (maybe is for self update)
        #if target is not None:
        #    self.queryset = self.queryset.filter(
        #        Q(participating_player_id__username=target) | Q(user=request.user))
            #self.queryset = self.queryset.filter(
            #    Q(recipient=request.user, user__username=target) |
            #    Q(recipient__username=target, user=request.user))
        return super().list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        msg = get_object_or_404(
            self.queryset.filter(Q(participating_player__game_id__public_key=kwargs["game_name"]),
                                 Q(pk=kwargs['pk'])))
        serializer = self.get_serializer(msg)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        print("creating")
        return super().create(request, *args, **kwargs)


class GameModelViewSet(ModelViewSet):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    allowed_methods = ('GET', 'HEAD', 'OPTIONS')
    pagination_class = None  # Get all games

    def list(self, request, *args, **kwargs):
        # Get all games except inactive
        self.queryset = self.queryset.exclude(active=False)
        return super(GameModelViewSet, self).list(request, *args, **kwargs)
