
# chat/consumers.py

from channels.generic.websocket import AsyncWebsocketConsumer
import json


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        #print(self.scope)   
        #TODO here we need authentication somehow like this below
        #user_id = self.scope["session"]["_auth_user_id"]
        self.group_name = "{}".format(self.scope["url_route"]["kwargs"]["game_name"])
        # Join room group
        print("connecting to " +self.group_name + " on channel " + self.channel_name)
        await self.channel_layer.group_add(
            self.group_name,
            self.channel_name
        )

        await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data=None, bytes_data = None):
        print("recieved")
        update_data_json = json.loads(text_data)
        update_data = update_data_json['update_data']
        # Send message to room group
        await self.channel_layer.group_send(
            # self.chat_group_name,
            {
                'type': 'recieve_update_message',
                'update_data': update_data
            }
        )

    async def recieve_update_message(self, event):
        print("recieved message with update id {}".format(event["update_id"]))
        update_id = event['update_id']
        # Send message to WebSocket
        await self.send(text_data=update_id)