from django.contrib import admin

from .models import FunctionCallModel

class FunctionCallModelAdmin(admin.ModelAdmin):
    readonly_fields = ('timestamp',)
    search_fields = ('id', 'participating_player__player_id', 'participating_player__game_id', 'participating_player__id')
    list_display = ('id', 'participating_player', 'timestamp')
    list_display_links = ('id',)
    list_filter = ('participating_player__player_id', 'participating_player__game_id')
    date_hierarchy = 'timestamp'


admin.site.register(FunctionCallModel, FunctionCallModelAdmin)
