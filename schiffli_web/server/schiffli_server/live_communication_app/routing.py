from . import consumers

from django.conf.urls import url

websocket_urlpatterns = [
    url(r'^ws/(?P<game_name>[^/]+)/$', consumers.ChatConsumer),
]
