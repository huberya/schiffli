from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token

from . import views

# IMPORTANT: ORDER THEM BY MORE SPECIALIZED
urlpatterns = [
    # Authentication
    path('api-token-auth/', obtain_auth_token, name='api_token_auth'),
    # User related part
    path('login_user/',
          views.login_user,
          name="login_user"),
    path('user/<str:username>/',
         views.SingleUser.as_view(),
         name="single_user"),
    path('user/',
         views.AllUsers.as_view(),
         name="all_users"),
    # Game related part
    path(
        'game/<str:public_key>/',
        views.SingleGame.as_view(),
        name="single_game"),
    path('game/',
         views.AllGames.as_view(),
         name="all_games"),
    # Participating players related part
    path(
        'players/<str:public_key>/',
        views.AllPlayersOfAGame.as_view(),
        name="all_players_of_a_game"),
     path(
        'update/<int:id>/',
        views.SingleUpdate.as_view(),
        name="single_update"),
]
