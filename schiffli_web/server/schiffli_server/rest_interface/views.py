# -*- coding: utf-8 -*-
from __future__ import unicode_literals

# Create your views here.
from django.contrib.auth import authenticate, login
from django.shortcuts import get_object_or_404
from rest_framework import generics
from django.http import HttpResponse, HttpResponseNotAllowed
from rest_framework.decorators import authentication_classes, api_view

from live_communication_app.models import FunctionCallModel
from live_communication_app.serializers import FunctionCallModelSerializer

from .models import User, Game, ParticipatingPlayer
from .serializers import ProfileSerializer, GameSerializer, ParticipatingPlayerSerializer

#ofc this should not be requiring login (doesnt quite work like that)
@api_view(['GET', 'POST'])
#@authentication_classes([])
def login_user(request):
    user = authenticate(username=request.POST.get('username'),  password=request.POST.get('password'))
    if user:
       login(request, user)
       return HttpResponse("Logged In")
    return HttpResponseNotAllowed("Not Logged In")

class AllUsers(generics.ListCreateAPIView):
    queryset = User.objects.all().order_by('id')
    serializer_class = ProfileSerializer

class SingleUser(generics.RetrieveUpdateAPIView):
    lookup_field = 'username'
    serializer_class = ProfileSerializer
    def get_queryset(self):
        return User.objects.filter(
            username=self.kwargs["username"])

class AllGames(generics.ListCreateAPIView):
    queryset = Game.objects.all().order_by('id')
    serializer_class = GameSerializer

class SingleGame(generics.RetrieveUpdateAPIView):
    lookup_field = 'public_key'
    serializer_class = GameSerializer
    def get_queryset(self):
        return Game.objects.filter(
            public_key=self.kwargs["public_key"])

class AllPlayersOfAGame(generics.ListCreateAPIView):
    lookup_field = 'game_id'
    serializer_class = ParticipatingPlayerSerializer

    def get_queryset(self):
        return ParticipatingPlayer.objects.filter(
            game_id__public_key=self.kwargs["public_key"]).order_by('id')

class SingleUpdate(generics.RetrieveAPIView):
    lookup_field = "id"
    serializer_class = FunctionCallModelSerializer

    def get_queryset(self):
        return FunctionCallModel.objects.filter(id=self.kwargs["id"])
