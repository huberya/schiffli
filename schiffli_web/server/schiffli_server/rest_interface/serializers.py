# serializers.py

from rest_framework import serializers

from .models import Game, User, ParticipatingPlayer


class GameSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = Game
        fields = ('id', 'active', 'public_key', 'game_state')


#TODO check this out
class ProfileSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = User
        fields = ('id', 'username', 'email') # dont show password (no?)


class ParticipatingPlayerSerializer(serializers.ModelSerializer):
    class Meta(object):
        model = ParticipatingPlayer
        fields = ('id', 'game_id', 'player_id')

    #Ideally we dont get the whole gamestate here aswell I think
    def to_representation(self, instance):
        #self.fields['game_id'] =  GameSerializer(many=False, read_only=True)
        #self.fields['player_id'] =  ProfileSerializer(many=False, read_only=True)
        return super().to_representation(instance)
