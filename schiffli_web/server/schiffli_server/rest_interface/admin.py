# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Game, User, ParticipatingPlayer


# Register your models here.
class GameAdmin(admin.ModelAdmin):
    all_rel_fields = ('id', 'public_key', 'active', 'game_state')
    list_display = all_rel_fields
    list_editable = all_rel_fields[1:]
    list_filter = all_rel_fields



class ParticipatingPlayerAdmin(admin.ModelAdmin):
    all_rel_fields = ('id', 'game_id', 'player_id')
    list_display = all_rel_fields
    list_editable = all_rel_fields[1:]
    list_filter = all_rel_fields


admin.site.register(Game, GameAdmin)
admin.site.register(ParticipatingPlayer, ParticipatingPlayerAdmin)
