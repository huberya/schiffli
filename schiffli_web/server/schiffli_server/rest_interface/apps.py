from django.apps import AppConfig


class RestInterfaceConfig(AppConfig):
    name = 'rest_interface'
