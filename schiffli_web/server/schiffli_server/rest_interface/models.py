from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.template import Context, loader
from django.utils import timezone

# Create your models here.
#contains information regarding a (active)game
class Game(models.Model):
    #game config foregin key
    #participating players through inverse
    active = models.BooleanField(default=True)
    public_key = models.CharField(max_length=50, unique=True)
    game_state = models.TextField()


#This is using Django users as we need those for authentication
# class Profile(models.Model):
#     user = models.OneToOneField(User, on_delete=models.CASCADE)
#     firstname = models.CharField(max_length=50, blank=True)
#     lastname = models.CharField(max_length=50, blank=True)

# @receiver(post_save, sender=User)
# def create_user_profile(sender, instance, created, **kwargs):
#     if created:
#         Profile.objects.create(user=instance)

# @receiver(post_save, sender=User)
# def save_user_profile(sender, instance, **kwargs):
#     instance.profile.save()


class ParticipatingPlayer(models.Model):
    game_id = models.ForeignKey(Game, on_delete=models.CASCADE, related_name="participating_players")
    player_id = models.ForeignKey(User, on_delete=models.PROTECT, related_name="in_games")
    class Meta:
        unique_together = ('game_id', 'player_id')
