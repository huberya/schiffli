let chatInput = $('#chat-input');
let chatButton = $('#btn-send');
let gameList = $('#game-list');
let messageList = $('#messages');

function updateGameList() {
    $.getJSON('api/v1/game/', function (data) {
        gameList.children('.game_key').remove();
        for (let i = 0; i < data.length; i++) {
            const userItem = `<a class="list-group-item game_key ${data[i]['public_key']}">${data[i]['public_key']}</a>`;
            $(userItem).appendTo('#game-list');
        }
        gameList.children('.active').removeClass('active');
        gameList.children('.'+gameName).addClass('active');
        $('.game_key').click(function () {
            let selected = event.target;
            window.location.replace('../' + selected.text)
        });
    });
}

function drawMessage(message) {
    let position = 'left';
    const date = new Date(message.timestamp);
    //TODO fix this left right stuff somehow
    //console.log(currentUser)
    if (message.participating_player === currentUser) position = 'right';
    const messageItem = `
            <li class="message ${position}">
                <div class="avatar">${message.participating_player}</div>
                    <div class="text_wrapper">
                        <div class="text">${message.update_data}<br>
                            <span class="small">${date}</span>
                    </div>
                </div>
            </li>`;
    $(messageItem).appendTo('#messages');
}

function getConversation(game_key) {
    $.getJSON(`api/v1/message/?target=${game_key}`, function (data) {
        messageList.children('.message').remove();
        for (let i = data['results'].length - 1; i >= 0; i--) {
            drawMessage(data['results'][i]);
        }
        messageList.animate({scrollTop: messageList.prop('scrollHeight')});
    });

}

function getMessageById(message_id) {
    id = JSON.parse(message_id).message
    $.getJSON(`api/v1/message/${id}/`, function (data) {
        //TODO what is requirement here?
        //if (data.user === currentRecipient ||
        //    (data.recipient === currentRecipient && data.user == currentUser)) {
            drawMessage(data);
        //}
        messageList.animate({scrollTop: messageList.prop('scrollHeight')});
    });
}

//participating player is basically the sender
function sendMessage(participating_player, update_data) {
    $.post('api/v1/message/', {
        participating_player: participating_player,
        update_data: update_data
    }).fail(function () {
        alert('Error! Check console!?!' + participating_player + " " + update_data);
    });
}


function enableInput() {
    chatInput.prop('disabled', false);
    chatButton.prop('disabled', false);
    chatInput.focus();
}

function disableInput() {
    chatInput.prop('disabled', true);
    chatButton.prop('disabled', true);
}

$(document).ready(function () {
    updateGameList();
    if(currentUser=="yannick") {
        enableInput();
    }
    else {
        disableInput();
    }
    getConversation(gameName);

//    let socket = new WebSocket(`ws://127.0.0.1:8000/?session_key=${sessionKey}`);
    var socket = new WebSocket(
        'ws://' + window.location.host +
        '/ws/' + gameName + '/?session_key=${sessionKey}'); //TODO here we somehow need to get room name

    chatInput.keypress(function (e) {
        if (e.keyCode == 13)
            chatButton.click();
    });

    chatButton.click(function () {
        if (chatInput.val().length > 0) {
            sendMessage(gameName, chatInput.val());
            chatInput.val('');
        }
    });

    socket.onmessage = function (e) {
        //getMessageById(e.data); This would be more efficient
        getConversation(gameName);
    };
});



