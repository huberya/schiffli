class InvalidResponse(Exception):
    pass

def check_request_response(response):
    if response.status_code < 200 or response.status_code >= 300:
        raise InvalidResponse("Received error code %d: %s" %
                              (response.status_code, response.content))
    return response
