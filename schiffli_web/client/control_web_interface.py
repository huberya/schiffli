#here the idea is that this gets the functions that would be called from gui instead of control
#question is if we can have a "generic function", that works for any name and any argument
#and just uses name and arguments to send to server
import os
from time import sleep
from typing import Dict, Any, List, Optional
import requests
from requests.auth import AuthBase, HTTPBasicAuth
import websocket
import yaml
import threading

from schiffli_control.control import GameControl, Action
from schiffli_control.interfaces import GameControlInterface, AbstractAction
from schiffli_util.util import GameConfig
from schiffli_web.client.functions import InvalidResponse, check_request_response

URL = "http://localhost:8000/"

def forwardToServerAndGetResponse(url: str, authentication: AuthBase, data_json: Dict[str,Any]) -> Dict[str,Any]:
    #TODO check on return code
    return requests.post(url, json=data_json, auth=authentication).json()


class UpdateSocket:
    def __init__(self, control_interface: GameControlInterface):
        self.control_interface_ = control_interface
        self.seen_actions_ = None
        #TODO use var
        websocket.enableTrace(True)
        self.ws_ =  websocket.WebSocketApp("ws://localhost:8000/ws/" + self.control_interface_.game_public_key+ "/",
                    on_message = lambda ws, update_id: self.receive_update(ws, update_id),
                    on_error = lambda ws, error_msg: self.ws_error(ws, error_msg),
                    on_close = lambda ws: self.on_close(ws))
        self.ws_.on_open = lambda ws: self.on_open(ws)

    def receive_update(self, ws: websocket.WebSocket, update_id: str):
        self.control_interface_.printInfo(f"Received id {update_id} from websocket.")
        
        #Execute actions
        def single_update(id):
                update_data = requests.get("{}rest/update/{}".format(URL, id), auth=HTTPBasicAuth(self.control_interface_.username, self.control_interface_.password)).json()
                sender_id = int(update_data["participating_player"])
                print(update_data)
                if sender_id == self.control_interface_.player_id:
                    self.control_interface_.printLog("Not updating because it was sent by self")
                else:
                    a = Action("")
                    a.fromDict(yaml.safe_load(update_data["update_data"]))
                    self.control_interface_.printLog(f"Executing action sent from {sender_id}")
                    #TODO deduplicate
                    a.execute(self.control_interface_)
                    for obs in self.control_interface_.getStateObservers():
                        obs.updateState(action=a)
                        
        if self.seen_actions_:
            for id in range(self.seen_actions_, int(update_id) + 1):
                single_update(id)
        else:
            single_update(update_id)
        
        self.seen_actions_ = int(update_id)

    def ws_error(self, ws: websocket.WebSocket, error_msg: str):
        self.control_interface_.printError(error_msg)

    def on_close(self, ws: websocket.WebSocket):
        pass

    def on_open(sel, ws: websocket.WebSocket):
        print ("### Initiating new websocket connectipython my-websocket.pyon ###")

    def initiate(self):
        self.ws_.run_forever()


#Idea now is that we save the full state just as in single player but instead of executing an action we send it to the server
#Is a bit harder, because of "request additional argument stuff"
class ControlClientInterface(GameControl):
    #override
    def __init__(self, game_public_key: str, username: str, password: str, game_config: GameConfig) -> None:
        self.game_public_key = game_public_key
        self.username = username
        self.password = password

        self.game_id = int(check_request_response(requests.get("{}rest/game/{}".format(URL, self.game_public_key), auth=HTTPBasicAuth(self.username, self.password))).json()["id"])
        self.player_id = int(check_request_response(requests.get("{}rest/user/{}".format(URL, self.username), auth=HTTPBasicAuth(self.username, self.password))).json()["id"])

        #get or post participating player id
        try:
            #TODO I think this can also be empty (no results in response) instead of InvalidResponse (not sure though)
            players = check_request_response(requests.get("{}rest/players/{}/".format(URL, self.game_public_key), auth=HTTPBasicAuth(self.username, self.password))).json()["results"]
        except InvalidResponse:
            players = []
        #print(players)
        if not [x for x in players if x["player_id"]==self.player_id]:
            players = check_request_response(requests.post("{}rest/players/{}/".format(URL, self.game_public_key), auth=HTTPBasicAuth(self.username, self.password), json={"game_id": self.game_id, "player_id": self.player_id})).json()["result"]
        
        self.participating_player_id_ = [x for x in players if x["player_id"]==self.player_id][0]["id"]

        GameControl.__init__(self, game_config)

        print("Initializing web socket")
        self.ws_connection = UpdateSocket(self)
        self.th = threading.Thread(target=self.ws_connection.initiate)
        self.th.start()

    #override
    def endActionPack(self) -> None:
        if self._packingactions == 0:
            self.printWarning("You are trying to unpack without packing first")
        self._packingactions-=1
        if self._packingactions==0:
            for action in self.actionpack:
                action.execute(self)
            for obs in self.getStateObservers():
                obs.updateState(actions=self.actionpack)

    #override
    def save(self, new_game=False) -> None:
        data = self._to_dict()
        if new_game:
            check_request_response(requests.post(os.path.join(URL, "rest", "game", ""), json={"game_state": yaml.safe_dump(data), "public_key": self.game_public_key, "active": True}, auth=HTTPBasicAuth(self.username, self.password)))
        else:
            check_request_response(requests.patch(os.path.join(URL, "rest", "game", self.game_public_key, ""), json={"game_state": yaml.safe_dump(data)}, auth=HTTPBasicAuth(self.username, self.password)))

    #override
    def load(self) -> None:
        data = check_request_response(requests.get(os.path.join(URL, "rest", "game", self.game_public_key, ""), auth=HTTPBasicAuth(self.username, self.password))).json()["game_state"]

        self._from_dict(yaml.safe_load(data))

    ### private
    #override
    def executeAction(self, action: AbstractAction) -> None:
        if self._packingactions:
            self.actionpack.append(action)
        else:
            self.printInfo("executing an action")
            
            #TODO this needs some more work
            requests.post("http://localhost:8000/live/{}/api/v1/message/".format(self.game_public_key), json={"participating_player": self.participating_player_id_, "update_data": yaml.safe_dump(action.toDict())}, auth=HTTPBasicAuth(self.username, self.password))
            
            action.execute(self)
            for obs in self.getStateObservers():
                obs.updateState(action=action)

    def quit(self):
        self.ws_connection.ws_.close()
        self.th.join()
