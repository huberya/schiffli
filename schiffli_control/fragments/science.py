import os

from typing import Optional

from schiffli_util import combineDicts, sciencetypes, imagefolder_rescaled

from schiffli_control.interfaces import GameControlInterface

from schiffli_control.fragments.relations import IsA, Owned, Tagged, Controlled

class ScienceCard(Owned, IsA):
    def __init__(self, uuid: str, control: GameControlInterface, tier: Optional[str]=None, type: Optional[str]=None, restore:  Optional[dict]=None):
        Owned.__init__(self, uuid = uuid, control = control, owner=None, restore=restore)
        IsA.__init__(self, type=type, restore=restore)
        if restore is None:
            self.tier = tier
        else:
            self.tier = restore["tier"]

        self.card=os.path.join(imagefolder_rescaled, "cards", str(self._type)+"_"+str(sciencetypes[self._type])+"_"+str(self.tier)+".png")

    def getTier(self):
        return self.tier
    
    def getCard(self):
        return self.card

    #TODO move kwarg values to some definitions and add checks to tests for it
    def activate(self):
        if self._type=="W1": #Windrad
            if self.tier=="T1":
                self._control.addEffectTo(target=self._owner, effecttype="windrad", duration=2, tradingship=1, fleet=2, inheritable=True)
            elif self.tier=="T2":
                self._control.addEffectTo(target=self._owner, effecttype="windrad", duration=2, tradingship=2, fleet=3, inheritable=True)
            elif self.tier=="T3":
                self._control.addEffectTo(target=self._owner, effecttype="windrad", duration=2, tradingship=3, fleet=4, inheritable=True)
            else:
                self.getControl().printDev("Not implemented tier:"+str(self.tier)+" for type:"+str(self._type))
        elif self._type=="W2":
            options = []
            for cand in self.getControl().getObjectsByTag("tradingship"):
                if cand.getOwner() == self.getControl().getActivePlayer():
                    options.append(cand.getUuid())
            targetship=self.getControl().requestAdditionalArgument(options)
            if self.tier=="T1":
                self._control.addEffectTo(target=targetship, effecttype="protection", duration=2, protection="partial", inheritable=False)
            elif self.tier=="T2":
                self._control.addEffectTo(target=targetship, effecttype="protection", duration=2, protection="full", inheritable=False)
            elif self.tier=="T3":
                self._control.addEffectTo(target=self._owner, effecttype="protection", duration=2, protection="full", inheritable=True)
            else:
                self.getControl().printDev("Not implemented tier:"+str(self.tier)+" for type:"+str(self._type))
        elif self._type=="W4":
            if self.tier=="T1":
                self.getControl().printDev("TODO")
                #complete up to 2 ships ausbau instantly
            elif self.tier=="T2":
                self.getControl().printDev("TODO")
                #complete wiederherstellung or ausbau of up to 2 ships instantly
            elif self.tier=="T3":
                self.getControl().printDev("TODO")
                #complete wiederherstellung or ausbau of all ships instantly
            else:
                self.getControl().printDev("Not implemented tier:"+str(self.tier)+" for type:"+str(self._type))
        elif self._type=="W5":
            if self.tier=="T1":
                self._control.addEffectTo(target=self._owner, effecttype="feinmechanik", duration=1, subtype="lower", inheritable=True)
            elif self.tier=="T2":
                self._control.addEffectTo(target=self._owner, effecttype="feinmechanik", duration=1, subtype="try", inheritable=True)
            elif self.tier=="T3":
                self._control.addEffectTo(target=self._owner, effecttype="feinmechanik", duration=1, subtype="higher", inheritable=True)
            else:
                self.getControl().printDev("Not implemented tier:"+str(self.tier)+" for type:"+str(self._type))   
        elif self._type=="W6":
            options = []
            for cand in self.getControl().getObjectsByTag("harbour"):
                options.append(cand.getUuid())
            targetharbour=self.getControl().requestAdditionalArgument(options)
            if self.tier=="T1":
                self._control.addEffectTo(target=targetharbour, effecttype="seuche", duration=2, exclude="none", by=self._owner, inheritable=False)
            elif self.tier=="T2":
                self._control.addEffectTo(target=targetharbour, effecttype="seuche", duration=3, exclude="ally", by=self._owner, inheritable=False)
            elif self.tier=="T3":
                self._control.addEffectTo(target=targetharbour, effecttype="seuche", duration=3, exclude="by", by=self._owner, inheritable=False)
            else:
                self.getControl().printDev("Not implemented tier:"+str(self.tier)+" for type:"+str(self._type))
        elif self._type=="W7":
            if self.tier=="T1":
                self._control.addTmpPoints(self._owner, "science", 5)
                self._control.addTmpPoints(self._owner, "nBuyableScienceCards", 1)
            elif self.tier=="T2":
                self._control.addTmpPoints(self._owner, "science", 10)
                self._control.addTmpPoints(self._owner, "nBuyableScienceCards", 1)
            elif self.tier=="T3":
                self._control.addTmpPoints(self._owner, "science", 20)
                self._control.addTmpPoints(self._owner, "nBuyableScienceCards", 1)
            else:
                self.getControl().printDev("Not implemented tier:"+str(self.tier)+" for type:"+str(self._type))
        elif self._type=="W8":
            if self.tier=="T1":
                self._control.addTmpPoints(self._owner, "ressource", 8)
            elif self.tier=="T2":
                self._control.addTmpPoints(self._owner, "ressource", 12)
            elif self.tier=="T3":
                self._control.addTmpPoints(self._owner, "ressource", 16)
            else:
                self.getControl().printDev("Not implemented tier:"+str(self.tier)+" for type:"+str(self._type))
        elif self._type=="W9":
            options = []
            for cand in self.getControl().getObjectsByTag("fleet"):
                if cand.getOwner() == self.getControl().getActivePlayer():
                    options.append(cand.getUuid())
            targetfleet=self.getControl().requestAdditionalArgument(options)
            if self.tier=="T1":
                self._control.addEffectTo(target=targetfleet, effecttype="defense", duration=1, value=1, inheritable=True)
            elif self.tier=="T2":
                self._control.addEffectTo(target=self._owner, effecttype="defense", duration=1, value=1, inheritable=True)
            elif self.tier=="T3":
                self._control.addEffectTo(target=self._owner, effecttype="defense", duration=1, value=2, inheritable=True)
            else:
                self.getControl().printDev("Not implemented tier:"+str(self.tier)+" for type:"+str(self._type))
        elif self._type=="W10":
            if self.tier=="T1":
                self._control.addTmpPoints(self._owner, "politics", 2)
                #counts as 2 politics for getting adlige (up to 3)
            elif self.tier=="T2":
                self._control.addTmpPoints(self._owner, "politics", 4)
                #counts as 4 politics for getting adlige (up to 4)
            elif self.tier=="T3":
                self._control.addTmpPoints(self._owner, "politics", 6)
                #counts as 6 politics for getting adlige (up to 4) ohne feilschen??
            else:
                self.getControl().printDev("Not implemented tier:"+str(self.tier)+" for type:"+str(self._type))
        elif self._type=="W11":
            self.getControl().printDev("TODO")
            #plagiat TODO
            #independent of tier select sciencecard from other player (without seeing it) How todo this???

        else:
            self.getControl().printDev("TODO: add remaining science card effects")

    def save(self):
        res = {}
        res["tier"]=self.tier
        res = combineDicts(res, Owned.save(self))
        res = combineDicts(res, IsA.save(self))
        return res

class ScienceLevel:
    def __init__(self, levelname):
        parts = levelname.split("_")
        self.level=int(parts[0])
        self.path=int(parts[1])

    def getString(self):
        return str(self.level)+"_"+str(self.path)

    def __add__(self, other):
        return ScienceLevel(str(self.level+other)+"_"+str(self.path))

    def __radd__(self, other):
        self + other