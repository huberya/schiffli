from typing import Optional, Dict, Any, Callable, List
from PIL import Image

#restoreable
#TODO think about what to save and what not (i.e. care about possibly changing imagefolder_rescaled maybe) and adapt to new structure
class Icon:
    def __init__(self, icon: Optional[str] = None, restore: Optional[Dict[str, Any]] = None) -> None:
        self.img: Optional[Image]
        if restore is None:
            self.icon = icon
            self.img = None  
        else:
            self.icon = restore["icon"]
            self.img = None
        # this is needed s.t a object cannot be animated twice at the same time (could happen because of async execution of tk's after())
        # note that while async TK is single threaded, therefore there is no need for thread safety on reading/writing the animated variable
        self.animated=False
        self.animationfunctions: List[Callable] = []

    def setIcon(self, icon: str) -> None:
        self.icon = icon

    def getIcon(self) -> Optional[str]:
        return self.icon

    # this is used to store imgs s.t. python cleanup does not remove it
    def setImg(self, img: Image) -> None:
        self.img = img
    
    def isAnimated(self) -> bool:
        return self.animated

    def startAnimation(self) -> None:
        self.animated = True

    def stopAnimation(self) -> None:
        self.animated = False

    def appendAnimation(self, animfunc: Callable) -> None:
        self.animationfunctions.append(animfunc)

    def getNextAnimation(self) -> Optional[Callable]:
        if len(self.animationfunctions) > 0:
            val = self.animationfunctions[0]
            self.animationfunctions.remove(val)
            return val
        else:
            return None

    def save(self) -> Dict[str, Optional[str]]:
        res: Dict[str, Optional[str]] = {}
        res["icon"]=self.icon
        return res

class Card:
    #TODO
    pass