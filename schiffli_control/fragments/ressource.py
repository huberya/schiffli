import os

from json import JSONEncoder
from typing import Optional, Dict, Any, List

from schiffli_util import combineDicts, imagefolder_rescaled, allrtypes, allicons, allcards, sciencevalues, politicsvalues, verkaufvalues, ankaufvalues

from schiffli_control.interfaces import GameControlInterface

from schiffli_control.fragments.guiattribute import Icon
from schiffli_control.fragments.relations import Tagged, IsA


#TODO think about
class RessourceTile(Icon, Tagged, IsA):
    def __init__(self, uuid: str, control: GameControlInterface, type: Optional[str] = None, values: Optional[List[int]] = None, restore: Optional[Dict[str, Any]] = None) -> None:
        Tagged.__init__(self, uuid = uuid, control = control, restore=restore)
        IsA.__init__(self, type=type, restore=restore)
        if restore is None:
            self._values: List[int]
            if values is None:
                self._values = []
            else:
                self._values = values

            self._nvalues=len(self._values)
            self._pos = 0
            #localIdx should be 0 or 1 and tell which of the two ressourceTiles it fills
            self.localidx = 0
            if not self._type is None and not self._values is None:
                iconname = self._type+"_"
                for i in range(len(self._values)):
                    iconname+=str(self._values[i])
                imgname=self._type
                for v in self._values:
                    imgname+=str(v)
                imgname+=".png"
                Icon.__init__(self, icon=os.path.join(os.path.join(imagefolder_rescaled, "ressfelder", imgname)), restore=restore)
        else:
            self._values=restore["values"]
            if self._values is None:
                self._nvalues = 0
            else:
                self._nvalues=len(self._values)
            self._pos=restore["pos"]
            self.localidx=restore["localidx"]

            Icon.__init__(self, icon=None, restore=restore)
    
    def getNValues(self) -> int:
        return self._nvalues

    def getValues(self) -> List[int]:
        return self._values
    
    def getPos(self) -> int:
        return self._pos

    def setPos(self, pos: int) -> None:
        self._pos=pos

    def getLocalIdx(self) -> int:
        return self.localidx

    def setLocalIdx(self, idx: int) -> None:
        self.localidx = idx

    def save(self) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        res["values"]=self._values
        res["pos"]=self._pos
        res["localidx"]=self.localidx
        res = combineDicts(res, Icon.save(self))
        res = combineDicts(res, IsA.save(self))
        res = combineDicts(res, Tagged.save(self))
        return res


#restoreable
#I think init should be done in subclass not here but how to assert its done
class RessourceStorage:
    def __init__(self, capacity: int = 0, restore: Optional[Dict[str, Any]] = None) -> None:
        if restore is None:
            #probably need to put 0 of everything
            self.ressources: Dict[str, int] = {}
            for rtype in allrtypes:
                self.ressources[rtype]=0
            self.capacity = capacity
        else:
            self.ressources = restore["ressources"]
            self.capacity = restore["capacity"]

    def getRessources(self) -> Dict[str, int]:
        return self.ressources
        
    def getNUniqueRessources(self) -> int:
        res=0
        for _, v in self.ressources.items():
            if v>0:
                res+=1
        return res
    
    def addRessource(self, rtype: str, num: int = 1) -> None:
        self.ressources[rtype]+=num
    
    def removeRessource(self, rtype: str, num: int = 1) -> None:
        if self.ressources[rtype]<num:
            #TODO this can not print either for not knowing control...probably this should also throw instead of print
            print("WARNING: spent more ressources than available, this should be prevented by construction!")
            return
        self.ressources[rtype]-=num
            
    def getNRessources(self) -> int:
        res=0
        for _, v in self.ressources.items():
            res+=v
        return res
    
    def getCapacity(self) -> int:
        return self.capacity

    def newGame(self) -> None:
        #put zero of all ressources
        for rtype in allrtypes:
            self.ressources[rtype]=0

    def getResIcon(self, rtype: str) -> str:
        return os.path.join(imagefolder_rescaled, allicons[rtype])

    def getResCard(self, rtype: str) -> str:
        return os.path.join(imagefolder_rescaled, "cards", allcards[rtype])

    def getResAmount(self, rtype: str) -> int:
        return self.ressources[rtype]

    def getResValue(self, rtype: str, vtype: str) -> int:
        if vtype=="science":
            if rtype in sciencevalues:
                return sciencevalues[rtype]
            else:
                return 0
        elif vtype=="politics":
            if rtype in politicsvalues:
                return politicsvalues[rtype]
            else:
                return 0
        elif vtype=="sell":
            if rtype in verkaufvalues:
                return verkaufvalues[rtype]
            else:
                return 0
        elif vtype=="buy" or vtype=="ressource":
            if rtype in ankaufvalues:
                return ankaufvalues[rtype]
            else:
                return 0
        else:
            #TODO this can not print either for not knowing control
            print("unrecognised type "+str(vtype)+". Maybe TODO")
            return 0

    #function should be overriden by subclasses
    #note: while the function looks symmetric, it is not implemented as such.
    #both sides have to agree to a trade for it to be valid
    def canTradeWith(self, obj: Optional[str]) -> bool:
        return False

    def save(self) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        res["ressources"] = self.ressources
        res["capacity"] = self.capacity
        return res
