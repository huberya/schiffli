from typing import Optional, List, Dict, Any

from schiffli_util import combineDicts

from schiffli_control.interfaces import GameControlInterface

#not restoreable
class Controlled:
    def __init__(self, control: GameControlInterface) -> None:
        self._control = control
    
    def setControl(self, control: GameControlInterface) -> None:
        self._control = control

    def getControl(self) -> GameControlInterface:
        return self._control

    #here we probably also want stuff like getActivePlayer etc for simplicity

#restoreable
class Tagged(Controlled):
    ### public
    def __init__(self, uuid: str, control: GameControlInterface, tag: Optional[str] = None, tags: Optional[List[str]] = None, restore: Dict[str, Any] = None):
        Controlled.__init__(self, control)
        self._completed = True
        self._tags: List[str]
        if restore is None:
            if tags is None:
                self._tags = []
            else:
                self._tags = tags
            if not tag is None:
                self._tags.append(tag)
            self._uuid=uuid
        else:
            self._tags = restore["tags"]
            self._uuid = restore["uuid"]
            self._completed = restore["completed"]
    
    def getTags(self) -> List[str]:
        return self._tags

    def addTag(self, tag: str) -> None:
        if tag in self._tags:
            self.getControl().printWarning("Tag already registered, ignoring")
            return
        self._tags.append(tag)

    def addTags(self, tags: List[str]) -> None:
        for t in tags:
            self.addTag(t)

    def getUuid(self) -> str:
        return self._uuid

    ### private
    #friend class control
    def setTag(self, tag: str) -> None:
        self._tags = [tag]

    def setTags(self, tags: List[str]) -> None:
        self._tags = tags

    def removeTag(self, tag: str) -> None:
        self._tags.remove(tag)

    #usage of this should be very careful as other objects might rely on this
    def setUuid(self, uuid: str) -> None:
        self._uuid = uuid

    def save(self) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        res["tags"]=self._tags
        res["uuid"]=self._uuid
        res["completed"]=self._completed
        return res

tagged2class = {}
for cl in Tagged.__subclasses__():
    tagged2class[cl.__name__] = cl

#restoreable
class Owned(Tagged):
    def __init__(self, uuid: str, control: GameControlInterface, owner: Optional[str] = None, tag: Optional[str] = None, tags: Optional[List[str]] = None, restore: Optional[Dict[str, Any]] = None) -> None:
        Tagged.__init__(self, uuid=uuid, control=control, tag=tag, tags=tags, restore=restore)
        self._owner: Optional[str]
        if restore is None:
            self._owner = owner
        else:
            self._owner = restore["owner"]

    def setOwner(self, owner: Optional[str]) -> None:
        if not self._owner is None:
            func = getattr(self._control.getObjectByUuid(self._owner), "removeObject")
            if callable(func):
                func(self.getUuid())
        self._owner = owner
        if not self._owner is None:
            func = getattr(self._control.getObjectByUuid(self._owner), "addObject")
            if callable(func):
                func(self.getUuid())

    def getOwner(self) -> Optional[str]:
        return self._owner

    def setCompleted(self, completed: bool) -> None:
        self._completed = completed

    def isCompleted(self) -> bool:
        return self._completed

    def save(self) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        res["owner"] = self._owner
        res = combineDicts(res, Tagged.save(self))
        return res

    def delete(self) -> None:
        if not self._owner is None:
            func = getattr(self._control.getObjectByUuid(self._owner), "removeObject")
            if callable(func):
                func(self.getUuid())

#restoreable
class IsA:
    def __init__(self, type: Optional[str] = None, restore: Optional[Dict[str, Any]] = None) -> None:
        self._type: str
        if restore is None:
            if type is None:
                #TODO this cannot print because it does not know control... (how to fix this)
                print("WARNING: constructing object without type, this probably shouldnt happen!")
                self._type = ""
            else:
                self._type = type
        else:
            self._type = restore["type"]
    def getType(self) -> str:
        return self._type

    def setType(self, type: str) -> None:
        self._type = type

    def save(self) -> Dict[str, Optional[str]]:
        res: Dict[str, Optional[str]] = {}
        res["type"] = str(self._type) #For some reason this sometimes is a numpy str....
        return res

#restoreable
class Colored:
    def __init__(self, color: Optional[str] = None, restore: Optional[Dict[str, Any]] = None) -> None:
        if restore is None:
            self._color = color
        else:
            self._color = restore["color"]

    def getColor(self) -> str:
        if self._color is None:
            raise RuntimeError("Requesting color before setting it for an object, this shouldn't happen!")
        return self._color

    def setColor(self, color: str) -> None:
        self._color = color

    def save(self) -> Dict[str, Optional[str]]:
        res = {}
        res["color"]=self._color
        return res

