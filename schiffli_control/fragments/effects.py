from typing import Optional, List, cast, Dict, Any

from schiffli_util import combineDicts

from schiffli_control.interfaces import GameControlInterface

from schiffli_control.fragments.relations import Tagged, IsA, Owned

# the idea of this is that it provides a container for all kinds of effects that can apply to either a player, a ship...
# not that its only for effects that affect at least a whole turn (i.e. status effects) things happening in an instant be packed
# in an action

# restoreable, has new round effects


#TODO fix this...doesnt even call Tagged's init...
class HasEffects(Tagged):  # resolution order issues?
    def __init__(self, restore=None):
        if restore is None:
            self._effects = []
        else:
            self._effects = restore["effects"]

    def newGame(self):
        self._effects = []

    def addEffect(self, effect):
        self._effects.append(effect)

    def removeEffect(self, effect):
        self._effects.remove(effect)

    def getEffects(self, inheritableonly=False):
        res = []
        if inheritableonly:
            for e in self._effects:
                if self._control.getObjectByUuid(e).isInheritable():
                    res.append(e)
        else:
            res = self._effects

        if isinstance(self, Owned) and not self.getOwner() is None:
            owner_obj = self.getControl().getObjectByUuid(self.getOwner())
            res+=owner_obj.getEffects(True)
        return res

    def save(self):
        res = {}
        res["effects"] = self._effects
        return res

    def delete(self):
        for e in list(self._effects):
            self._control.getObjectByUuid(e).removeTarget(self.getUuid())

# TODO target should also be able to take multiple values


class Effect(Tagged, IsA):
    def __init__(self, uuid: str, control: GameControlInterface, type: str = "", duration: int = 1, restore: Optional[dict] = None, inheritable: bool = False, **effectargs) -> None:
        Tagged.__init__(self, uuid=uuid, control=control, restore=restore)
        IsA.__init__(self, type=type, restore=restore)
        if restore is None:
            self._duration = duration
            self._effectargs = effectargs
            self._inheritable = inheritable
            self._targets: List[str] = []
        else:
            self._duration = restore["duration"]
            self._effectargs = restore["effectargs"]
            self._inheritable = restore["inheritable"]
            self._targets = restore["targets"]

    def setTarget(self, target: str) -> None:
        targetobj = cast(HasEffects, self._control.getObjectByUuid(target))
        assert isinstance(targetobj, HasEffects)
        self._targets = [target]
        targetobj.addEffect(self.getUuid())

    def addTarget(self, target: str) -> None:
        self._targets.append(target)

    def removeTarget(self, target: str) -> None:
        self._targets.remove(target)

    def getDuration(self) -> int:
        return self._duration

    def getArgs(self) -> dict:
        return self._effectargs

    def isInheritable(self) -> bool:
        return self._inheritable

    def newRound(self) -> None:
        self._duration -= 1
        # maybe more depending on effect
        if self._duration <= 0:
            # delete if duration is 0
            for t in self._targets:
                tobj = cast(HasEffects, self._control.getObjectByUuid(t))
                tobj.removeEffect(self.getUuid())

    def save(self) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        res["duration"] = self._duration
        res["effectargs"] = self._effectargs
        res["inheritable"] = self._inheritable
        res["targets"] = self._targets
        res = combineDicts(res, Tagged.save(self))
        res = combineDicts(res, IsA.save(self))
        return res

all_effect_types = ["windrad", "protection", "feinmechanik", "seuche", "defense"]
