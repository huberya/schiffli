from typing import List, Optional

class Attack:
    def __init__(self, attacker: str, targetuuid: str, target: int = 0, strength: int = 0, stricthit: bool = False, blockable: bool = False, fromString: Optional[str] = None) -> None:
        if fromString is None:
            self.attacker = attacker
            self.targetuuid = targetuuid
            self.target = target
            self.strength = strength
            self.stricthit = stricthit
            self.blockable = blockable
        else:
            vals = fromString.split(",")
            self.target = int(vals[0])
            self.strength = int(vals[1])
            self.stricthit = bool(vals[2])
            self.blockable = bool(vals[3])

    def toString(self) -> str:
        return str(self.target)+","+str(self.strength)+","+str(self.stricthit)+","+str(self.blockable)



class Attackable:
    def getAttacks(self, target: str) -> List[Attack]:
        raise RuntimeError("Not implemented function getAttacks!")

    def receiveAttacks(self, attacks: List[Attack]) -> None:
        raise RuntimeError("Not implemented function receiveAttacks!")
    
    def getValidAttackTargets(self) -> List[str]:
        raise RuntimeError("Not implemented function getValidAttackTargets!")