import os
import random

from typing import Dict, Optional, Any, List

from schiffli_control.fragments import Controlled
from schiffli_control.interfaces import GameControlInterface

#TODO imports
#from schiffli_control import AbstractAction

#local class should contain obj uuid and list of actions to execute once time is up
#TODO how (and where) to do restore
class TimedActionsWithObject:
    def __init__(self, objuuid: str, actions: List['scontrol.AbstractAction']):
        self._objuuid = objuuid
        self._actions = actions

    def executeActions(self, control: GameControlInterface) -> None:
        for a in self.getActions():
            a.execute(control)

    def getActions(self) -> List['scontrol.AbstractAction']:
        return self._actions

    def getUuid(self) -> str:
        return self._objuuid

    def save(self) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        res["objuuid"] = self._objuuid
        res["actions"] = []
        for a in self.getActions():
            res["actions"].append(a.save())
        return res

#TODO FIXME
class RoundControl(Controlled):
    def __init__(self, control: GameControlInterface, restore: Optional[Dict[str, Any]] = None) -> None:
        Controlled.__init__(self, control)
        if restore is None:
            self.currturn: int = 0
            self.objects: Dict[int, List[TimedActionsWithObject]] = {}
        else:
            self.currturn: int = restore["currturn"]
            self.objects: Dict[int, List[TimedActionsWithObject]] = {}
            for k,v in restore["objects"].items():
                self.objects[k]=[]
                for timed in v:
                    self.objects[k].append(TimedActionsWithObject(timed["objuuid"], timed["actions"]))

    def getCurrTurn(self) -> int:
        return self.currturn

    def getTurnIn(self, offset: int) -> int:
        return (self.currturn + offset) % 6

    def getUuidsAt(self, turn: int) -> List[str]:
        res: List[str] = []
        if turn in self.objects.keys():
            for timed in self.objects[turn]:
                res.append(timed.getUuid())
        return res

    
    def newRound(self) -> None:
        self.currturn += 1
        if self.currturn in self.objects.keys():
            for o in self.objects[self.currturn]:
                o.executeActions(self.getControl())
            self.objects[self.currturn] = []

    #TODO what is the appropriate function type
    #I think correct alternative is to give list of actions (they should inherently be saveable)
    def addObject(self, objuuid: str, actions: List['scontrol.AbstractAction'], turnnr: Optional[int] = None, turnoffset: Optional[int] = None) -> bool:
        key: int
        if turnnr is None and not turnoffset is None:
            key = self.currturn + turnoffset
        elif turnoffset is None and not turnnr is None:
            key = turnnr
        else:
            self.getControl().printWarning("either turnnr or turnoffset must be set, but never both")
            return False

        newtimed = TimedActionsWithObject(objuuid, actions)
        if key in self.objects.keys():
            self.objects[key].append(newtimed)
        else:
            self.objects[key] = [newtimed]
        return True

    def save(self) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        res["currturn"] = self.currturn
        res["objects"] = {}
        for k,v in self.objects.items():
            res["objects"][k] = []
            for timed in v:
                res["objects"][k].append(timed.save())
        return res