from typing import Dict, cast, Any

from schiffli_util import buildingtypes, tradingshiptypes, battleshiptypes, harbourtypes, sciencetypes, effecttypes

from schiffli_control.fragments import RessourceStorage, Attackable, ScienceCard, Tagged, Effect
from schiffli_control.interfaces import GameControlInterface, AbstractAction
from schiffli_control.mapobjects import *

from schiffli_control.control.player import Player
from schiffli_control.control.mission import Mission

#TODO use factory here!!
#and then can remove some dependencies hopefully

# all actions that can be affected by science and such should be implmented as an action, s.t. it can be checked here on execution
# literally everything should be an action
#actions: #TODO what actions exist

#those are more temporary actions, they cannot be reverted and do not change the state by themself (i.e. creating additional random numbers)
#the only reason they must exist is because observers might care about them
class SimpleAction(AbstractAction):
    pass
        
###private
#friend class GameControl
class Action(AbstractAction):
    #control is only allowed to be used to call functions of it not to pass as a value
    def __init__(self, type: str, **args) -> None:
        # init action depending on type has different args (such as ship to recieve, ship to move etc)
        AbstractAction.__init__(self, type, **args)
        self.executed = False  # do we need this

    # returns True on success and False otherwise
    # this function should enforce rules, i.e. only execute if the action is allowed (think about this again, doesnt know kontext)
    def execute(self, control: GameControlInterface) -> bool:
        if self._type == "init":
            objtypename = self._args["objtypename"]
            if not "constructorargs" in self._args.keys():
                self._args["constructorargs"] = {}
            obj: Tagged
            if objtypename in buildingtypes:
                obj = Building(uuid = self._args["uuid"], control = control, type = objtypename, **self._args["constructorargs"])
            elif objtypename in tradingshiptypes:
                obj = TradingShip(uuid = self._args["uuid"], control = control,type = objtypename, **self._args["constructorargs"])
            elif objtypename in battleshiptypes:
                obj = BattleShip(uuid = self._args["uuid"], control = control,type = objtypename, **self._args["constructorargs"])
            elif objtypename in harbourtypes:
                obj = Harbour(uuid = self._args["uuid"], control = control,type = objtypename, **self._args["constructorargs"])
            elif objtypename == "pirate":
                obj = PirateShip(uuid = self._args["uuid"], control = control,**self._args["constructorargs"])
            elif objtypename == "fleet":
                obj = WarFleet(uuid = self._args["uuid"], control = control,**self._args["constructorargs"])
            elif objtypename in sciencetypes.keys():
                obj = ScienceCard(uuid = self._args["uuid"], control = control,type=objtypename, **self._args["constructorargs"])
            elif objtypename in effecttypes:
                obj = Effect(uuid = self._args["uuid"], control = control, type = objtypename, duration=self._args["duration"], inheritable=self._args["inheritable"], **self._args["constructorargs"])
            elif objtypename == "mission":
                obj = Mission(uuid = self._args["uuid"], control = control, **self._args["constructorargs"])
            else:
                control.printError("unrecognised objtypename "+objtypename+". This should not happen!")
                return False
            control.addObject(obj)
            
        elif self._type == "setAttribute":
            obj = control.getObjectByUuid(self._args["objuuid"])
            #TODO exception handling
            func = getattr(obj, "set"+self._args["attributename"].title())
            func(self._args["attributevalue"])

        elif self._type == "updateRandom":
            if self._args["value"]=="next":
                control.getRandomState().updateState()

        elif self._type == "moveRessourceFromTo":
            if not self._args["origin"] is None:
                origin = cast(RessourceStorage, control.getObjectByUuid(self._args["origin"]))
                origin.removeRessource(self._args["rtype"], num=self._args["num"])
            if not self._args["to"] is None:
                receiver = cast(RessourceStorage, control.getObjectByUuid(self._args["to"]))
                receiver.addRessource(self._args["rtype"], num=self._args["num"])

        elif self._type == "moveMapObject":
            #TODO what type
            obj = cast(AbstractMapObject, control.getObjectByUuid(self._args["objuuid"]))
            assert isinstance(obj, AbstractMapObject)
            self._args["origin"] = obj.getPos() #required for undo
            func = getattr(obj, "move", None)
            if callable(func):
                func(self._args["target"])
            else:
                obj.setPos(self._args["target"])
        
        elif self._type == "attackMapObject":
            #we need to do case distinction here
            #I think there are 5 (maybe 6): Pirate -> Fleet, Pirate -> Tradingship, Fleet -> Fleet, Fleet -> Pirate, Fleet -> Building, (Fleet -> Tradingship)?
            obj = control.getObjectByUuid(self._args["objuuid"])
            target_obj = control.getObjectByUuid(self._args["target"])
            assert isinstance(obj, Attackable)
            cast(Attackable, obj)
            assert isinstance(target_obj, Attackable)
            cast(Attackable, target_obj)
            obj_attacks = obj.getAttacks(self._args["target"])
            target_obj_attacks = target_obj.getAttacks(self._args["objuuid"])

            #TODO save them somehwo as part of action for undo

            obj.receiveAttacks(target_obj_attacks)
            target_obj.receiveAttacks(obj_attacks)

        elif self._type == "assignShipTo":
            shipobj = cast(BattleShip, control.getObjectByUuid(self._args["ship"]))
            assert isinstance(shipobj, BattleShip)
            #check if origin and target are editable
            fleet = shipobj.getFleet()
            if not fleet is None:
                originobj = cast(WarFleet, control.getObjectByUuid(fleet))
                if not originobj.isEditable():
                    return False
            if not self._args["to"] is None:
                targetobj = cast(WarFleet, control.getObjectByUuid(self._args["to"]))
                if not targetobj.isEditable():
                    return False

            #order is weird for logic but required to be able to easily abort if not possible because there is already a ship
            if not self._args["to"] is None:
                #why error here...
                if not targetobj.addShip(self._args["ship"], self._args["fleetpos"]):
                    return False
            if not shipobj.getFleet() is None:
                originobj.removeShip(self._args["ship"])
            shipobj.setFleet(self._args["to"])
            
        #TODO maybe simplify the tmppoint stuff
        elif self._type == "removeTmpPoints":
            obj = cast(Player, control.getObjectByUuid(self._args["obj"]))
            obj.removeTmpPoints(self._args["ptype"], num=self._args["num"])

        elif self._type == "addTmpPoints":
            obj = cast(Player, control.getObjectByUuid(self._args["obj"]))
            obj.addTmpPoints(self._args["ptype"], num=self._args["num"])
        
        return True
        
    def toDict(self) -> Dict[Any, Any]:
        return {
            "type": self._type,
            "args": self._args,
            "executed": self.executed
        }

    def fromDict(self, dict: Dict[Any, Any]) -> None:
        self._type = dict["type"]
        self._args = dict["args"]
        self.executed = dict["executed"]
