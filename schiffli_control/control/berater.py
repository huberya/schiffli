from typing import Optional

from schiffli_util import combineDicts

from schiffli_control.fragments import Colored, IsA, Icon

class Berater(Colored, Icon, IsA):
    def __init__(self, name: Optional[str] = None, restore: Optional[dict] = None) -> None:
        Icon.__init__(self, icon="TODO", restore=restore)
        IsA.__init__(self, type="berater", restore=restore)
        Colored.__init__(self)
        if restore is None:
            self.name=name
            self.king = None #TODO
        else:
            self.name=restore["name"]
            self.king=restore["king"]


    #TODO what is this even
    def getKing(self) -> str:
        return self.king
        
    def save(self) -> dict:
        res={}
        res["name"]=self.name
        res["king"]=self.king
        res = combineDicts(res, Icon.save(self))
        res = combineDicts(res, IsA.save(self))
        return res

    