from typing import Any, Optional, Dict, List

from schiffli_control.fragments import Controlled
from schiffli_control.interfaces import GameControlInterface

#TODO who and how to store which player already revieved points for condition
#Local object
class WinCondition:
    def __init__(self, func):
        self._func = func

    #TODO think about passing gamestate efficiently maybe
    def isFulfilled(self, *args, **kwargs):
        return self._func(*args, **kwargs)

#TODO
#class that holds points/achieved goals etc and checks at end of each players turn I think
class PointSystem(Controlled):
    def __init__(self, control: GameControlInterface, restore: Optional[Dict[str, Any]] = None) -> None:
        Controlled.__init__(self, control)
        self._winconditions: List[WinCondition] = [] #TODO fill this