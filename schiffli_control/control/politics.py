import numpy as np
from typing import Optional, List

from schiffli_util import combineDicts

from schiffli_control.interfaces import GameControlInterface

from schiffli_control.control.berater import Berater

#TODO this is really wrong, like terrible, think about datastructure of berater
class Politics:
    def __init__(self, control: GameControlInterface , restore: Optional[dict] = None) -> None:
        #for relation we use:
        #           -1: krieg
        #           0: neutral
        #           1: verbündet
        #           2: mit vertrag (vertragstyp?)
        if restore is None:
            self._control = control
            self._nplayers = len(self._control.getPlayers())
            self._relations = np.zeros((self._nplayers, self._nplayers))

            #berater
            self.berater: List[str] = [] #better word

            #more?

        else:
            self._control = control
            self._nplayers = len(self._control.getPlayers())
            self._relations = np.asarray(restore["relations"]).reshape((self._nplayers, self._nplayers))
            self.berater = restore["berater"]

    
    def newGame(self, event=0):
        self._relations = np.zeros((self._nplayers, self._nplayers))

        #berater
        self.berater = [] #better word

    def getRelations(self):
        return self._relations

    def getBerater(self):
        return self.berater

    def getBeraterByColor(self, color):
        res = []
        for b in self.berater:
            if b.getColor()==color:
                res.append(b)
        return res

    def addBerater(self, color):
        self._control.printDev("TODO")

    def removeBerater(self, berater):
        self._control.printDev("TODO")

    def save(self):
        res={}
        res["relations"]=self._relations.reshape(-1).tolist()
        res["berater"]=[]
        for b in self.berater:
            res["berater"].append(b.save())

        return res