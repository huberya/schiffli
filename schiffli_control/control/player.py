from typing import Optional, List, cast

from schiffli_util import startressources, startships, globalworld, tradingshiptypes, battleshiptypes, buildingtypes, sciencetypes, combineDicts

from schiffli_control.interfaces import GameControlInterface
from schiffli_control.fragments import Colored, RessourceStorage, HasEffects, Tagged, IsA, ScienceLevel, Owned
from schiffli_control.mapobjects import WarFleet, Building


#this class represents players, also this should control permissions and so on for players
#a player is active iff its his turn and inactive otherwise
class Player (Colored, RessourceStorage, HasEffects, Tagged, IsA):
    def __init__(self, uuid: str, control: GameControlInterface, restore: Optional[dict] = None) -> None:
        Colored.__init__(self, color=None, restore=restore)
        RessourceStorage.__init__(self, capacity=999999, restore=restore)
        HasEffects.__init__(self, restore=restore) #TODO
        Tagged.__init__(self, uuid=uuid, control=control, restore=restore)
        IsA.__init__(self, "player")
        if restore is None:
            #normal case
            self._active: bool = False
            self._tradingships: List[str] = []
            self._battleships: List[str] = []
            self._fleets: List[str] = []
            self._buildings: List[str] = []
            self._sciencecards: List[str] = []
            self._sciencelevel = ScienceLevel("0_0")
            self._tmppoints={
                "science": 0,
                "politics": 0,
                "ressource": 0,
                "nBuyableScienceCards": 1
            }

        else:
            self._sciencelevel = ScienceLevel(restore["sciencelevel"])
            self._tmppoints = restore["tmppoints"]
            if not "nBuyableScienceCards" in self._tmppoints.keys():
                self._tmppoints["nBuyableScienceCards"] = 1
            self._tradingships = restore["tradingships"]
            self._battleships = restore["battleships"]
            self._fleets = restore["fleets"]
            self._buildings = restore["buildings"]
            self._sciencecards = restore["sciencecards"]
            self._active=False
        
    def isActive(self) -> bool:
        return self._active
                
    def newGame(self) -> None:
        HasEffects.newGame(self)
        RessourceStorage.newGame(self)
        #add starting ressources
        for rtype in startressources:
            self.addRessource(rtype)

        self._active=False
        self._tradingships=[]
        for s in startships:
            tile=globalworld.getHomeTown(self)
            self._control.newMapObject(s, tile, self.getUuid(), tags=["tradingship", "clickable"])
            
        self._fleets=[]
        #player has 3 empty fleets
        for i in range(3):
            self._control.newMapObject("fleet", -1, self.getUuid(), tags=["fleet", "clickable"], num=i+1)
        self._buildings=[]
        self._sciencecards=[]
        self._sciencelevel=ScienceLevel("0_0")
        
        
        
        
    def newRound(self) -> None:
        self._tmppoints["nBuyableScienceCards"] = 1
        #some other stuff todo on new round
        
    def endTurn(self) -> None:
        self._active=False
        for k, _ in self._tmppoints.items():
            self._tmppoints[k]=0
        
    def startTurn(self) -> None:
        self._active=True
    
    #gives position of hometown    
    def getPos(self) -> int:
        return globalworld.homecountry[self.getColor()]
        
    def addObject(self, objectuuid: str) -> None:
        if not self._active:
            self.getControl().printInfo("Adding object to inactive player. Are you sure this is correct?", to_all=True)
        obj = cast(IsA, self._control.getObjectByUuid(objectuuid))
        if obj.getType() in tradingshiptypes:
            self._tradingships.append(objectuuid)
        elif obj.getType() in battleshiptypes:
            self._battleships.append(objectuuid)
        elif obj.getType() in buildingtypes:
            self._buildings.append(objectuuid)
        elif obj.getType() == "fleet":
            self._fleets.append(objectuuid)
        elif obj.getType() in sciencetypes:
            self._sciencecards.append(objectuuid)
        else:
            self.getControl().printWarning("Dont know how to add object of type "+obj.getType())

    def removeObject(self, objectuuid: str) -> None:
        if not self._active:
            self.getControl().printInfo("Removing object from inactive player. Are you sure this is correct?")
        obj = cast(IsA, self._control.getObjectByUuid(objectuuid))
        if obj.getType() in tradingshiptypes:
            self._tradingships.remove(objectuuid)
        elif obj.getType() in battleshiptypes:
            self._battleships.remove(objectuuid)
        elif obj.getType() in buildingtypes:
            self._buildings.remove(objectuuid)
        elif obj.getType() == "fleet":
            self._fleets.remove(objectuuid)
        elif obj.getType() in sciencetypes:
            self._sciencecards.remove(objectuuid)   
        else:
            self.getControl().printWarning("Dont know how to remove object of type "+obj.getType())

    def getTmpPoints(self, type: str) -> int:
        return self._tmppoints[type]

    def addTmpPoints(self, type: str, num: int = 1) -> None:
        self._tmppoints[type]+=num
    
    def removeTmpPoints(self, type: str, num: int = 1) -> None:
        self._tmppoints[type]-=num

    def getScienceCards(self) -> List[str]:
        return self._sciencecards

    def removeScienceCard(self, sciencecard: str) -> None:
        self._sciencecards.remove(sciencecard)

    def getScienceLevel(self) -> ScienceLevel:
        return self._sciencelevel

    def setScienceLevel(self, sciencelevel: ScienceLevel) -> None:
        self._sciencelevel=sciencelevel

    def canBuyScience(self) -> bool:
        return self._tmppoints["nBuyableScienceCards"] > 0
    
    def getTradingShips(self) -> List[str]:
        return self._tradingships
        
    def ownsHarbour(self, harbour: str) -> bool:
        hpos = self._control.getObjectByUuid(harbour)
        if hpos in globalworld.harbours.keys():
            towntiles=globalworld.harbours[hpos]
            for tile in towntiles:
                for b in self._control.getObjectsByPos(tile, tag="building"):
                    assert isinstance(b, Building)
                    b = cast(Building, b)
                    if b.getPos() in towntiles:
                        return True
        #not found so we dont own it
        return False
    
    def getOwnedHarbours(self) -> List[str]:
        res: List[str] = []
        for hpos in globalworld.harbours.keys():
            harbour = self._control.getObjectsByPos(hpos, tag="harbour")[0].getUuid() #we know there can only be one harbour at a position
            if self.ownsHarbour(harbour):
                res.append(harbour)
        return res
        
    def getTownsFromHarbour(self, pos: int) -> List[str]:
        res: List[str] = []
        if pos in globalworld.harbours.keys():
            towntiles=globalworld.harbours[pos]
            for b in self._buildings:
                bobj = cast(Building, self._control.getObjectByUuid(b))
                if bobj.getPos() in towntiles:
                    res.append(b)
        #not found so we dont own it
        return res
        
    def getBattleShips(self) -> List[str]:
        return self._battleships
        
    def getFleets(self) -> List[str]:
        return self._fleets
        
    def getFleet(self, fleetnr: int) -> Optional[str]:
        for f in self._fleets:
            fleetobj = cast(WarFleet, self._control.getObjectByUuid(f))
            if fleetobj.getNum()==fleetnr:
                return f
        #if there is no fleet with that number
        return None
    
    def canTradeWith(self, obj: Optional[str]) -> bool:
        return True

    def save(self) -> dict:
        res={}
        res["sciencelevel"]=self.getScienceLevel().getString()
        res["tmppoints"]=self._tmppoints
        res["tradingships"]=self._tradingships
        res["battleships"]=self._battleships
        res["fleets"]=self._fleets
        res["buildings"]=self._buildings
        res["sciencecards"]=self._sciencecards
        res = combineDicts(res, Colored.save(self))
        res = combineDicts(res, RessourceStorage.save(self))
        res = combineDicts(res, HasEffects.save(self))
        res = combineDicts(res, Tagged.save(self))
        res = combineDicts(res, IsA.save(self))
        return res
    
    def delete(self) -> None:
        HasEffects.delete(self)
        #tell all owned objects that we dont exist anymore
        for t in list(self._tradingships):
            cast(Owned, self._control.getObjectByUuid(t)).setOwner(None)
        for b in list(self._battleships):
            cast(Owned, self._control.getObjectByUuid(b)).setOwner(None)
        for b in list(self._buildings):
            cast(Owned, self._control.getObjectByUuid(b)).setOwner(None)
        for f in list(self._fleets):
            cast(Owned, self._control.getObjectByUuid(f)).setOwner(None)
        for s in list(self._sciencecards):
            cast(Owned, self._control.getObjectByUuid(s)).setOwner(None)