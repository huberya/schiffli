# imports
import os
import numpy as np
import uuid
import json
from datetime import datetime
from functools import lru_cache

from typing import Dict, List, Union, Optional, cast, Any

from schiffli_util import globalworld, playercolors, harbouramount, harbourtypes, buildingtypes, buildingcost, sciencetier_from_cost, sciencetypes, baseroundtriggers, shipcost, tradingshiptypes, allrtypes, buildtime, nActiveMissions, missionimages, NotEnoughRessources, TemporaryInvalidAction, InvalidAction, InvalidTarget, NotEnoughSpace, InvalidConfiguration, BadCode, GameConfig


from schiffli_control.fragments import Tagged, RessourceTile, ScienceCard, RessourceStorage, all_effect_types
from schiffli_control.interfaces import StateObserverI, GameControlInterface, AbstractAction, RandomState
from schiffli_control.mapobjects import AbstractMapObject, Building, TradingShip, Harbour, PirateShip, WarFleet, Refinement, BattleShip

#internal imports
from schiffli_control.control.player import Player
from schiffli_control.control.mission import Mission
from schiffli_control.control.politics import Politics
from schiffli_control.control.roundcontrol import RoundControl
from schiffli_control.control.action import Action, SimpleAction

#No one is allowed to import this guy, because circular

# master class to control who is active and actually execute stuff
# should follow the observer pattern
class GameControl(GameControlInterface):
    ### public
    def __init__(self, game_config: GameConfig) -> None:
        ## member variables
        self._stateobserver: List[StateObserverI] = []

        self._taggedobjects: List[Tagged] = [] #this is kind of the storage, probably we want something faster than a list eventually, but we want the rest to be independent of the implmenation anyways

        self._nplayers: int = game_config.nplayers
        self._players: List[str] = []
        for i in range(self._nplayers):
            self._players.append(str(uuid.uuid4()))

        self._activeplayer: int = 0

        #TODO move turncount to roundcontrol
        self._turncount: int = 0
        self._roundcontrol: RoundControl = RoundControl(self)

        self._politics: Politics = Politics(self)

        self._placingsettlements: bool = False
        self._firsthalfplacement: bool = False
        
        #used for getting tile corners by clicking
        #self.pointlist = []

        self._packingactions: int = 0

        self._randomstate: RandomState = RandomState()

        #idea here is that an object can aquire this lock (if it is free), as long as aquired only actions where this object takes part are possible
        self._objectlock: Union[str, None] = None

        self._devmode: bool = game_config.devmode

        #TODO this would be for speedup (maybe later)
        # matrix to contain pointers to abjects at positions
        # eacht position has a list containing all objects at that position and is empty if None
        """self.content_matrix = np.empty(globalworld.nfields, dtype=object)
        for i in range(globalworld.nfields):
            self.content_matrix[i] = []
        # matrix to contain ressource types and dice values by position. Empty dict means no ressources there
        self.ressource_matrix = np.empty(globalworld.nfields, dtype=object)
        for i in range(globalworld.nfields):
            self.ressource_matrix[i] = []"""

    def addStateObserver(self, observer: StateObserverI) -> None:
        self._stateobserver.append(observer)

    def getStateObservers(self) -> List[StateObserverI]:
        return self._stateobserver
    
    #TODO how to make sure there are never 2 observers for one player
    def getActiveStateObserver(self) -> StateObserverI:
        for obs in self.getStateObservers():
            if self.getActivePlayer() in obs.getResponsiblePlayers():
                return obs
        raise InvalidConfiguration("No state observer found for active player!")

    def getObjectsByTag(self, tag: str) -> List[Tagged]:
        res: List[Tagged] = []
        for obj in self._taggedobjects:
            if tag in obj.getTags():
                res.append(obj)
        return res

    def getObjectsByTags(self, tags: List[str]) -> List[Tagged]:
        res: List[Tagged] = []
        for obj in self._taggedobjects:
            if set(tags).issubset(obj.getTags()):
                res.append(obj)
        return res

    @lru_cache(maxsize=None)
    def getObjectByUuid(self, uuid: str) -> Tagged:
        if not type(uuid)==str:
            self.printError("Uuid (" + str(uuid) + ") is not a string, This is most likely a mistake", to_all=True)
            raise BadCode("bad call to function getObjectByUuid")
        for obj in self._taggedobjects:
            if uuid == obj.getUuid():
                return obj
        raise InvalidConfiguration("call to function getObjectByUuid found no result for given id! This means there is a memory leak!")

    def getObjectsByPos(self, pos: int, tag: Optional[str] = None, tags: Optional[List[str]] = None) -> List[Tagged]:
        res: List[Tagged] = []
        for obj in self._taggedobjects:
            #TODO how to convince pylint that this is ok
            getPosAttr = getattr(obj, "getPos", None)
            if callable(getPosAttr) and getPosAttr()==pos:
                if (tags is None or set(tags).issubset(obj.getTags())) and (tag is None or tag in obj.getTags()):
                    res.append(obj)
        return res

    #TODO thinkg about this
    def addObject(self, obj : Tagged) -> None:
        # this is a (hopefully) rare example where something happens without it being an action, because there is no way to save it
        setControlAttr = getattr(obj, "setControl", None)
        if callable(setControlAttr):
            obj.setControl(self)
        self._taggedobjects.append(obj)

    def addObjects(self, objs: List[Tagged]) -> None:
        for obj in objs:
            self.addObject(obj)

    def deleteObject(self, objuuid: str) -> bool:
        for obj in self._taggedobjects:
            if obj.getUuid() == objuuid:
                #TODO how to convince pylint that this is ok
                func = getattr(obj, "delete", None)
                if callable(func):
                    func()
                self._taggedobjects.remove(obj)
                return True
        return False

    def newGame(self) -> None:
        #somehow need to cleanup objects here I think
        for obj in list(self._taggedobjects):
            self.deleteObject(obj.getUuid())
        
        for i, p_uuid in enumerate(self._players):
            p = Player(uuid = p_uuid, control=self)
            p.setTags(["player", str(i), "clickable"]) #maybe more
            self._taggedobjects.append(p)
        self._activeplayer = 0

        self._randomstate.updateState()
        self._activeplayer = 0
        currpc = np.random.permutation(playercolors).tolist()

        # give start ressources to players
        for idx, puuid in enumerate(self._players):
            # make sure everything is empty
            player = cast(Player, self.getObjectByUuid(puuid))
            player.setColor(currpc[idx])
        
        self.pirates: List[str] = []

        #TODO move turncount to roundcontrol
        self._turncount = 0
        #self._roundcontrol.newGame()
        
        self._politics.newGame()

        self.wonders: List[str] = []
        self._active_missions: List[str] = []

        self._placingsettlements = True
        self._firsthalfplacement = True

        # here we place the harbours
        allharbours = []
        for k, v in harbouramount.items():
            for i in range(v):
                allharbours.append(k)
        allharbours = np.random.permutation(allharbours)
        tmpcounter = 0
        for hk, _ in globalworld.harbours.items():
            self.newMapObject(allharbours[tmpcounter], hk, None, tags=["harbour", "clickable"])
            tmpcounter += 1

        # do random ressource placement (by country and no 2 with 3 on one country)
        # cleanup incase this is restart
        for ck, _ in globalworld.continents.items():
            availressources: List[Tagged] = []
            for tv in globalworld.getRessourcesByContinent(ck):
                availressources.append(RessourceTile(str(uuid.uuid4()), self, type = tv[0], values = tv[1]))
            self.addObjects(availressources)
            allcountries = globalworld.getCountriesByContinent(ck)
            validconfig = False
            # repeat until there are no double threes
            while not validconfig:
                validconfig = True
                permutation = np.random.permutation(len(availressources))
                for i in range(len(allcountries)):
                    availressources[permutation[2*i]].setPos(allcountries[i])
                    availressources[permutation[2*i]].setLocalIdx(0)
                    availressources[permutation[2*i]].setTags(["restile", str(allcountries[i])])
                    availressources[permutation[2*i+1]].setPos(allcountries[i])
                    availressources[permutation[2*i+1]].setLocalIdx(1)
                    availressources[permutation[2*i+1]].setTags(["restile", str(allcountries[i])])
                    if availressources[permutation[2*i]].getNValues() >= 3 and availressources[permutation[2*i+1]].getNValues() >= 3:
                        validconfig = False
                        continue

        for obj in self._taggedobjects:
            func = getattr(obj, "newGame", None)
            if callable(func):
                func()

        for obs in self.getStateObservers():
            obs.updateState(new_game = True)

    def getRoundControl(self) -> RoundControl:
        return self._roundcontrol

    def getPlayers(self) -> List[str]:
        return self._players
        
    def getActivePlayer(self) -> str:
        return self._players[self._activeplayer]

    #helper function
    def getActivePlayerObj(self) -> Player:
        return cast(Player, self.getObjectByUuid(self.getActivePlayer()))

    def getRandomState(self) -> RandomState:
        return self._randomstate

    def getActiveMissions(self) -> List[str]:
        return self._active_missions

    def placingSettlements(self) -> bool:
        return self._placingsettlements
        
    def placeSettlement(self, pos: int) -> None:
        if not self.placingSettlements():
            raise InvalidAction
        if not self._objectlock is None:
            raise TemporaryInvalidAction
        if len(self.getObjectsByPos(pos, tag="building"))!=0 or (not pos in globalworld.buildingposition):
            raise InvalidTarget

        self.newMapObject("settlement", pos, self.getActivePlayer(), tags=["building", "clickable"])

        if self._firsthalfplacement:
            self._activeplayer+=1
        elif self._activeplayer == 0:
            self._placingsettlements = False
            self.newRound()
        else:
            self._activeplayer -= 1

        if self._activeplayer>=self._nplayers:
            self._activeplayer=self._nplayers-1
            self._firsthalfplacement = False
        
        for obs in self.getStateObservers():
            #TODO fix this (should get an action or something)
            obs.updateState()

    #TODO exception handling for usage
    def buildTown(self, builderuuid: str, btype: str, pos: int, **constructorargs) -> None:
        if not self._objectlock is None and not self._objectlock == builderuuid:
            raise TemporaryInvalidAction
        builder = cast(RessourceStorage, self.getObjectByUuid(builderuuid))
        assert isinstance(builder, RessourceStorage)
        if len(self.getObjectsByPos(pos, tag="building"))!=0 or (not pos in globalworld.buildingposition):
            raise InvalidTarget
        #check for sufficient ressources
        for r, amount in buildingcost[btype].items():
            if builder.getResAmount(r) < amount:
                # we don't have enough so return
                raise NotEnoughRessources
        
        #remove ressources and create building
        self.startActionPack()
        for r, amount in buildingcost[btype].items():
            a = Action("moveRessourceFromTo", rtype=r, origin = builderuuid, to=None, num=amount)
            a.execute(self)
        self.newMapObject(btype, pos, self.getActivePlayer(), tags=["building", "clickable"], **constructorargs)
        self.endActionPack()

    def buyShip(self, stype: str, tags: List[str], **constructorargs) -> None:
        if not self._objectlock is None:
            raise TemporaryInvalidAction
        #check for sufficient ressources
        buyer = cast(RessourceStorage, self.getActivePlayerObj())
        assert isinstance(buyer, RessourceStorage)
        for r, amount in shipcost[stype].items():
            if buyer.getResAmount(r) < amount:
                # we don't have enough so return
                raise NotEnoughRessources
        #determine placement position
        if stype in tradingshiptypes:
            pos = globalworld.getHomeTown(buyer)
        else:
            pos = -1
        #remove ressources and create ship
        self.startActionPack()
        for r, amount in shipcost[stype].items():
            a = Action("moveRessourceFromTo", rtype=r, origin = self.getActivePlayer(), to=None, num=amount)
            a.execute(self)
        self.newMapObject(stype, pos, self.getActivePlayer(), tags=tags, delay=buildtime[stype], **constructorargs)
        self.endActionPack()


    def newMapObject(self, objtypename: str, pos: int, owner: Optional[str], tags: Optional[List[str]] = None, delay: int = 0, **constructorargs) -> None:
        #TODO problem with this is that it makes building settlement fail because ship has the object lock, need to make this smarter
        #if not self._objectlock is None:
        #    raise TemporaryInvalidAction
        self.startActionPack()
        curruuid = str(uuid.uuid4())
        a = Action("init", objtypename = objtypename, uuid = curruuid, constructorargs=constructorargs)
        self.executeAction(a)

        self.setAttribute(objuuid = curruuid, attributename="Owner", attributevalue = owner)
        if delay == 0:
            self.setAttribute(objuuid = curruuid, attributename="Pos", attributevalue = pos)
        else:
            self.setAttribute(objuuid = curruuid, attributename="Available", attributevalue = False)
            self.setAttribute(objuuid = curruuid, attributename="Pos", attributevalue = -1)
            completeAction = Action("setAttribute", objuuid = curruuid, attributename="Available", attributevalue=True)
            placeAtAction = Action("setAttribute", objuuid = curruuid, attributename="Pos", attributevalue=pos)
            self.addObjToRoundControl(curruuid, [completeAction, placeAtAction], turnoffset=delay)

            #The question should here be the concept wether the player already owns the object while being built or not
            #how to make sure that:
            #1) is somewhat owned by player but not really
            #2) is painted on roundcontrol but nowhere else
            #3) has single function call that actually adds it to player
        if not tags is None:
            self.setAttribute(objuuid = curruuid, attributename="tags", attributevalue = tags)
        self.endActionPack()
        #think about content_matrix concept (is basically only to speed upt)
        #self.content_matrix[pos].append(obj)
       
    #TODO what type
    def moveMapObject(self, obj: str, target: int) -> None:
        if not self._objectlock is None and not self._objectlock == obj:
            raise TemporaryInvalidAction
        if not cast(AbstractMapObject, self.getObjectByUuid(obj)).isAvailable():
            raise TemporaryInvalidAction
        func = getattr(self.getObjectByUuid(obj), "getValidTargets", None)
        if not callable(func):
            raise InvalidAction
        if not target in func():
            raise InvalidTarget
        a = Action("moveMapObject", objuuid = obj, target = target)
        self.executeAction(a)

    #TODO is str correct type here
    #TODO what type
    def attackMapObject(self, obj: str, target: str) -> None:
        if not self._objectlock is None and not self._objectlock == obj:
            raise TemporaryInvalidAction
        func = getattr(self.getObjectByUuid(obj), "getValidAttackTargets", None)
        if not callable(func) or not target in func():
            return
        self.startActionPack()
        a = Action("attackMapObject", objuuid = obj, target = target)
        self.executeAction(a)
        self.endActionPack()

    def removeTmpPoints(self, obj: str, ptype: str, num: int) -> None:
        a = Action("removeTmpPoints", obj=obj, ptype=ptype, num=num)
        self.executeAction(a)

    def addTmpPoints(self, obj: str, ptype: str, num: int) -> None:
        a = Action("addTmpPoints", obj=obj, ptype=ptype, num=num)
        self.executeAction(a)

    #TODO objectlock maybe
    def moveRessourceFromTo(self, rtype: str, origin: Optional[str], to: Optional[str], num: int = 1) -> None:
        if not origin is None:
            origin_obj = cast(RessourceStorage, self.getObjectByUuid(origin))
            if not origin_obj.canTradeWith(to):
                raise TemporaryInvalidAction
            if origin_obj.getResAmount(rtype) < num:
                raise NotEnoughRessources
        if not to is None:
            to_obj = cast(RessourceStorage, self.getObjectByUuid(to))
            if not to_obj.canTradeWith(origin):
                raise TemporaryInvalidAction
            if to_obj.getCapacity() < to_obj.getNRessources() + num:
                raise NotEnoughSpace
        a = Action("moveRessourceFromTo", rtype=rtype, origin=origin, to=to, num=num)
        self.executeAction(a)
    
    #helper function for dev
    def addRessourcesTo(self, rtype: str, to: str, num: int = 1) -> None:
        self.moveRessourceFromTo(rtype, None, to, num)

    #helper function for dev
    def removeRessourcesFrom(self, rtype: str, origin: str, num: int = 1) -> None:
        self.moveRessourceFromTo(rtype, origin, None, num)

    def buyRessource(self, rtype: str) -> None:
        playerobj = self.getActivePlayerObj()
        if playerobj.getTmpPoints(type="ressource")>=playerobj.getResValue(vtype="sell", rtype=rtype):
            self.startActionPack()
            self.removeTmpPoints(self.getActivePlayer(), "ressource", playerobj.getResValue(vtype="sell", rtype=rtype))
            self.moveRessourceFromTo(rtype, None, self.getActivePlayer())
            self.endActionPack()
        else:
            raise NotEnoughRessources

    def sellRessource(self, rtype: str, ptype: str = "ressource") -> None:
        playerobj = self.getActivePlayerObj()
        self.startActionPack()
        self.moveRessourceFromTo(rtype, self.getActivePlayer(), None)
        if ptype == "ressource":
            self.addTmpPoints(self.getActivePlayer(), ptype, playerobj.getResValue(vtype="buy", rtype=rtype))
        else:
            self.addTmpPoints(self.getActivePlayer(), ptype, playerobj.getResValue(vtype=ptype, rtype=rtype))
        self.endActionPack()

    #here refinement is passed as an object, this is fine because its a temporary object
    def refineRessources(self, refinement: Refinement, objuuid: str) -> None:
        if not self._objectlock is None and not self._objectlock == objuuid:
            raise TemporaryInvalidAction
        obj = cast(RessourceStorage, self.getObjectByUuid(objuuid))
        #make sure obj has enough ressources
        inressources = refinement.getInRessources()
        for k, v in inressources.items():
            if obj.getResAmount(k) < v:
                raise NotEnoughRessources
        #get out ressources from refinement (probably need to somehow pass a random number)
        #also this should check for effects
        rand = self.requestAdditionalRandomNumbers(1)
        outressources = refinement.getOutRessources(rand[0])
        #remove inressourcesfrom object and add outressources
        for k, v in inressources.items():
            self.moveRessourceFromTo(k, objuuid, None, num=v)
        for k, v in outressources.items():
            self.moveRessourceFromTo(k, None, objuuid, num = v)

    def completeMission(self, mission: str, objuuid: str) -> None:
        if not (self._objectlock is None or self._objectlock == objuuid):
            raise TemporaryInvalidAction        
        #check if ship has sufficient ressources
        missionobj = cast(Mission, self.getObjectByUuid(mission))
        shipobj = cast(TradingShip, self.getObjectByUuid(objuuid))
        if not shipobj.canTradeWith(missionobj.getTarget()):
            raise TemporaryInvalidAction
        inressources = missionobj.getObjectives()
        for k, v in inressources.items():
            if shipobj.getResAmount(k) < v:
                raise NotEnoughRessources
        #remove ressources from ship
        for k, v in inressources.items():
            self.moveRessourceFromTo(k, objuuid, None, num=v)
        
        self.addTmpPoints(objuuid, "politics", missionobj.getReward())

        #TODO: new berater stuff and somehow restrict points (maybe)

        #remove mission from active missions
        self._active_missions.remove(mission)
        
    def buyScience(self, tier: str) -> None:
        if not self._objectlock is None:
            raise TemporaryInvalidAction
        if not self.getActivePlayerObj().canBuyScience():
            raise TemporaryInvalidAction
        for k, v in sciencetier_from_cost.items():
            if v == tier:
                cost = int(k)
        player = self.getActivePlayerObj()
        if player.getTmpPoints("science") < cost:
            raise NotEnoughRessources
        self.startActionPack()
        self.removeTmpPoints(self.getActivePlayer(), "science", cost)
        self.removeTmpPoints(self.getActivePlayer(), "nBuyableScienceCards", 1)
        self.addScienceCard(self.getActivePlayer(), tier)
        self.endActionPack()


    def addScienceCard(self, obj: str, tier: str) -> None:
        #needs to create a card object of tier and then assign it to obj
        self.startActionPack()
        curruuid = str(uuid.uuid4())
        a = Action("init", objtypename = np.random.choice(list(sciencetypes.keys())), constructorargs = {"tier":tier}, uuid = curruuid)
        self.executeAction(a)
        self.setAttribute(objuuid = curruuid, attributename="Owner", attributevalue = self.getActivePlayer())
        self.endActionPack()

    def activateScienceCard(self, sciencecard: str) -> None:
        if not self._objectlock is None:
            raise TemporaryInvalidAction
        self.startActionPack()
        #this should do all the activation related stuff
        cast(ScienceCard, self.getObjectByUuid(sciencecard)).activate()
        self.deleteObject(sciencecard)
        self.endActionPack()

    def addEffectTo(self, target: str, effecttype: str, duration: int, inheritable: bool, **kwargs) -> None:
        if not effecttype in all_effect_types:
            raise InvalidConfiguration
        self.startActionPack()
        curruuid = str(uuid.uuid4())
        a = Action("init", objtypename = effecttype, duration=duration, uuid=curruuid, inheritable=inheritable, constructorargs=kwargs)
        self.executeAction(a)
        self.setAttribute(objuuid = curruuid, attributename="Target", attributevalue = target)
        self.endActionPack()
        
    def evalTile(self, pos: int) -> List[str]:
        res: List[str] = []
        for restile in self.getObjectsByTags(["restile", str(pos)]):
            restile = cast(RessourceTile, restile)
            if self.getRandomState().getPrimaryRandom() in restile.getValues():
                res.append(restile.getType())
        return res

    #TODO lock and exceptions
    def assignShipToFleet(self, ship: str, fleet: str, fleetpos: int) -> bool:
        shipobj = cast(BattleShip, self.getObjectByUuid(ship))
        #make sure the ship is allowed to move at all
        #TODO clean this up
        originfleet = shipobj.getFleet()
        if not (originfleet is None or cast(WarFleet, self.getObjectByUuid(originfleet)).isEditable()) and (fleet is None or cast(WarFleet, self.getObjectByUuid(fleet)).isEditable()):
            return False
        a = Action("assignShipTo", ship=ship, to=fleet, fleetpos=fleetpos)
        self.executeAction(a)
        return True

    def nextTurn(self) -> None:
        if not self._objectlock is None:
            raise TemporaryInvalidAction
        #TODO how to do this as an action
        playerobj = self.getActivePlayerObj()
        playerobj.endTurn()
        self._activeplayer += 1
        if self._activeplayer >= self._nplayers:
            self._activeplayer = 0
            self.newRound()
        else:
            playerobj = self.getActivePlayerObj()
            playerobj.startTurn()
            for obs in self.getStateObservers():
                obs.updateState()

    #TODO Interface until here I think
    
    def startActionPack(self) -> None:
        #TODO somehow mark for undo/redo
        if self._packingactions == 0:
            self.actionpack: List[AbstractAction] = []
        self._packingactions+=1

    def endActionPack(self) -> None:
        if self._packingactions == 0:
            self.printWarning("You are trying to unpack without packing first")
        self._packingactions-=1
        if self._packingactions==0:
            for action in self.actionpack:
                action.execute(self)
            for obs in self.getStateObservers():
                obs.updateState(actions=self.actionpack)
 
    #TODO pack in actions somehow
    #TODO make throw exceptions
    def acquireObjectLock(self, caller: str) -> bool:
        print(f"{caller} tries to aqcuire lock {self._objectlock}")
        if self._objectlock is None:
            self._objectlock = caller
            return True
        else:
            return False

    def freeObjectLock(self, caller: str) -> bool:
        if self._objectlock == caller:
            self._objectlock = None
            return True
        else:
            return False

    #TODO find a solution for this
    def completeCurrTradingTown(self, ship: str):
        shipobj = self.getObjectByUuid(ship)
        assert isinstance(shipobj, TradingShip)
        shipobj.completeCurrTradingTown()
    
    def requestAdditionalArgument(self, options: List[Any]):
        if len(options) == 0:
            raise TemporaryInvalidAction
        return self.getActiveStateObserver().requestAdditionalArgument(options)
        

    def requestAdditionalRandomNumbers(self, num: int) -> List[int]:
        rand = self.getRandomState().getAdditionalNumbers(1)
        self.executeAction(SimpleAction("temporaryRandomNumbers", numbers = rand))
        return rand
    

    #printing utility functions
    #these should be able to optionally switch to observer of active player
    def printLog(self, text: str, to_all: bool = False):
        if to_all:
            for obs in self.getStateObservers():
                obs.printLog(text)
        else:
            self.getActiveStateObserver().printLog(text)

    def printInfo(self, text: str, to_all: bool = False):
        if to_all:
            for obs in self.getStateObservers():
                obs.printInfo(text)
        else:
            self.getActiveStateObserver().printInfo(text)

    def printWarning(self, text: str, to_all: bool = False):
        if to_all:
            for obs in self.getStateObservers():
                obs.printWarning(text)
        else:
            self.getActiveStateObserver().printWarning(text)

    def printError(self, text: str, to_all: bool = False):
        if to_all:
            for obs in self.getStateObservers():
                obs.printError(text)
        else:
            self.getActiveStateObserver().printError(text)

    def printDev(self, text: str, to_all: bool = False):
        if to_all:
            for obs in self.getStateObservers():
                obs.printDev(text)
        else:
            self.getActiveStateObserver().printDev(text)



    ### private
    def executeAction(self, action: AbstractAction) -> None:
        if self._packingactions:
            self.actionpack.append(action)
        else:
            action.execute(self)
            for obs in self.getStateObservers():
                obs.updateState(action=action)

    def setAttribute(self, objuuid: str, attributename: str, attributevalue: Any) -> None:
        a = Action("setAttribute", objuuid = objuuid, attributename=attributename, attributevalue=attributevalue)
        self.executeAction(a)

    #TODO move as much as possible of this to roundcontrol
    def newRound(self) -> None:
        #TODO think about how to do this with actions
        # invoke newRound function for all players and pass the generated random numbers
        self._turncount += 1
        a = Action("updateRandom", value="next")
        self.executeAction(a)
        self.printLog("primary random number: "+str(self._randomstate.getPrimaryRandom()), to_all=True)
        self.printLog("secondary random number: "+str(self._randomstate.getSecondaryRandom()), to_all=True)
        # run triggers (such as pirates or ships that are completly repaired/built)
        # base triggers
        #this should be fine maybe should typehint the stuff in definitions
        #TODO fix this
        for t in baseroundtriggers[self._turncount % 6]:
            if "Piraten" == t:
                haspirate = False
                for p in self.getObjectsByTag("pirate"):
                    assert isinstance(p, PirateShip)
                    cast(PirateShip, p)
                    if p.getPos() in globalworld.seapartition[self.getRandomState().getPrimaryRandom()]:
                        haspirate = True
                        break

                if not haspirate:
                    #create new mapobject
                    self.newMapObject("pirate", globalworld.piratespawnpoints[self.getRandomState().getPrimaryRandom()-1], None, tags = ["pirate, clickable"]) #TODO maybe add something like automove tag here
            elif "Expeditionen" == t:
                for muuid in self.getActiveMissions():
                    mobj = self.getObjectByUuid(muuid)
                    self._taggedobjects.remove(mobj)
                self._active_missions = []
                #select new missions
                midx = np.random.choice(list(missionimages.keys()) , nActiveMissions, replace=False)
                self.startActionPack()
                for m in midx:
                    #create object and add uuid to active missions
                    curruuid = str(uuid.uuid4())
                    #TODO create this action
                    a = Action("init", objtypename = "mission", uuid = curruuid, constructorargs={"missionnr":m})
                    self.executeAction(a)
                    self._active_missions.append(curruuid)
                self.endActionPack()
            else:
                self.printDev("TODO: baseround trigger " + t, to_all=True)

        for obj in self._taggedobjects:
            func = getattr(obj, "newRound", None)
            if callable(func):
                func()

        self.getRoundControl().newRound()

        self._activeplayer = 0
        self.getActivePlayerObj().startTurn()

        for obs in self.getStateObservers():
            obs.updateState()

    #TODO make use of this (incoporate build/repairtimes)
    def addObjToRoundControl(self, objuuid: str, actions: List[AbstractAction], turnnr: Optional[int] = None, turnoffset: Optional[int] = None) -> bool:
        if not self.getRoundControl().addObject(objuuid = objuuid, actions = actions, turnnr = turnnr, turnoffset = turnoffset):
            self.printWarning("Add to roundcontrol failed for unknown reasons!")
            return False
        return True

    # save function
    def _to_dict(self) -> Dict[Any, Any]:
        # build the dictionary
        #TODO think about how to do the ressource tiles
        data: dict = {}
        # save everything from control directly
        data["control"] = {}
        #save objects
        data["control"]["objects"]={}
        for obj in self._taggedobjects:
            data["control"]["objects"][obj.getUuid()] = obj.save()
        
        data["control"]["activeplayer"] = self._activeplayer
        data["control"]["turncount"] = self._turncount
        data["control"]["players"] = self._players
        data["control"]["objectlock"] = self._objectlock
        data["control"]["placingsettlements"] = self._placingsettlements

        #TODO put politics in seperate main category
        data["control"]["politics"] = self._politics.save()
        data["control"]["roundcontrol"] = self._roundcontrol.save()
        data["control"]["activeExpedtions"] = self._active_missions

        return data

    def save(self) -> None:
        # build the dictionary
        #TODO think about how to do the ressource tiles
        data = self._to_dict()

        # maybe more?
        currtime = datetime.now()
        filename = "schiffli_"+str(currtime.year)+"_"+str(currtime.month)+"_"+str(
            currtime.day)+"_"+str(currtime.hour)+"_"+str(currtime.minute)+".save"
        with open(filename, "w") as sf:
            json.dump(data, sf)

    def _from_dict(self, data: Dict[Any, Any]):
        self._activeplayer = data["control"]["activeplayer"]
        self._players = data["control"]["players"]
        self.selectedobject = None
        self._placingsettlements = False
        self._turncount = data["control"]["turncount"]
        self._objectlock = data["control"]["objectlock"]
        self._placingsettlements = data["control"]["placingsettlements"]

        for _, objdata in data["control"]["objects"].items():
            #TODO make some kind of factory/registry I think
            objtypename: str = objdata["type"]
            #make this better (only case where no uuid is needed)
            obj: Tagged
            if objtypename in buildingtypes:
                obj = Building(uuid = "", control = self, restore = objdata)
            elif objtypename in tradingshiptypes:
                obj = TradingShip(uuid = "", control = self, restore = objdata)
            elif objtypename in harbourtypes:
                obj = Harbour(uuid = "", control = self, restore = objdata)
            elif objtypename == "pirate":
                obj = PirateShip(uuid = "", control = self, restore = objdata)
            elif objtypename == "fleet":
                obj = WarFleet(uuid = "", control = self, restore = objdata)
            elif objtypename == "player":
                obj = Player(uuid = "", control = self, restore = objdata)
            elif objtypename in allrtypes:
                obj = RessourceTile(uuid = "", control = self, restore = objdata)
            elif objtypename in sciencetypes.keys():
                obj = ScienceCard(uuid = "", control = self, restore=objdata)
            elif objtypename == "mission":
                obj = Mission(uuid = "", control = self, restore = objdata)
            else:
                self.printError("Unrecognised objtypename "+objtypename+". This should not happen!")
            func = getattr(obj, "setControl", None)
            if callable(func):
                obj.setControl(self)
            self._taggedobjects.append(obj)

        #TODO load politics

        #TODO remove optional
        if "roundcontrol" in data["control"].keys():
            self._roundcontrol = RoundControl(self, restore = data["control"]["roundcontrol"])
        if "activeMissions" in data["control"].keys():
            self._active_missions = data["control"]["activeMissions"]

        self.getActivePlayerObj().startTurn()
        for obs in self.getStateObservers():
            #TODO: once there are multiple observers somehow make sure the right one takes repsonsibility for the correct player
            obs.setResponsiblePlayers(self._players)
            obs.updateState()
    
    def load(self, filename: str) -> None:
        self._taggedobjects = []
        with open(filename, "r") as sf:
            data = json.load(sf)
        self._from_dict(data)

    def quit(self):
        pass

    #this is debatable where it belongs
    def openpdf(self, filename: str) -> None:
        #subprocess.Popen([os.path.join("files", filename)],shell=True)
        #open(os.path.join("files", filename))
        os.system(os.path.join("files", filename))
