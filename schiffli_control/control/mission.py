import os

from typing import Optional, Dict, Any

from schiffli_util import missiontargets, missionobjectives, missionrewards, missionimages, combineDicts, imagefolder_rescaled

from schiffli_control.interfaces import GameControlInterface
from schiffli_control.fragments import Icon, IsA, Tagged

#TODO
class Mission(Icon, Tagged, IsA):
    def __init__(self, uuid: str, control: GameControlInterface, missionnr: Optional[int] = None, restore: Optional[Dict[str, Any]] = None):
        Tagged.__init__(self, uuid=uuid, control=control, restore=restore)
        IsA.__init__(self, type="mission", restore=restore)
        self.target: str
        self.objectives: Dict[str, int]
        self.reward: int
        if restore is None and not missionnr is None:
            Icon.__init__(self, os.path.join(imagefolder_rescaled, "missions", missionimages[missionnr]), restore=restore) #TODO needs graphic
            self.graphic = os.path.join(imagefolder_rescaled, "refinementgraphics", "placeholder.png")
            self.target = missiontargets[missionnr]
            self.objectives = missionobjectives[missionnr]
            self.reward = missionrewards[missionnr]
        elif not restore is None:
            self.graphic = restore["card"]
            self.target = restore["target"]
            self.objectives = restore["objectives"]
            self.reward = restore["reward"]
        else:
            self.getControl().printError("Neither mission number nor restore is set!")

    def getTarget(self) -> str:
        return self.target

    def getObjectives(self) -> Dict[str, int]:
        return self.objectives

    def getReward(self) -> int:
        return self.reward
    
    def getGraphic(self) -> Optional[str]:
        return self.graphic

    #what does a mission need in addition?

    def save(self) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        res["target"] = self.target
        res["objectives"] = self.objectives
        res["reward"] = self.reward
        res["graphic"] = self.graphic
        res = combineDicts(res, Tagged.save(self))
        res = combineDicts(res, Icon.save(self))
        res = combineDicts(res, IsA.save(self))
        return res