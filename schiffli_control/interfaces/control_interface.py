#TODO add back this line and move away from strings
#from __future__ import annotations
from functools import lru_cache
from typing import List, Optional, TYPE_CHECKING

if TYPE_CHECKING:
    from schiffli_control.interfaces import StateObserverI, RandomState, AbstractAction
    from schiffli_control.control import Player, RoundControl
    from schiffli_control.fragments import Tagged
    from schiffli_control.mapobjects import Refinement
    from schiffli_util import GameConfig

class GameControlInterface:
    def __init__(self, game_config: 'GameConfig') -> None:
        raise RuntimeError("not implemented function init")

    def addStateObserver(self, observer: 'StateObserverI') -> None:
        raise RuntimeError("not implemented function addStateObserver")

    def getStateObservers(self) -> List['StateObserverI']:
        raise RuntimeError("not implemented function getStateObservers")
    
    def getActiveStateObserver(self) -> 'StateObserverI':
        raise RuntimeError("not implemented function getActiveStateObserver")

    def getObjectsByTag(self, tag: str) -> List['Tagged']:
        raise RuntimeError("not implemented function getObjectsByTag")

    def getObjectsByTags(self, tags: List[str]) -> List['Tagged']:
        raise RuntimeError("not implemented function getObjectsByTags")

    @lru_cache(maxsize=None)
    def getObjectByUuid(self, uuid: str) -> 'Tagged':
        raise RuntimeError("not implemented function getObjectByUuid")

    def getObjectsByPos(self, pos: int, tag: Optional[str] = None, tags: Optional[List[str]] = None) -> List['Tagged']:
        raise RuntimeError("not implemented function getObjectsByPos")

    def executeAction(self, action: 'AbstractAction') -> None:
        raise RuntimeError("not implemented function executeAction")

    def addObject(self, obj : 'Tagged') -> None:
        raise RuntimeError("not implemented function addObject")

    def addObjects(self, objs: List['Tagged']) -> None:
        raise RuntimeError("not implemented function addObjects")

    def deleteObject(self, objuuid: str) -> bool:
        raise RuntimeError("not implemented function deleteObject")

    def newGame(self) -> None:
        raise RuntimeError("not implemented function newGame")

    def getRoundControl(self) -> 'RoundControl':
        raise RuntimeError("not implemented function getRoundControl")

    def getPlayers(self) -> List[str]:
        raise RuntimeError("not implemented function getPlayers")
        
    def getActivePlayer(self) -> str:
        raise RuntimeError("not implemented function getActivePlayer")

    def getActivePlayerObj(self) -> 'Player':
        raise RuntimeError("not implemented function getActivePlayerObj")

    def getRandomState(self) -> 'RandomState':
        raise RuntimeError("not implemented function getRandomState")

    def getActiveMissions(self) -> List[str]:
        raise RuntimeError("not implemented function getActiveMissions")

    def placingSettlements(self) -> bool:
        raise RuntimeError("not implemented function placingSettlements")
        
    def placeSettlement(self, pos: int) -> None:
        raise RuntimeError("not implemented function placeSettlement")

    def buildTown(self, builderuuid: str, btype: str, pos: int, **constructorargs) -> None:
        raise RuntimeError("not implemented function buildTown")

    def buyShip(self, stype: str, tags: List[str], **constructorargs) -> None:
        raise RuntimeError("not implemented function buyShip")

    def newMapObject(self, objtypename: str, pos: int, owner: Optional[str], tags: Optional[List[str]] = None, delay: int = 0, **constructorargs) -> None:
        raise RuntimeError("not implemented function newMapObject")
       
    def moveMapObject(self, obj: str, target: int) -> None:
        raise RuntimeError("not implemented function moveMapObject")

    def attackMapObject(self, obj: str, target: str) -> None:
        raise RuntimeError("not implemented function attackMapObject")

    def removeTmpPoints(self, obj: str, ptype: str, num: int) -> None:
        raise RuntimeError("not implemented function removeTmpPoints")

    def addTmpPoints(self, obj: str, ptype: str, num: int) -> None:
        raise RuntimeError("not implemented function addTmpPoints")

    def moveRessourceFromTo(self, rtype: str, origin: Optional[str], to: Optional[str], num: int = 1) -> None:
        raise RuntimeError("not implemented function moveRessourceFromTo")

    def buyRessource(self, rtype: str) -> None:
        raise RuntimeError("not implemented function buyRessource")

    def sellRessource(self, rtype: str, ptype: str = "ressource") -> None:
        raise RuntimeError("not implemented function sellRessource")

    def refineRessources(self, refinement: 'Refinement', objuuid: str) -> None:
        raise RuntimeError("not implemented function refineRessources")

    def completeMission(self, mission: str, objuuid: str) -> None:
        raise RuntimeError("not implemented function completeMission")
        
    def buyScience(self, tier: str) -> None:
        raise RuntimeError("not implemented function buyScience")

    def addScienceCard(self, obj: str, tier: str) -> None:
        raise RuntimeError("not implemented function addScienceCard")

    def activateScienceCard(self, sciencecard: str) -> None:
        raise RuntimeError("not implemented function activateScienceCard")

    def addEffectTo(self, target: str, effecttype: str, duration: int, inheritable: bool, **kwargs) -> None:
        raise RuntimeError("not implemented function addEffectTo")
        
    def evalTile(self, pos: int) -> List[str]:
        raise RuntimeError("not implemented function evalTile")

    def assignShipToFleet(self, ship: str, fleet: str, fleetpos: int) -> bool:
        raise RuntimeError("not implemented function assignShipToFleet")

    def nextTurn(self) -> None:
        raise RuntimeError("not implemented function nextTurn")

    def printLog(self, text: str, to_all: bool = False):
        raise RuntimeError("not implemented function printLog")

    def printInfo(self, text: str, to_all: bool = False):
        raise RuntimeError("not implemented function printInfo")

    def printWarning(self, text: str, to_all: bool = False):
        raise RuntimeError("not implemented function printWarning")

    def printError(self, text: str, to_all: bool = False):
        raise RuntimeError("not implemented function printError")

    def printDev(self, text: str, to_all: bool = False):
        raise RuntimeError("not implemented function printDev")

    def quit(self):
        raise RuntimeError("not implemented function quit")
