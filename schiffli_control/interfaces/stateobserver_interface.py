#this is the interface any stateobserver must implement
from typing import Optional, Any, List, TYPE_CHECKING

if TYPE_CHECKING:
    from schiffli_control.interfaces import GameControlInterface, AbstractAction

class StateObserverI:
    #TODO this should probably do registration
    def __init__(self, control: 'GameControlInterface', **kwargs) -> None:
        raise RuntimeError("not implemented function init")

    #TODO exception instead of bool
    def updateState(self, action: Optional['AbstractAction'] = None, actions: Optional[List['AbstractAction']] = None, new_game: bool = False) -> bool:
        raise RuntimeError("not implemented function updateState")

    #TODO type hint for action probably cant import
    def requestAdditionalArgument(self, options: List[Any]) -> Any:
        raise RuntimeError("not implemented function requestAdditionalArgument")

    def getResponsiblePlayers(self) -> List[str]:
        raise RuntimeError("not implemented function getResponsiblePlayers")
    
    def setResponsiblePlayers(self, players: List[str]) -> None:
        raise RuntimeError("not implemented function getResponsiblePlayers")
    
    def printLog(self, text: str) -> None:
        raise RuntimeError("not implemented function printLog")
    
    def printInfo(self, text: str) -> None:
        raise RuntimeError("not implemented function printInfo")

    def printWarning(self, text: str) -> None:
        raise RuntimeError("not implemented function printWarning")

    def printError(self, text: str) -> None:
        raise RuntimeError("not implemented function printError")

    def printDev(self, text: str) -> None:
        raise RuntimeError("not implemented function printDev")