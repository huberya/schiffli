#TODO add back this line and move away from strings
#from __future__ import annotations
from typing import TYPE_CHECKING, Any, Dict

if TYPE_CHECKING:
    from schiffli_control.interfaces import GameControlInterface

class AbstractAction:
    #control is only allowed to be used to call functions of it not to pass as a value
    def __init__(self, type: str, **args) -> None:
        # init action depending on type has different args (such as ship to recieve, ship to move etc)
        self._type = type
        self._args = args

    def getType(self) -> str:
        return self._type
    
    def getArgs(self) -> Dict[str, Any]:
        return self._args

    def execute(self, control: 'GameControlInterface') -> bool:
        return True

    def toDict(self) -> Dict[Any, Any]:
        return {
            "type": self._type,
            "args": self._args
        }

    def fromDict(self, dict: Dict[Any, Any]) -> None:
        self._type = dict["type"]
        self._args = dict["args"]
