import random
from typing import List

class RandomState:
    def __init__(self) -> None:
        self._primaryrandom = random.randint(1,6)
        self._secondaryrandom = random.randint(1,6)

    def updateState(self) -> None:
        self._primaryrandom = random.randint(1,6)
        self._secondaryrandom = random.randint(1,6)

    def setState(self, primaryrandom: int, secondaryrandom: int) -> None:
        self._primaryrandom=primaryrandom
        self._secondaryrandom = secondaryrandom

    def getPrimaryRandom(self) -> int:
        return self._primaryrandom

    def getSecondaryRandom(self) -> int:
        return self._secondaryrandom
    
    def getAdditionalNumbers(self, num: int = 1) -> List[int]:
        res = []
        for _ in range(num):
            res.append(random.randint(1,6))
        return res