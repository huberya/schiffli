import os
import traceback

from typing import Optional, Dict, Any

from schiffli_util import allicons, imagefolder_rescaled, combineDicts

from schiffli_control.interfaces import GameControlInterface
from schiffli_control.fragments import Owned, Icon, IsA, HasEffects

# this is the abstract base class for any object that should be placed on the map (canvas)
# all these objects should be implemented as subclasses of this one


class AbstractMapObject(Icon, Owned, HasEffects, IsA):
    def __init__(self, uuid: str, control: GameControlInterface, type: Optional[str] = None, restore: Optional[Dict[str, Any]] = None) -> None:
        Owned.__init__(self, uuid=uuid, control=control, owner=None, restore=restore)
        HasEffects.__init__(self, restore=restore)
        IsA.__init__(self, type=type, restore=restore) #order is important here
        Icon.__init__(self, os.path.join(imagefolder_rescaled, allicons[self._type]))
        if restore is None:
            # normal case
            # no valid starting position
            self._pos = -1            
            self._buildtime = 0
            self._available = True
        else:
            self._pos = restore["pos"]
            self._buildtime = restore["buildtime"]
            self._available = restore["available"]

    def isAvailable(self) -> bool:
        return self._available

    def setAvailable(self, available: bool) -> None:
        self._available = available

    def setPos(self, pos: int) -> None:
        #TODO remove this (for debugging only)
        if(pos > 300):
            self.getControl().printError("got out of range position, probably uninitialized")
            traceback.print_stack()
        self._pos = pos

    def getPos(self) -> int:
        return self._pos

    def getBuildTime(self) -> int:
        return self._buildtime

    def newRound(self) -> None:
        pass

    def save(self) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        res["pos"] = int(self._pos)
        res["buildtime"] = self._buildtime
        res["available"] = self._available
        res = combineDicts(res, Owned.save(self))
        res = combineDicts(res, Icon.save(self))
        res = combineDicts(res, HasEffects.save(self))
        res = combineDicts(res, IsA.save(self))
        return res

    def delete(self):
        Owned.delete(self)
        HasEffects.delete(self)
