#THIS FILE IS REQUIRED TO INCLUDE THIS DIRECTORY AS A MODULE
#DO NOT DELETE THIS FILE

from schiffli_control.mapobjects.mapobject import AbstractMapObject

from schiffli_control.mapobjects.building import Building
from schiffli_control.mapobjects.harbour import Harbour, Refinement
from schiffli_control.mapobjects.ship import TradingShip, BattleShip, WarFleet, PirateShip

from schiffli_control.mapobjects.mapobject_factory import map_object2class