import os

from typing import Optional, Dict, Any, cast, List

from schiffli_util import buildingcapacity, imagefolder_rescaled, allicons, basisressources, buildingattack, buildingdefense, buildingnattacks, battleshiptypes, tradingshiptypes, combineDicts

from schiffli_control.interfaces import GameControlInterface
from schiffli_control.fragments import RessourceStorage, Colored, Attackable, Attack

from schiffli_control.mapobjects.mapobject import AbstractMapObject

class Building(AbstractMapObject, RessourceStorage, Attackable, Colored): #also has an Icon, think about colored, as it is a common case to have colors changing the Icon
    def __init__(self, uuid: str, control: GameControlInterface, type: Optional[str] = None, restore: Optional[Dict[str, Any]] = None) -> None:
        AbstractMapObject.__init__(self, uuid=uuid, control=control, type=type, restore=restore)
        RessourceStorage.__init__(self, capacity = buildingcapacity[self._type], restore=restore)
        Colored.__init__(self, restore=restore)
        if not restore is None:
            #what to do about getColor being optional
            self.setIcon(os.path.join(imagefolder_rescaled, self.getColor(), allicons[self._type]))

    def setOwner(self, owner: Optional[str]) -> None:
        AbstractMapObject.setOwner(self, owner)
        if not owner is None:
            ownerobj = cast(Colored, self._control.getObjectByUuid(owner))
            assert isinstance(ownerobj, Colored)
            self.setColor(ownerobj.getColor())
            self.setIcon(os.path.join(imagefolder_rescaled, self.getColor(), allicons[self._type]))

    def newRound(self) -> None:
        AbstractMapObject.newRound(self)
        self._control.startActionPack()
        if self._type == "settlement" or self._type == "fort" or self._type == "metropole":
            # need to change this
            rtypes = self._control.evalTile(self._pos)
            for rtype in rtypes:
                if self._type == "metropole":
                    if rtype in basisressources:
                        self._control.moveRessourceFromTo(rtype, None, self._owner, num=2)
                    else:
                        self._control.moveRessourceFromTo(rtype, None, self.getUuid(), num=2)
                else:
                    if rtype in basisressources:
                        self._control.moveRessourceFromTo(rtype, None, self._owner)
                    else:
                        self._control.moveRessourceFromTo(rtype, None, self.getUuid())
        self._control.endActionPack()

    def getAttack(self) -> int:
        return buildingattack[self._type]

    def getDefense(self) -> int:
        return buildingdefense[self._type]

    def getNAttacks(self) -> int:
        return buildingnattacks[self._type]

    def getAttacks(self, target: str) -> List[Attack]:
        res: List[Attack] = []
        for _ in range(self.getNAttacks):
            rng = self._control.requestAdditionalRandomNumbers(2)
            res.append(Attack(self.getUuid(), target, rng[0], self.getAttack()+rng[1]), blockable = True)
        return res

    def receiveAttacks(self, attacks: List[Attack]) -> None:
        #TODO this is very special, see reglement
        for attack in attacks:
            attackerobj = self._control.getObjectByUuid(attack.attacker)
            if attackerobj.getType() in battleshiptypes:
                pass
            else:
                self.getControl().printWarning("ERROR: dont know how to handle attack on " + self.getType() + " from " + attackerobj.getType())

    def canTradeWith(self, obj: Optional[str]) -> bool:
        if self._type in ["settlement", "fort", "metropole"]:
            return obj is None or self.getControl().getObjectByUuid(obj).getType() in tradingshiptypes
        return False

    def save(self) -> Dict[str, Any]:
        res:Dict[str, Any] = {}
        res = combineDicts(res, AbstractMapObject.save(self))
        res = combineDicts(res, RessourceStorage.save(self))
        res = combineDicts(res, Colored.save(self))
        return res

    def delete(self) -> None:
        AbstractMapObject.delete(self)