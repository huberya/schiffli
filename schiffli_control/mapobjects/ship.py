import os
import sys
import numpy as np

from typing import Optional, Dict, Any, List, cast, TYPE_CHECKING

from schiffli_util import shipcapacity, shipspeed, shipboost, shiparmor, shiphealth, shipdamage, shiprange, imagefolder_rescaled, allicons, globalworld, buildingtypes, tradingshiptypes, TemporaryInvalidAction, combineDicts, computeDistance

from schiffli_control.interfaces import GameControlInterface
from schiffli_control.fragments import RessourceStorage, Colored, Attack, Attackable

from schiffli_control.mapobjects.mapobject import AbstractMapObject

if TYPE_CHECKING:
    from schiffli_control.control import Player

class TradingShip(AbstractMapObject, RessourceStorage, Attackable, Colored):
    def __init__(self, uuid: str, control: GameControlInterface, type: Optional[str] = None, restore: Optional[Dict[str, Any]] = None) -> None:
        AbstractMapObject.__init__(self, uuid=uuid, control=control, type=type, restore=restore)
        RessourceStorage.__init__(self, capacity=shipcapacity[self._type], restore=restore)
        Colored.__init__(self, restore=restore)
        if restore is None:
            # normal case
            # here come additional properties of the merch ship
            self._speed = shipspeed[self._type]
            self._boost = shipboost[self._type]
            self._armor = shiparmor[self._type]

            self._currrange = self._speed
            self._moving = False
            self._usedboost = False
            self._tradingtowns: List[str] = []
            self._validtargets: List[int] = []
        else:
            self._speed = shipspeed[self._type]
            self._boost = shipboost[self._type]
            self._armor = shiparmor[self._type]
            self._currrange = restore["currrange"]
            self._moving = restore["moving"]
            self._usedboost = restore["usedboost"]
            self._tradingtowns = restore["tradingtowns"]
            self._validtargets = restore["validtargets"]
            self.setIcon(os.path.join(imagefolder_rescaled, self.getColor(), allicons[self._type]))

    def setOwner(self, owner: Optional[str]) -> None:
        AbstractMapObject.setOwner(self, owner)
        if not owner is None:
            ownerobj = cast(Colored, self._control.getObjectByUuid(owner))
            assert isinstance(ownerobj, Colored)
            self.setColor(ownerobj.getColor())
            self.setIcon(os.path.join(imagefolder_rescaled, self.getColor(), allicons[self._type]))

    def getCurrRange(self) -> int:
        return self._currrange

    def newRound(self) -> None:
        AbstractMapObject.newRound(self)
        self._moving = False
        self._currrange = self._speed
        self._usedboost = False
        self._tradingtowns = []


    def getDefense(self) -> int:
        return self._armor

    def getValidTargets(self) -> List[int]:
        return self._validtargets

    def computeValidTargets(self, oldpos: Optional[int] = None) -> None:
        if self._owner is None:
            raise RuntimeError("Tried to call computeValidTargets without having owner set. This is invalid!")
        res = []
        if self._currrange <= 0:
            self._validtargets = []
            return
        tmpvalidtargets = np.argwhere(globalworld.connectivity_matrix[0, self._pos, :] == 1)
        ownerobj = cast(Colored, self._control.getObjectByUuid(self._owner))
        assert isinstance(ownerobj, Colored)
        if oldpos is None or self._pos in globalworld.harbours.keys() or self._pos == globalworld.getHomeTown(ownerobj):
            # here we are allowed to turn around
            for v in tmpvalidtargets:
                res.append(v[0])
        else:
            # here we are not
            for v in tmpvalidtargets:
                if not v[0] == oldpos:
                    res.append(v[0])
        #remove generally invalid targets
        #for now this is only other homecountries
        res2 = []
        for v in res:
            if v in globalworld.homecountry.values() and not v == globalworld.getHomeTown(ownerobj):
                continue
            res2.append(v)
        self._validtargets = res2

    # moves and returns the valid targets for next move
    def move(self, newpos: int) -> None:
        self._control.printDev(f"Obtained request to move {self.getUuid()} to {newpos}.")
        if self._owner is None:
            raise RuntimeError("Tried to call move without having owner set. This is invalid!")
        oldpos = self._pos
        ownerobj = cast('Player', self._control.getObjectByUuid(self._owner))
        if not self._moving:
            self._control.printDev(f"Acquiring new object lock")
            if not self._control.acquireObjectLock(self.getUuid()):
                #cant acquire lock->cant move (TODO think about if we can make this noexcept)
                raise TemporaryInvalidAction
            #we start moving so we need to check if there are any effects that increase our range right now
            for e in self.getEffects():
                eobj = self.getControl().getObjectByUuid(e)
                if eobj.getType() == "windrad":
                    self._currrange += eobj.getArgs()["tradingship"]

            self._moving = True
            if self._pos in globalworld.harbours:
                harbour = self._control.getObjectsByPos(self._pos, tag="harbour")[0].getUuid()
                self._tradingtowns.append(harbour)
                tmptowns = ownerobj.getTownsFromHarbour(self._pos)
                if len(tmptowns)>0:
                    # store this so the player can later trade stuff in this harbour
                    self._tradingtowns += tmptowns
                    if not self._usedboost:
                        self._currrange += self._boost
                        self._usedboost = True

            if globalworld.homecountry[ownerobj.getColor()] == self._pos and not self._owner is None:
                self._tradingtowns += [self._owner]
        
        # set new position
        self._control.printDev(f"Moving the object.")
        self.setPos(newpos)

        #TODO pirate interaction
        """
        for o in self._control.content_matrix[self._pos]:
            if o.getType() == "pirate":
                o.attackTarget(self)
        """

        # do as in startmove but only for now pos
        if self._pos in globalworld.harbours:
            harbour = self._control.getObjectsByPos(self._pos, tag="harbour")[0].getUuid()
            self._tradingtowns.append(harbour)
            tmptowns = ownerobj.getTownsFromHarbour(self._pos)
            if len(tmptowns)>0:
                # store this so the player can later trade stuff in this harbour
                self._tradingtowns += tmptowns
                if not self._usedboost:
                    self._currrange += self._boost
                    self._usedboost = True

        if globalworld.homecountry[ownerobj.getColor()] == self._pos:
            self._tradingtowns += [self._owner]

        # reduce range by 1
        self._currrange -= 1
        #TODO remove caching of valid targets I think
        self.computeValidTargets(oldpos=oldpos)
        if self._currrange <= 0:
            self.endMove()

    def endMove(self) -> None:
        self._moving = False
        if len(self._tradingtowns)==0:
            if not self._control.freeObjectLock(self.getUuid()):
                #TODO raise something
                self.getControl().printWarning("Tried to free a lock without owning it. That should not be possible!!")
        # should display a popup for each harbour passed (in correct order) to allow to trade stuff
        # (also needs to consider hometown)
        #TODO somhow use a lock to stop all interactions besides trades to do by this ship as this has to be done now!!

    def isMoving(self) -> bool:
        return self._moving

    def getCurrTradingTown(self) -> Optional[str]:
        if len(self._tradingtowns) == 0:
            return None
        return self._tradingtowns[0]

    def completeCurrTradingTown(self) -> None:
        if len(self._tradingtowns) == 0:
            raise TemporaryInvalidAction
        del self._tradingtowns[0]
        if len(self._tradingtowns)==0:
            try:
                self._control.freeObjectLock(self.getUuid())
            except TemporaryInvalidAction:
                self.getControl().printWarning("Tried to free a lock without owning it. That should not be possible!!")
            except:
                self.getControl().printError("Unexpected error: "+ sys.exc_info()[0])
                raise

    def canTradeWith(self, obj: Optional[str]) -> bool:
        return self.isAvailable() and len(self._tradingtowns) > 0 and obj == self._tradingtowns[0]

    def getAttacks(self, target: str) -> List[Attack]:
        return []

    def receiveAttacks(self, attacks: List[Attack]) -> None:
        #TODO check who attacks and make loose ressources
        self.getControl().printDev("TODO")

    def getValidAttackTargets(self) -> List[str]:
        return []

    def save(self) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        res["currrange"] = self._currrange
        res["moving"] = self._moving
        res["usedboost"] = self._usedboost
        res["tradingtowns"] = self._tradingtowns
        res["validtargets"] = self._validtargets
        res = combineDicts(res, AbstractMapObject.save(self))
        res = combineDicts(res, RessourceStorage.save(self))
        res = combineDicts(res, Colored.save(self))
        return res

    def delete(self) -> None:
        AbstractMapObject.delete(self)


# this is kind of a border case (not really a mapobject since it can not be displayed on its own. need to decide about that
class BattleShip(AbstractMapObject, Colored, Attackable):
    def __init__(self, uuid: str, control: GameControlInterface, type: Optional[str] = None, restore: Optional[Dict[str, Any]] = None) -> None:
        AbstractMapObject.__init__(self, uuid=uuid, control=control, type=type, restore=restore)
        Colored.__init__(self, restore=restore)
        self._fleet: Optional[str]
        if restore is None:
            # normal case
            self._fleet = None
            self._health = shiphealth[self._type]
            self._armor = shiparmor[self._type]
            self._damage = shipdamage[self._type]
            self._range = shiprange[self._type]
        else:
            # restoring from file
            self._fleet = restore["fleet"]
            self._health = restore["health"]
            self._armor = shiparmor[self._type]
            self._damage = shipdamage[self._type]
            self._range = shiprange[self._type]

        # TODO
        self._attackblockable = False
        self._attackstricthit = True

    def setOwner(self, owner: Optional[str]) -> None:
        AbstractMapObject.setOwner(self, owner)
        if not owner is None:
            ownerobj = cast('Player', self._control.getObjectByUuid(owner))
            self.setColor(ownerobj.getColor())
            self.setIcon(os.path.join(imagefolder_rescaled, self.getColor(), allicons[self._type]))

    def getFleet(self) -> Optional[str]:
        return self._fleet

    def setFleet(self, fleet: Optional[str], fleetpos: int = -1) -> None:
        self._fleet = fleet

    def getFleetPos(self) -> int:
        return self._control.getObjectByUuid(self.getFleet()).getPos()

    def getDefense(self) -> int:
        return self._armor

    def getAttack(self) -> int:
        return self._damage

    def getHealt(self) -> int:
        return self._health

    def getRange(self) -> int:
        return self._range

    def getAttackBlockable(self) -> bool:
        return self._attackblockable

    def getAttackStrictHit(self) -> bool:
        return self._attackstricthit

    def getAttacks(self, target: str) -> List[Attack]:
        res: List[Attack] = []
        targetobj = self.getControl().getObjectByUuid(target)
        if computeDistance(targetobj.getPos(), self.getFleetPos(), connectivity = globalworld.connectivity_matrix[1,:,:]) <= self.getRange():
            rng = self.getControl().requestAdditionalRandomNumbers(2)
            res.append(Attack(attacker = self.getUuid(), targetuuid = target, target = rng[0], strength = rng[1]+self.getAttack(), stricthit = self.getAttackStrictHit(), blockable = self.getAttackBlockable()))
        return res

    def receiveAttacks(self, attacks: List[Attack]) -> None:
        effectivearmor = self._armor
        for e in self.getEffects():
            if e.getType()=="defense":
                effectivearmor+=e.getArgs()["value"]
        for attack in attacks:
            if attack.strength > effectivearmor:
                self._health -= 1
                if self._health <= 0:
                    self.getControl().printDev("TODO")
                    return
                    # must put somewhere for repair
                    # need to think about this carefully, because ships must attack even if they get destroyed during that battle (single round battle between fleets) maybe we can deepcopy attackers
                    # actually we dont have to because we can simply create all actions first before executing

    def getValidAttackTargets(self) -> List[str]:
        #TODO check all targets in ships range
        self.getControl().printDev("TODO")
        return []

    def save(self) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        res["fleet"] = self._fleet
        res["health"] = self._health
        res = combineDicts(res, AbstractMapObject.save(self))
        res = combineDicts(res, Colored.save(self))
        return res

    def delete(self) -> None:
        AbstractMapObject.delete(self)
        fleet = self.getFleet()
        if not fleet is None:
            cast(WarFleet, self._control.getObjectByUuid(fleet)).removeShip(self.getUuid())


class PirateShip(AbstractMapObject, Attackable):
    def __init__(self, uuid: str, control: GameControlInterface, restore: Optional[Dict[str, Any]] = None) -> None:
        AbstractMapObject.__init__(self, uuid=uuid, control=control, type="pirate", restore=restore)

    def move(self, target: int) -> None:
        # we need to search for patterns somehow so we know which field to go next (we only know direction but not to what nr this relates on the map)
        # also needs to attack ships if possible
        # direction is
        #   6   1
        # 5       2
        #   4   3
        # find newpos for target
        newpos = globalworld.pirateconnectivity[self._pos, target-1]
        # set new position
        self.setPos(newpos)

        # here pirate should attack ships maybe
        self.attack()

    def newRound(self) -> None:
        AbstractMapObject.newRound(self)
        self._control.moveMapObject(self.getUuid(), self._control.getRandomState().getPrimaryRandom())
        self._control.moveMapObject(self.getUuid(), self._control.getRandomState().getSecondaryRandom())

    def attack(self):
        for o in self.getValidAttackTargets():
            self.getControl().attackMapObject(self.getUuid(), o)

    def getAttacks(self, target: str) -> List[Attack]:
        rng = self.getControl().requestAdditionalRandomNumbers(1)
        a = Attack(attacker = self.getUuid(), targetuuid = target, target=rng[0], strength=9999)
        return [a]

    def receiveAttacks(self, attacks: List[Attack]) -> None:
        #TODO simply sink
        self.getControl().printDev("TODO")

    def getValidAttackTargets(self) -> List[str]:
        res: List[str] = []
        for o in self.getControl().getObjectsByPos(self.getPos()):
            if isinstance(o, Attackable) and not o == self:
                res.append(o.getUuid())
        #TODO here we will need to add sepcial values defined by map
        return res

    def getValidTargets(self) -> List[int]:
        return [1,2,3,4,5,6]

    def save(self) -> Dict[str, Any]:
        res = AbstractMapObject.save(self)
        return res

    def delete(self) -> None:
        AbstractMapObject.delete(self)


class WarFleet(AbstractMapObject, Attackable):
    def __init__(self, uuid: str, control: GameControlInterface, num: int = 1, restore: Optional[Dict[str, Any]] = None) -> None:
        AbstractMapObject.__init__(self, uuid=uuid, control=control, type="fleet", restore=restore)
        self._validtargets: List[int]
        self._attacktargets: List[str]
        self._ships: Dict[int, Optional[str]]
        if restore is None:
            # normal case
            self._plan = os.path.join(os.path.join(imagefolder_rescaled, "plans"), "Kampfflotte"+str(num)+".png")
            self._size = 6
            self._roundstartpos = self._pos
            self._num = num
            self._ships = {
                0: None,
                1: None,
                2: None,
                3: None,
                4: None,
                5: None
            }
            self._currrange = 5
            self._hasattacked = False
            self._validtargets = []
            self._attacktargets = []
        else:
            # restore from file
            self._roundstartpos = restore["roundstartpos"]
            self._num = restore["num"]
            self._plan = os.path.join(os.path.join(
                imagefolder_rescaled, "plans"), "Kampfflotte"+str(self._num)+".png")
            self._size = restore["size"]
            #we need to convert the keys to int, otherwise will get key errors later
            tmpships = restore["ships"]
            self._ships={}
            for k, v in tmpships.items():
                self._ships[int(k)] = v
            self._currrange = restore["currrange"]
            self._hasattacked = restore["hasattacked"]
            self._validtargets = restore["validtargets"]
            self._attacktargets = restore["attacktargets"]

    def newRound(self) -> None:
        AbstractMapObject.newRound(self)
        self._currrange = 5
        self._roundstartpos = self._pos
        self._hasattacked = False

    def getSize(self) -> int:
        return self._size

    def getShip(self, pos: int) -> Optional[str]:
        return self._ships[pos]

    # adds the ship to pos of the fleet
    # returns True on success and False otherwise
    def addShip(self, ship: str, pos: int) -> bool:
        # fleet should also check if it is close to a harbour. Otherwise cant put ships on it
        if self._ships[pos] == None and self.isEditable():
            self._ships[pos] = ship
            return True
        else:
            return False

    def removeShip(self, ship: str) -> bool:
        if not self.isEditable():
            return False
        
        for k,v in self._ships.items():
            if v == ship:
                self._ships[k]=None
                return True
        return False

    def getPlan(self) -> str:
        return self._plan

    def getNum(self) -> int:
        return self._num

    def setNum(self, num: int) -> None:
        self._num = num

    def getCurrRange(self) -> int:
        return self._currrange

    def isEditable(self) -> bool:
        if self._owner is None:
            raise RuntimeError("Tried to call isEditable without having owner set. This is invalid!")
        # check if not in water or next to harbour
        ownerobj = cast('Player', self._control.getObjectByUuid(self._owner))
        coastfields: List[int] = []
        harbours = ownerobj.getOwnedHarbours()
        # harbours
        for h in harbours:
            hobj = cast(AbstractMapObject, self._control.getObjectByUuid(h))
            coastfields += globalworld.fleetplacement[hobj.getPos()]
        # hometown
        coastfields += globalworld.fleetplacement[globalworld.homecountry[ownerobj.getColor()]]
        if self._pos == -1 or self._pos in coastfields:
            return True
        else:
            return False

    def move(self, pos: int) -> None:
        # we somehow need to add rules for attacking only if moved towards target
        if self._pos == -1:
            # here it was not in the water before so we need to place it newly (and set remaining movement to 0)
            self.setPos(pos)
            self._currrange = 0
        else:
            # remove fleet from water
            if pos in globalworld.fleetplacement.keys():
                self.setPos(-1)
                self._currrange = 0
            # move
            else:
                if self._roundstartpos == self._pos:
                    #moving for the first time check for range extending effects
                    for e in self.getEffects():
                        eobj = self.getControl().getObjectByUuid(e)
                        if eobj.getType()=="windrad":
                            self._currrange += eobj.getArgs()["fleet"]
                            
                self.setPos(pos)
                self._currrange -= 1
                #TODO pirate interaction
                # this if for pirates to attack if there is one
                """
                for o in self._control.content_matrix[self._pos]:
                    if o.getType() == "pirate":
                        o.attackTarget(self)
                """

        self.computeValidTargets()

    def getValidTargets(self) -> List[int]:
        if self._owner is None:
            raise RuntimeError("Tried to call getValidTargets without having owner set. This is invalid!")
        if cast('Player', self._control.getObjectByUuid(self._owner)).isActive():
            return self._validtargets
        else:
            return []

    def computeValidTargets(self, oldpos: Optional[int] = None) -> None:
        if self._owner is None:
            raise RuntimeError("Tried to call computeValidTargets without having owner set. This is invalid!")
        res: List[int] = []
        hasships = False
        for _, v in self._ships.items():
            if not v is None:
                hasships = True
                break
        # can only be put in water if has at least one ship
        if self._currrange > 0 and hasships:
            ownerobj = cast('Player', self._control.getObjectByUuid(self._owner))
            if self._pos == -1:
                harbours = ownerobj.getOwnedHarbours()
                # harbours
                for h in harbours:
                    hobj = cast(AbstractMapObject, self._control.getObjectByUuid(h))
                    res += globalworld.fleetplacement[hobj.getPos()]
                # hometown
                res += globalworld.fleetplacement[globalworld.homecountry[ownerobj.getColor()]]
            else:
                # I have no clue why this gives a nested list...but it does, so we reshape
                tmpvalidtargets: List[int] = np.argwhere(globalworld.connectivity_matrix[1, self._pos, :] == 1).reshape(-1)
                for valt in tmpvalidtargets:
                    res.append(valt)

                # maybe harbours to take out of water
                coastfields: List[int] = []
                harbours = ownerobj.getOwnedHarbours()
                # harbours
                for h in harbours:
                    hobj = cast(AbstractMapObject, self._control.getObjectByUuid(h))
                    coastfields += globalworld.fleetplacement[hobj.getPos()]
                coastfields += globalworld.fleetplacement[globalworld.homecountry[ownerobj.getColor()]]
                if self._pos in coastfields:
                    res.append(globalworld.homecountry[ownerobj.getColor()])
                    for h in harbours:
                        hobj = cast(AbstractMapObject, self._control.getObjectByUuid(h))
                        res.append(hobj.getPos())

        for r in res:
            for o in self._control.getObjectsByPos(r):
                if o.getType() in ["fleet", "pirate"] and o.getOwner() != self.getOwner():
                    res.remove(r)
                    continue
        self._validtargets = res


    def getAttackTargets(self) -> List[str]:
        if self._owner is None:
            raise RuntimeError("Tried to call getAttackTargets without having owner set. This is invalid!")
        if self._control.getObjectByUuid(self._owner).isActive():
            return self._attacktargets
        else:
            return []

    def getAttacks(self, target: str) -> List[Attack]:
        #TODO we should request all random numbers at once otherwise animation will take forever I think
        res: List[Attack] = []
        targetobj = self.getControl().getObjectByUuid(target)
        if targetobj.getType() == "fleet":
            for _, s in self._ships.items():
                if not s is None:
                    sobj = cast(BattleShip, self.getControl().getObjectByUuid(s))
                    res += sobj.getAttacks(target)
        elif targetobj.getType() == "pirate":
            #pirate will always sink no matter the attack
            res.append(Attack(self.getUuid(), target, strength=9999))
        elif targetobj.getType() in buildingtypes:
            rng = self.getControl().requestAdditionalRandomNumbers(2)
            techsum = 0 #TODO
            res.append(Attack(self.getUuid(), target, strength=rng[0]+rng[1]+techsum))
        elif targetobj.getType() in tradingshiptypes:
            rng = self.getControl().requestAdditionalRandomNumbers(1)
            res.append(Attack(self.getUuid(), target, strength=rng[0]))
        else:
            self.getControl().printWarning("Dont know how to create attacks for target of type "+targetobj.getType())
        return res
            

    def receiveAttacks(self, attacks: List[Attack]) -> None:
        for attack in attacks:
            candidatepos = self.handleBlockable(attack, attack.target)
            if candidatepos == -1:
                if attack.stricthit:
                    for i in range(1, 5):
                        candidatepos = self.handleBlockable(attack, (attack.target + i) % 6)
                        if candidatepos != -1:
                            sobj = cast(BattleShip, self.getControl().getObjectByUuid(self._ships[candidatepos]))
                            sobj.receiveAttacks([attack])
            else:
                sobj = cast(BattleShip, self.getControl().getObjectByUuid(self._ships[candidatepos]))
                sobj.receiveAttacks([attack])

    def getValidAttackTargets(self) -> List[str]:
        res: List[str] = []
        for i in range(self.getSize()):
            s = self.getShip(i)
            if not s is None:
                sobj = cast(BattleShip, self.getControl().getObjectByUuid(s))
                for newtarget in sobj.getValidAttackTargets():
                    if not newtarget in res:
                        res.append(newtarget)
        return res

    #helperfunction
    @classmethod
    def handleBlockable(self, attack: Attack, candidatepos: int) -> int:
        if candidatepos in [1,3,5] and not self._ships[candidatepos] is None:
            return candidatepos
        elif not self._ships[candidatepos] is None or (attack.blockable and not self._ships[candidatepos-1] is None):
            if attack.blockable and not self._ships[candidatepos-1] is None:
                return candidatepos-1
            else:
                return candidatepos
        else:
            return -1


    def save(self) -> Dict[str, Any]:
        res: Dict[str, Any] = {}
        res["roundstartpos"] = self._roundstartpos
        res["num"] = self._num
        res["size"] = self._size
        res["ships"] = self._ships
        res["currrange"] = self._currrange
        res["hasattacked"] = self._hasattacked
        res["validtargets"] = self._validtargets
        res["attacktargets"] = self._attacktargets
        res = combineDicts(res, AbstractMapObject.save(self))
        return res

    def delete(self) -> None:
        AbstractMapObject.delete(self)
        for _, v in self._ships.items():
            if not v is None:
                vobj = cast(BattleShip, self._control.getObjectByUuid(v))
                vobj.setFleet(None)