from schiffli_control.mapobjects.mapobject import AbstractMapObject

map_object2class = {}
for cl in AbstractMapObject.__subclasses__():
    map_object2class[cl.__name__] = cl
