import os
import random

from typing import Optional, Dict, Any, List

from schiffli_util import refinementbyharbour, refinementinressources, refinementoutressources, refinementgraphics, imagefolder_rescaled, combineDicts

from schiffli_control.interfaces import GameControlInterface
from schiffli_control.fragments import IsA

from schiffli_control.mapobjects.mapobject import AbstractMapObject

class Harbour(AbstractMapObject):
    def __init__(self, uuid: str, control: GameControlInterface, type: Optional[str] = None, restore: Optional[Dict[str, Any]] = None) -> None:
        AbstractMapObject.__init__(self, uuid, control, type=type, restore=restore)
        if restore is None:
            # normalcase
            # somehow store what can be refined to what odds/conditions
            refinementnames = refinementbyharbour[self._type]
            self._refinements: List[Refinement] = []
            for r in refinementnames:
                self._refinements.append(Refinement(r))
        else:
            # loading from file
            refinementnames = refinementbyharbour[self._type]
            self._refinements = []
            for r in refinementnames:
                self._refinements.append(Refinement(r))

    def getRefinements(self):
        return self._refinements
    
    # currently unused
    # Maybe should use this instead, question is if there are any effects from harbour that influence refinement
    def refine(self, ship, refinementidx):
        # return resulting ressources when <ressources> are refined here
        self.getControl().printDev("TODO")

    def save(self) -> Dict[str, Any]:
        res: Dict[str, Any] ={}
        res = combineDicts(res, AbstractMapObject.save(self))
        return res

    def delete(self) -> None:
        #TODO remove target from missions
        AbstractMapObject.delete(self)


class Refinement(IsA):
    def __init__(self, name: str) -> None: 
        IsA.__init__(self, name)
        self._inressources = refinementinressources[self._type]
        self._outressources = refinementoutressources[self._type]
        self._graphic = os.path.join(os.path.join(imagefolder_rescaled, "refinementgraphics"), refinementgraphics[self._type])

    def getInRessources(self) -> Dict[str, int]:
        return self._inressources

    def getOutRessources(self, rand: int) -> Dict[str, int]:
        res = {}
        for k, v in self._outressources.items():
            if rand in v:
                res[k] = 1 #TODO is it possible at all to get more than one from refinement
        return res

    def getGraphic(self) -> str:
        return self._graphic
