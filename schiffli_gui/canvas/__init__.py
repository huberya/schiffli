#THIS FILE IS REQUIRED TO INCLUDE THIS DIRECTORY AS A MODULE
#DO NOT DELETE THIS FILE

#CAREFUL ORDER IS IMPORTANT HERE
#Each block depends on (some modules) from the upper block and no dependencies inside one block

from schiffli_gui.canvas.abstractcanvas import AbstractCanvas, AbstractRessourceCanvas

from schiffli_gui.canvas.actioncanvas import ActionCanvas
from schiffli_gui.canvas.battleshipcanvas import BattleShipCanvas
from schiffli_gui.canvas.confirmcanvas import ConfirmCanvas
from schiffli_gui.canvas.diplomacycanvas import DiplomacyCanvas
from schiffli_gui.canvas.infocanvas import InfoCanvas
from schiffli_gui.canvas.missioncanvas import MissionCanvas
from schiffli_gui.canvas.payrescanvas import PayResCanvas
from schiffli_gui.canvas.pickrescanvas import PickResCanvas
from schiffli_gui.canvas.politicscanvas import PoliticsCanvas
from schiffli_gui.canvas.ressourcecanvas import RessourceCanvas
from schiffli_gui.canvas.roundcontrolcanvas import RoundControlCanvas
from schiffli_gui.canvas.sciencecanvas import ScienceCanvas
from schiffli_gui.canvas.sciencecardcanvas import ScienceCardCanvas
from schiffli_gui.canvas.shipvendorcanvas import ShipVendorCanvas
from schiffli_gui.canvas.tmpcanvas import TmpInfoCanvas, TmpPointCanvas, TmpResCanvas
