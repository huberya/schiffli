
import os

from schiffli_util import *
from schiffli_util.tkimports import *

from schiffli_gui.canvas.abstractcanvas import AbstractCanvas


class ConfirmCanvas(AbstractCanvas):
    def __init__(self, mainwindow):
        AbstractCanvas.__init__(self, mainwindow)

    def create(self, event=None, text="Bestätigen", function=None):
        self.setSize(300, 160)
        AbstractCanvas.create(self, int(self.app.winfo_pointerx()), int(self.app.winfo_pointery()))

        self.function=function

        #put text
        self.canvas.create_text(5, 5, width=290, anchor=tk.NW, text=text)

        #place a yes and a no button
        #think about how to continue flow #pass a function to call with yes/no maybe (or rather no basically doesnt need to do anything)
        #maybe call function that used to call with confirmed=True
        # button
        bBG = self.canvas.create_rectangle(
            25, 125, 125, 160, fill="grey40")
        b = self.canvas.create_text(
            75, 130, anchor=tk.N, text="Ja", font=tk.font.Font(size=12))
        self.canvas.tag_bind(bBG, "<Button-1>", self.yesclick)
        self.canvas.tag_bind(b, "<Button-1>", self.yesclick)

        bBG = self.canvas.create_rectangle(
            175, 125, 275, 160, fill="grey40")
        b = self.canvas.create_text(
            225, 130, anchor=tk.N, text="Nein", font=tk.font.Font(size=12))
        self.canvas.tag_bind(bBG, "<Button-1>", self.noclick)
        self.canvas.tag_bind(b, "<Button-1>", self.noclick)

    def yesclick(self, event=None):
        self.destroy()
        if not self.function is None:
            self.function()

    def noclick(self, event=None):
        #I dont think we need to do anything here
        self.destroy()

    def destroy(self, event=None):
        AbstractCanvas.destroy(self)
