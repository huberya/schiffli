from PIL import Image, ImageTk

from schiffli_util.tkimports import *

from schiffli_gui.canvas.abstractcanvas import AbstractCanvas


class ScienceCardCanvas(AbstractCanvas):
    def __init__(self, mainwindow):
        AbstractCanvas.__init__(self, mainwindow)

    def create(self, event=None):
        if self.prevent_reopen:
            return
        nwidth=max(1, (len(self._control.getActivePlayerObj().getScienceCards())+1)//2)
        self.setSize(int(1.5*nwidth*self.iconxsize), 10*self.iconysize)
        sizex, sizey = self.getSize()

        AbstractCanvas.create(self, int(self.mainwindow.boardxdim/6+sizex/2), int(self.mainwindow.windowysize-sizey/2))
        self.repaint()

        self.canvas.bind("<Leave>", self.destroy)

    def repaint(self):
        if self.canvas is None:
            return False  # here is not visible so we don't need to do anything

        self.canvas.delete("all")
        self.imgs = []
        self.cards = []
        for idx, scuuid in enumerate(self._control.getActivePlayerObj().getScienceCards()):
            sc = self._control.getObjectByUuid(scuuid)
            img = ImageTk.PhotoImage(Image.open(sc.getCard()))
            self.canvas.create_image(int(
                1.5*(idx//2)*self.iconxsize), int(idx%2*3*self.iconysize), anchor=tk.NW, image=img, tags=("card", sc.getUuid()))
            self.imgs.append(img)

        self.canvas.tag_bind("card", "<Button-1>", self.playcard)
    
    def playcard(self, event):
        scuuid = self.canvas.gettags(self.canvas.find_closest(event.x, event.y))[1]
        self.mainwindow.confirmationcanvas.create(text="Diese Aktion verbraucht diese Karte. Die Karte jetzt einsetzen?", function = lambda card = scuuid: self._control.activateScienceCard(card))
        #TODO use control to activate with confirm like below
        #a = Action("playsciencecard", subject=self._control.getActivePlayer(), card=sc)
        

    def destroy(self, event=None):
        self.setPreventReopen()
        self.app.after(100, self.resetPreventReopen)
        AbstractCanvas.destroy(self)
        self.cards = []
