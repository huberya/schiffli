from PIL import Image, ImageTk

from schiffli_util.tkimports import *

from schiffli_gui.canvas.abstractcanvas import AbstractCanvas, AbstractRessourceCanvas

# TODO maybe put prevent reopen in super
class RessourceCanvas(AbstractRessourceCanvas):
    def __init__(self, mainwindow):
        AbstractRessourceCanvas.__init__(self, mainwindow)

    def create(self, event=None):
        if self.prevent_reopen:
            return
        nuniqueres = 0
        for _, amount in self._control.getActivePlayerObj().getRessources().items():
            if amount > 0:
                nuniqueres += 1
        if nuniqueres<1:
            nuniqueres=1

        self.setSize(int(1.5*nuniqueres*self.iconxsize), 10*self.iconysize)
        sizex, sizey = self.getSize()
        AbstractCanvas.create(self, int(sizex/2), int(self.mainwindow.windowysize-sizey/2))

        self.repaint()

        self.canvas.bind("<Leave>", self.destroy)

    def repaint(self):
        if self.canvas is None:
            return False  # here is not visible so we don't need to do anything

        self.canvas.delete("all")
        self.imgs = []
        nuniqueres = 0
        player = self._control.getActivePlayerObj()
        for rtype, amount in player.getRessources().items():
            if amount > 0:
                # draw the ressources
                if amount > 12:
                    xoffset = 0.1
                elif amount > 6:
                    xoffset = 0.25
                else:
                    xoffset = 0.5
                for i in range(amount):
                    img = ImageTk.PhotoImage(Image.open(player.getResCard(rtype)))
                    self.canvas.create_image(int(1.5*nuniqueres*self.iconxsize), int(i*xoffset*self.iconysize), anchor=tk.NW, image=img)
                    self.imgs.append(img)
                nuniqueres += 1

    def destroy(self, event=None):
        self.setPreventReopen()
        self.app.after(100, self.resetPreventReopen)

        AbstractRessourceCanvas.destroy(self)
