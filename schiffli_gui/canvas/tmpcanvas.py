import os

from PIL import Image, ImageTk

from schiffli_util import buildingtypes, globalworld, imagefolder_rescaled
from schiffli_util.tkimports import * 

from schiffli_gui.canvas.abstractcanvas import AbstractCanvas

class TmpResCanvas(AbstractCanvas):
    def __init__(self, mainwindow):
        AbstractCanvas.__init__(self, mainwindow)

    def create(self, mapobject, type="ressources"):
        if type == "ressources":
            nuniqueres = mapobject.getNUniqueRessources()
            # positioncalculation
            if mapobject.getType() in buildingtypes:
                posxy = self.mainwindow.uvToCanvas(globalworld.buildingposition[mapobject.getPos()])
            else:
                posxy = self.mainwindow.tileToPos(mapobject.getPos())
            self.setSize(2*self.iconxsize, max(nuniqueres, 1)*self.iconysize)
            AbstractCanvas.create(self, posxy.x, posxy.y, _in=self.mainwindow.canvas)
            self.canvas.bind("<Leave>", self.destroy)
            self._type = type
            # draw the ressources from that building
            nuniqueres = 0
            for rtype, amount in mapobject.getRessources().items():
                if amount > 0:
                    if amount > 8:
                        xoffset = 0.1
                    elif amount > 4:
                        xoffset = 0.25
                    else:
                        xoffset = 0.5
                    # draw the ressources
                    for i in range(amount):
                        img = ImageTk.PhotoImage(Image.open(mapobject.getResIcon(rtype)))
                        self.canvas.create_image(int(i*xoffset*self.iconxsize), nuniqueres*self.iconysize, anchor=tk.NW, image=img)
                        self.imgs.append(img)
                    nuniqueres += 1

        elif type == "pirateattack":
            nuniqueres = mapobject.getNUniqueRessources()
            self.setSize(2*self.iconxsize, max(nuniqueres, 1)*self.iconysize)
            # positioncalculation
            posxy = self.mainwindow.tileToPos(mapobject.getPos())
            AbstractCanvas.create(self, posxy.x, posxy.y, _in=self.mainwindow.canvas)
            self.reskeys = []
            self.nressources = 0
            self.deleted = 0
            self._type = type
            
            # draw the ressources from that building
            nuniqueres = 0
            for rtype, currres in mapobject.getRessources().items():
                if currres.getAmount() > 0:
                    self.reskeys.append(rtype)
                    # draw the ressources
                    if currres.getAmount() > 8:
                        xoffset = 0.1
                    elif currres.getAmount() > 4:
                        xoffset = 0.25
                    else:
                        xoffset = 0.5
                    for i in range(currres.getAmount()):
                        img = ImageTk.PhotoImage(Image.open(currres.getIcon()))
                        self.canvas.create_image(int(
                            i*xoffset*self.iconxsize), nuniqueres*self.iconysize, anchor=tk.NW, image=img, tags="ressource")
                        self.imgs.append(img)
                        self.nressources += 1
                    nuniqueres += 1

            self.canvas.tag_bind("ressource", "<Button-1>", lambda event,
                                 mapobject=mapobject: self.deleteres(event, mapobject))
            if nuniqueres == 0:
                self.destroy()

    def deleteres(self, event, mapobject):
        if event.y//self.iconysize >= len(self.reskeys):
            # can happen after having deleted some that we have an empty row->return
            return

        rtype = self.reskeys[event.y//self.iconysize]
        mapobject.removeRessource(rtype)
        self.deleted += 1
        if self.deleted >= self.nressources/2:
            # deleted enough, we are done
            self.destroy()
        else:
            self.repaint(mapobject)

    def repaint(self, mapobject):
        if self._type == "pirateattack":
            # should probably do this more efficiently
            self.canvas.delete("all")
            self.imgs = []
            self.reskeys = []
            nuniqueres = 0
            for rtype, currres in mapobject.getRessources().items():
                if currres.getAmount() > 0:
                    self.reskeys.append(rtype)
                    if currres.getAmount() > 8:
                        xoffset = 0.1
                    elif currres.getAmount() > 4:
                        xoffset = 0.25
                    else:
                        xoffset = 0.5
                    # draw the ressources
                    for i in range(currres.getAmount()):
                        img = ImageTk.PhotoImage(Image.open(currres.getIcon()))
                        self.canvas.create_image(int(
                            i*xoffset*self.iconxsize), nuniqueres*self.iconysize, anchor=tk.NW, image=img, tags="ressource")
                        self.imgs.append(img)
                    nuniqueres += 1

        # others do not have repaint yet

    def destroy(self, event=None):
        AbstractCanvas.destroy(self)
        self.reskeys = []

class TmpInfoCanvas(AbstractCanvas):
    def __init__(self, mainwindow):
        AbstractCanvas.__init__(self, mainwindow)

    def create(self, mapobject, type="ships"):
        if type == "ships":
            # paint ships from fleet (and fleet plan)
            # fleet plan itself
            posxy = self.mainwindow.tileToPos(mapobject.getPos())
            self.setSize(3*self.iconxsize, 4*self.iconysize)
            AbstractCanvas.create(self, posxy.x, posxy.y, _in=self.mainwindow.canvas)
            self.canvas.bind("<Leave>", self.destroy)
            self._type = type

            img = ImageTk.PhotoImage(Image.open(mapobject.getPlan()))
            self.canvas.create_image(0, 0, anchor=tk.NW, image=img)
            self.imgs.append(img)
            for i in range(mapobject.getSize()):
                suuid = mapobject.getShip(i)
                if not suuid is None:
                    s=self._control.getObjectByUuid(suuid)
                    img = ImageTk.PhotoImage(Image.open(s.getIcon()))
                    self.canvas.create_image((0.4+(1.1*(i % 2)))*self.iconxsize, (0.7+(i//2))*self.iconysize, anchor=tk.NW, image=img)  # probably need to add tags here somehow
                    self.imgs.append(img)

        elif type == "harbourinfo":
            posxy = self.mainwindow.tileToPos(mapobject.getPos())
            #size?
            self.setSize(3*self.iconxsize, 4*self.iconysize)
            AbstractCanvas.create(self, posxy.x, posxy.y, _in=self.mainwindow.canvas)
            self.canvas.bind("<Leave>", self.destroy)
            self._type = type

            posxy = self.mainwindow.tileToPos(mapobject.getPos())
            for idx, r in enumerate(mapobject.getRefinements()):
                img = ImageTk.PhotoImage(Image.open(r.getGraphic()))
                self.canvas.create_image(1.5*self.iconxsize, idx*self.iconysize+45, anchor=tk.N, image=img)
                self.imgs.append(img)

    def repaint(self, mapobject):
        #no repaint needed atm
        pass

    def destroy(self, event=None):
        AbstractCanvas.destroy(self)

class TmpPointCanvas(AbstractCanvas):
    def __init__(self, mainwindow):
        AbstractCanvas.__init__(self, mainwindow)

        #create the empty canvas next to the scienceoverview canvas
        self.setSize(3*self.iconxsize, mainwindow.windowysize-mainwindow.boardydim-20)
        sizex, sizey = self.getSize()
        AbstractCanvas.create(self, int(2*mainwindow.boardxdim/6+sizex/2), int(mainwindow.boardydim+sizey/2))

        #TODO: put icons and place text
        img = ImageTk.PhotoImage(Image.open(os.path.join(imagefolder_rescaled, "Icon_SciencePoints.png")))
        self.canvas.create_image(5, 20, anchor=tk.NW, image=img)
        self.imgs.append(img)

        img = ImageTk.PhotoImage(Image.open(os.path.join(imagefolder_rescaled, "Icon_PoliticsPoints.png")))
        self.canvas.create_image(5, 20+1.5*self.iconysize, anchor=tk.NW, image=img)
        self.imgs.append(img)

        img = ImageTk.PhotoImage(Image.open(os.path.join(imagefolder_rescaled, "Icon_RessourcePoints.png")))
        self.canvas.create_image(5, 20+3*self.iconysize, anchor=tk.NW, image=img)
        self.imgs.append(img)

        self.sciencetext = self.canvas.create_text(10+self.iconxsize, 20+0.3*self.iconysize, anchor=tk.NW, text="0", font=tk.font.Font(size=20))

        self.politicstext = self.canvas.create_text(10+self.iconxsize, 20+1.8*self.iconysize, anchor=tk.NW, text="0", font=tk.font.Font(size=20))

        self.ressourcetext = self.canvas.create_text(10+self.iconxsize, 20+3.3*self.iconysize, anchor=tk.NW, text="0", font=tk.font.Font(size=20))

    def create(self):
        pass
    
    def updateState(self, action=None):
        self.set("science", self._control.getActivePlayerObj().getTmpPoints("science"))
        self.set("politics", self._control.getActivePlayerObj().getTmpPoints("politics"))
        self.set("ressource", self._control.getActivePlayerObj().getTmpPoints("ressource"))

    def set(self, type, num):
        if type=="science":
            self.canvas.itemconfig(self.sciencetext, text = str(num)) 
        elif type=="politics":
            self.canvas.itemconfig(self.politicstext, text = str(num))
        elif type=="ressource":
            self.canvas.itemconfig(self.ressourcetext, text = str(num))
        else:
            self.mainwindow.printWarning("unrecognised type:"+str(type))

    def destroy(self):
        pass