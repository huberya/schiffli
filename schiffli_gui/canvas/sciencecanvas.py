import os

from PIL import Image, ImageTk
from shapely.geometry import Point

from schiffli_control import ScienceLevel
from schiffli_util import imagefolder_rescaled, colordictionary
from schiffli_util.tkimports import *

from schiffli_gui.canvas.abstractcanvas import AbstractCanvas


class ScienceCanvas(AbstractCanvas):
    def __init__(self, mainwindow):
        AbstractCanvas.__init__(self, mainwindow)
        self.plan = os.path.join(imagefolder_rescaled, "plans", "scienceplan.png")
        self.buycard1=os.path.join(imagefolder_rescaled, "cards", "cardback_wiss1.png")
        self.buycard2=os.path.join(imagefolder_rescaled, "cards", "cardback_wiss2.png")
        self.buycard3=os.path.join(imagefolder_rescaled, "cards", "cardback_wiss3.png")
        self.locked=False

        #think about best representation
        self.fields ={
            #fill by clicking
            "0_0": Point(0.2994791666666667, 0.8287037037037037),
            "1_0": Point(0.2282986111111111, 0.7631172839506173),
            "1_1": Point(0.3003472222222222, 0.7584876543209876),
            "1_2": Point(0.3693576388888889, 0.7615740740740741),
            "2_0": Point(0.1853298611111111, 0.6959876543209876),
            "2_1": Point(0.30078125, 0.6983024691358025),
            "2_2": Point(0.4188368055555556, 0.6983024691358025),
            "3_0": Point(0.15321180555555555, 0.5941358024691358),
            "3_1": Point(0.3077256944444444, 0.591820987654321),
            "3_2": Point(0.4635416666666667, 0.5949074074074074),
            "4_0": Point(0.14930555555555555, 0.4930555555555556),
            "4_1": Point(0.3051215277777778, 0.4930555555555556),
            "4_2": Point(0.4578993055555556, 0.49382716049382713),
            "5_0": Point(0.10850694444444445, 0.4529320987654321),
            "5_1": Point(0.1957465277777778, 0.44907407407407407),
            "5_2": Point(0.2608506944444444, 0.44907407407407407),
            "5_3": Point(0.3480902777777778, 0.4529320987654321),
            "5_4": Point(0.4153645833333333, 0.4513888888888889),
            "5_5": Point(0.5008680555555556, 0.44675925925925924),
            "6_0": Point(0.0837673611111111, 0.3425925925925926),
            "6_1": Point(0.23133680555555555, 0.3410493827160494),
            "6_2": Point(0.3767361111111111, 0.35185185185185186),
            "6_3": Point(0.5177951388888888, 0.35108024691358025),
            "7_0": Point(0.10590277777777778, 0.2492283950617284),
            "7_1": Point(0.3068576388888889, 0.23765432098765432),
            "7_2": Point(0.4952256944444444, 0.24845679012345678),
            "8_0": Point(0.1423611111111111, 0.20987654320987653),
            "8_1": Point(0.25, 0.19907407407407407),
            "8_2": Point(0.3559027777777778, 0.19598765432098766),
            "8_3": Point(0.4609375, 0.2052469135802469),
            "9_0": Point(0.18489583333333334, 0.10570987654320987),
            "9_1": Point(0.4262152777777778, 0.10185185185185185)
        }

        self.sciencetree = {
            "0_0": ["1_0", "1_1", "1_2"],
            "4_0": ["5_0", "5_1"],
            "4_1": ["5_2", "5_3"],
            "4_2": ["5_4", "5_5"],
            "5_2": ["6_1"],
            "5_3": ["6_2"],
            "5_4": ["6_2"],
            "5_5": ["6_3"],
            "6_2": ["7_1"],
            "6_3": ["7_2"],
            "7_1": ["8_1", "8_2"],
            "7_2": ["8_3"],
            "8_1": ["9_0"],
            "8_2": ["9_1"],
            "8_3": ["9_1"],
            "9_0": [],
            "9_1": []
        }

        

    def create(self, event=None):
        self.setSize(0.8*self.mainwindow.windowxsize, 0.8*self.mainwindow.windowysize)
        AbstractCanvas.create(self, int(self.mainwindow.windowxsize/2), int(self.mainwindow.windowysize/2))
        #self.canvas.bind("<Leave>", self.destroy) (leave is not the way to go because of opening payrescanvas, maybe offer esc?)
        
        # button
        bBG = self.canvas.create_rectangle(int(0.5*self.sizex-self.iconxsize), 0, int(0.5*self.sizex+self.iconxsize), 35, fill="grey40")
        b = self.canvas.create_text(0.5*self.sizex, 5, anchor=tk.N, text="Schliessen", font=tk.font.Font(size=12))
        self.canvas.tag_bind(bBG, "<Button-1>", self.destroy)
        self.canvas.tag_bind(b, "<Button-1>", self.destroy)

        self.repaint()
    
    def repaint(self):
        if self.canvas is None:
            return
        self.canvas.delete("img")
        self.imgs=[]
        img = ImageTk.PhotoImage(Image.open(self.plan))
        self.canvas.create_image(0, 45, anchor=tk.NW, image=img, tags=("plan","img"))
        self.imgs.append(img)
        self.canvas.tag_bind("plan", "<Button-1>", self.mapClick)

        #paint positions of players in science
        posbycolor={}
        #TODO getter for _players
        for puuid in self._control._players:
            p = self._control.getObjectByUuid(puuid)
            scstring = p.getScienceLevel().getString()
            if scstring in posbycolor.keys():
                posbycolor[scstring].append(p.getColor())
            else:
                posbycolor[scstring]=[p.getColor()]
        
        for k, v in posbycolor.items():
            for idx, c in enumerate(v):
                img = ImageTk.PhotoImage(Image.open(os.path.join(imagefolder_rescaled, c, "Krone.png")))
                posxy = self.uvToCanvas(self.fields[k])
                self.canvas.create_image(int(posxy.x+(idx-len(v)//2)*(float(self.iconxsize)/2)), int(posxy.y), anchor=tk.CENTER, image=img, tags="img")
                self.imgs.append(img)
        

        #paint cardbacks to buy science (tag_bind doesnt work, why? maybe because images, use find closest again instead, as in battleshipcanvas)
        img = ImageTk.PhotoImage(Image.open(self.buycard1))
        self.canvas.create_image(int(self.sizex/3), int(self.sizey), anchor=tk.S, image=img, tags=("buyscience10", "img"))
        self.imgs.append(img)
        self.canvas.tag_bind("buyscience10", "<Button-1>", lambda event, value=10: self.buyScience(value))

        img = ImageTk.PhotoImage(Image.open(self.buycard2))
        self.canvas.create_image(int(self.sizex/2), int(self.sizey), anchor=tk.S, image=img, tags=("buyscience25", "img"))
        self.imgs.append(img)
        self.canvas.tag_bind("buyscience25", "<Button-1>", lambda event, value=25: self.buyScience(value))

        img = ImageTk.PhotoImage(Image.open(self.buycard3))
        self.canvas.create_image(int(2*self.sizex/3), int(self.sizey), anchor=tk.S, image=img, tags=("buyscience50", "img"))
        self.imgs.append(img)
        self.canvas.tag_bind("buyscience50", "<Button-1>", lambda event, value=50: self.buyScience(value))


    #TODO exception handling
    def buyScience(self, value):
        if self.locked:
            return
        if not self._control.getActivePlayerObj().canBuyScience():
            self.mainwindow.showInfo(text = "Bereits die maximale Anzahl Wissenschaftskarten für diese Runde gekauft!")
            return

        self.locked=True
        self.mainwindow.payrescanvas.create(value, "science", sciencecanvas=self)

    def proceedLevel(self):
        playerobj = self._control.getActivePlayerObj()
        currsciencelevel = playerobj.getScienceLevel()
        if currsciencelevel.getString() in self.sciencetree.keys():
            tmp = self.sciencetree[currsciencelevel.getString()]
            nextcandidates=[]
            for s in tmp:
                nextcandidates.append(ScienceLevel(s))
        else:
            nextcandidates = [currsciencelevel + 1]
        #highlight possible ways and make clickable
        for c in nextcandidates:
            posxy=self.uvToCanvas(self.fields[c.getString()])
            self.canvas.create_oval(posxy.x-int(0.5*self.iconxsize), posxy.y-int(0.5*self.iconysize), posxy.x+int(0.5*self.iconxsize), posxy.y+int(0.5*self.iconysize), tags=("targets", c.getString()), fill=colordictionary[playerobj.getColor()])
        self.canvas.tag_bind("targets", "<Button-1>", self.proceedLevelClick)

    def proceedLevelClick(self, event):
        #on click set the players science level to the corresponding
        if len(self.canvas.gettags(self.canvas.find_closest(event.x, event.y)))!=3: #3 because has current added
            #can happen sometimes when clicking close to border
            return
        clickedlevel=self.canvas.gettags(self.canvas.find_closest(event.x, event.y))[1]
        self._control.getActivePlayerObj().setScienceLevel(ScienceLevel(clickedlevel))
        self.locked=False
        self.canvas.delete("targets")
        self.repaint()

    def mapClick(self, event=None):
        #for debug
        #getting uv coords from clicking
        #uv=self.canvasToUv(Point(event.x, event.y))
        #print("Point("+str(uv.x)+", "+str(uv.y)+"),")
        pass

    def destroy(self, event=None):
        if self.locked:
            return
        AbstractCanvas.destroy(self)
