import os

from PIL import Image, ImageTk
from shapely.geometry import Point

from schiffli_util import *
from schiffli_util.tkimports import *

from schiffli_gui.canvas.abstractcanvas import AbstractCanvas


class MissionCanvas(AbstractCanvas):
    def __init__(self, mainwindow):
        AbstractCanvas.__init__(self, mainwindow)


    def create(self, event=None):
        self.setSize(0.8*self.mainwindow.windowxsize, 0.8*self.mainwindow.windowysize)
        AbstractCanvas.create(self, int(self.mainwindow.windowxsize/2), int(self.mainwindow.windowysize/2))
        #self.canvas.bind("<Leave>", self.destroy) (leave is not the way to go because of opening payrescanvas, maybe offer esc?)
        
        # button
        bBG = self.canvas.create_rectangle(int(0.5*self.sizex-self.iconxsize), 0, int(0.5*self.sizex+self.iconxsize), 35, fill="grey40")
        b = self.canvas.create_text(0.5*self.sizex, 5, anchor=tk.N, text="Schliessen", font=tk.font.Font(size=12))
        self.canvas.tag_bind(bBG, "<Button-1>", self.destroy)
        self.canvas.tag_bind(b, "<Button-1>", self.destroy)

        
        for idx, muuid in enumerate(self.getControl().getActiveMissions()):
            mobj = self.getControl().getObjectByUuid(muuid)
            img = ImageTk.PhotoImage(Image.open(mobj.getIcon()))
            self.canvas.create_image((0.4+(2.6*(idx // 2)))*self.iconxsize, (0.7+5.1*(idx % 2))*self.iconysize, anchor=tk.NW, image=img)  #do whe need a click function for them?
            self.imgs.append(img)


    def repaint(self):
        #no repaint needed atm
        pass

    def destroy(self, event=None):
        AbstractCanvas.destroy(self)