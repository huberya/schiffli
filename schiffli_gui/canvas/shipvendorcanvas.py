import os

from PIL import Image, ImageTk

from schiffli_control import BattleShip, TradingShip
from schiffli_util import tradingshiptypes, battleshiptypes, imagefolder_rescaled, allicons, globalworld
from schiffli_util.tkimports import *

from schiffli_gui.canvas.abstractcanvas import AbstractCanvas


class ShipVendorCanvas(AbstractCanvas):
    def __init__(self, mainwindow):
        AbstractCanvas.__init__(self, mainwindow)

    def create(self):
        self.setSize(5*self.iconxsize, int((max(int(len(tradingshiptypes)+1), int(len(battleshiptypes)+1))+1)*self.iconysize/2)+60)
        AbstractCanvas.create(self, self.mainwindow.windowxsize//2, self.mainwindow.windowysize//2)

        # create text
        # button
        bBG = self.canvas.create_rectangle(int(0.5*self.sizex-self.iconxsize), 0, int(0.5*self.sizex+self.iconxsize), 35, fill="grey40")
        b = self.canvas.create_text(0.5*self.sizex, 5, anchor=tk.N, text="Schliessen", font=tk.font.Font(size=12))
        self.canvas.tag_bind(bBG, "<Button-1>", self.destroy)
        self.canvas.tag_bind(b, "<Button-1>", self.destroy)

        self.canvas.create_text(self.iconxsize, 45, anchor=tk.N, text="Handelsschiffe", font=tk.font.Font(size=12))
        self.canvas.create_text(4*self.iconxsize, 45, anchor=tk.N, text="Kriegsschiffe", font=tk.font.Font(size=12))

        # paint tradingships
        for idx, t in enumerate(tradingshiptypes):
            img = ImageTk.PhotoImage(Image.open(
                os.path.join(imagefolder_rescaled, allicons[t])))
            self.canvas.create_image(idx % 2*self.iconxsize, idx//2*self.iconysize+60, anchor=tk.NW, image=img)
            self.imgs.append(img)

        # paint battleships
        for idx, t in enumerate(battleshiptypes):
            img = ImageTk.PhotoImage(Image.open(
                os.path.join(imagefolder_rescaled, allicons[t])))
            self.canvas.create_image(3*self.iconxsize + idx % 2*self.iconxsize, idx//2*self.iconysize+60, anchor=tk.NW, image=img)
            self.imgs.append(img)

        self.canvas.bind("<Button-1>", self.canvasclick)

    def canvasclick(self, event):
        # get correct ship type
        if event.x <= 2*self.iconxsize and event.y > 60:
            row = int((event.y-60)/self.iconysize)
            col = int(event.x/self.iconxsize)
            stype = tradingshiptypes[2*row+col]
            self._control.buyShip(stype, tags=["tradingship", "clickable"])

        elif event.x >= 3*self.iconxsize and event.y > 60:
            row = int((event.y-60)/self.iconysize)
            col = int((event.x-3*self.iconxsize)/self.iconxsize)
            stype = battleshiptypes[2*row+col]
            self._control.buyShip(stype, tags=["battleship", "clickable"])
        else:
            # clicked somewhere but not on a ship
            return

    def destroy(self, event=None):
        AbstractCanvas.destroy(self)
