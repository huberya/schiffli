from PIL import Image, ImageTk
from shapely.geometry import Point

from schiffli_control import Controlled
from schiffli_util.tkimports import *


class AbstractCanvas(Controlled):
    def __init__(self, mainwindow, sizex=0, sizey=0):
        Controlled.__init__(self, mainwindow.control)
        self.mainwindow = mainwindow
        self.canvas = None
        self.imgs = []
        self.iconxsize = mainwindow.iconxsize
        self.iconysize = mainwindow.iconysize
        self.app = mainwindow.app
        self.sizex = sizex
        self.sizey = sizey
        self.prevent_reopen = False

    def setSize(self, sizex, sizey):
        self.sizex = sizex
        self.sizey = sizey

    def getSize(self):
        return self.sizex, self.sizey

    def uvToCanvas(self, uv):
        return Point(uv.x*self.sizex, uv.y*self.sizey)

    def canvasToUv(self, pos):
        return Point(pos.x/self.sizex, pos.y/self.sizey)

    def resetPreventReopen(self):
        self.prevent_reopen = False
    
    def setPreventReopen(self):
        self.prevent_reopen = True
        
    def create(self, posx, posy , _in=None): #can we easily decide where to place this stuff...
        if self.prevent_reopen:
            return
        if _in is None:
            _in=self.app
        if not self.canvas is None:
            self.destroy()

        self.canvas = tk.Canvas(self.app, width=self.sizex, height=self.sizey, bd=0)
        self.imgs = []
        self.canvas.place(x=posx, y=posy, in_=_in, anchor=tk.CENTER)
        #what can be done here...

    def destroy(self):
        self._control.mapblocked = False
        if not self.canvas is None:
            self.canvas.delete("all")
            self.imgs = []
            self.canvas.destroy()
            self.canvas = None

class AbstractRessourceCanvas(AbstractCanvas):
    def __init__(self, mainwindow, sizex=0, sizey=0):
        AbstractCanvas.__init__(self, mainwindow, sizex=sizex, sizey=sizey)
        self.reskeys = []

    def paintRessources(self, ressourcekeys):
        self.reskeys = ressourcekeys
        playerobj = self._control.getActivePlayerObj()
        for idx, k in enumerate(ressourcekeys):
            if playerobj.getResAmount(rtype=k) > 8:
                xoffset = 0.1
            elif playerobj.getResAmount(rtype=k) > 4:
                xoffset = 0.25
            else:
                xoffset = 0.5

            for i in range(playerobj.getResAmount(rtype=k)):
                img = ImageTk.PhotoImage(Image.open(playerobj.getResIcon(rtype=k)))
                self.canvas.create_image(int(i*xoffset*self.iconxsize), idx*self.iconysize+45, anchor=tk.NW, image=img, tags="ressource")
                self.imgs.append(img)

        self.canvas.tag_bind("ressource", "<Button-1>", lambda event: self.click(event))

    def create(self, ressourcekeys, posx=0, posy=0): #signature?
        if self.prevent_reopen:
            return
        AbstractCanvas.create(self, posx, posy)

        # button
        bBG = self.canvas.create_rectangle(int(0.75*self.iconxsize), 0, int(2.25*self.iconxsize), 35, fill="grey40")
        b = self.canvas.create_text(1.5*self.iconxsize, 5, anchor=tk.N, text="Abbruch", font=tk.font.Font(size=12))
        self.canvas.tag_bind(bBG, "<Button-1>", self.destroy)
        self.canvas.tag_bind(b, "<Button-1>", self.destroy)
        
        self.paintRessources(ressourcekeys)

    def repaint(self, ressourcekeys):
        # should probably do this more efficiently
        self.canvas.delete("ressource")
        self.imgs = []
        self.reskeys = []
        
        self.paintRessources(ressourcekeys)

    def click(self, event):
        #think about this: maybe want exceptionhandling here or so...
        pass

    def destroy(self):
        AbstractCanvas.destroy(self)
        self.reskeys = []