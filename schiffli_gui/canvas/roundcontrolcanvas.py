import os

from PIL import Image, ImageTk
from shapely.geometry import Point

from schiffli_util import imagefolder_rescaled, polarToCart
from schiffli_util.tkimports import *

from schiffli_gui.canvas.abstractcanvas import AbstractCanvas


class RoundControlCanvas(AbstractCanvas):
    def __init__(self, mainwindow):
        AbstractCanvas.__init__(self, mainwindow)
        #TODO use actual graphic
        self.plan = os.path.join(imagefolder_rescaled, "plans", "placeholder.png")     

    def create(self, event=None):
        self.setSize(0.8*self.mainwindow.windowxsize, 0.8*self.mainwindow.windowysize)
        AbstractCanvas.create(self, int(self.mainwindow.windowxsize/2), int(self.mainwindow.windowysize/2))
        #self.canvas.bind("<Leave>", self.destroy) (leave is not the way to go because of opening payrescanvas, maybe offer esc?)
        
        # button
        bBG = self.canvas.create_rectangle(int(0.5*self.sizex-self.iconxsize), 0, int(0.5*self.sizex+self.iconxsize), 35, fill="grey40")
        b = self.canvas.create_text(0.5*self.sizex, 5, anchor=tk.N, text="Schliessen", font=tk.font.Font(size=12))
        self.canvas.tag_bind(bBG, "<Button-1>", self.destroy)
        self.canvas.tag_bind(b, "<Button-1>", self.destroy)

        self.repaint()
    
    def repaint(self):
        if self.canvas is None:
            return
        self.canvas.delete("img")
        self.imgs=[]
        img = ImageTk.PhotoImage(Image.open(self.plan))
        self.canvas.create_image(0, 45, anchor=tk.NW, image=img, tags=("plan","img"))
        self.imgs.append(img)
        
        #TODO probably need to tune offset to number of objects
        roundcontrol = self._control.getRoundControl()
        for i in range(6): #is 1 to 7 better here?
            tmpturn = roundcontrol.getCurrTurn()+i
            for idx, objuuid in roundcontrol.getUuidsAt(tmpturn):
                obj = self._control.getObjectByUuid(objuuid)
                #somehow paint the object at pos(in uv)
                pos = Point(0.5, 0.5) + polarToCart(r=0.3+idx*0.08, alpha=tmpturn%6*60)
                img = ImageTk.PhotoImage(Image.open(obj.getIcon()))
                self.canvas.create_image(pos.x, pos.y, anchor=tk.CENTER, image=img, tags=("timed","img"))
                self.imgs.append(img)

    def destroy(self, event=None):
        AbstractCanvas.destroy(self)
