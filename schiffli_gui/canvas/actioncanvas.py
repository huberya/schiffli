import os

from PIL import Image, ImageTk
from shapely.geometry import Point

from schiffli_control import Building
from schiffli_util.tkimports import *
from schiffli_util import globalworld, imagefolder_rescaled, NotEnoughRessources, NotEnoughSpace

from schiffli_gui.canvas.abstractcanvas import AbstractCanvas


class ActionCanvas(AbstractCanvas):
    def __init__(self, mainwindow):
        AbstractCanvas.__init__(self, mainwindow)
        self._type = None

    def create(self, town, ship, type="trading"):
        self._type = type
        # positioncalculation
        posxy = self.mainwindow.tileToPos(town.getPos())

        self.ship = ship
        self.town = town
        
        if type == "trading":
            #TODO apply seuche aswell if corresponding harbout is affected
            # compute needed height (dependent on number of different ressources), width should be fixed
            nuniqueres = 0
            # count unique ressources in ship and town
            nuniqueres += town.getNUniqueRessources()
            nuniqueres += ship.getNUniqueRessources()

            self.setSize(5*self.iconxsize, nuniqueres*self.iconysize+45)
            AbstractCanvas.create(self, posxy.x, posxy.y, _in=self.mainwindow.canvas)

            self.reskeys = []
            self.selectedrow = None
            
            # put all the control stuff and the ressources that exist from both sides (somehow selectable with arrows I think)
            # also add a button that is used for exit
            self.canvas.create_text(0.1*self.iconxsize, 5, anchor=tk.NW, text=town.getType(), font=tk.font.Font(size=12))
            self.canvas.create_text(4.9*self.iconxsize, 5, anchor=tk.NE, text=ship.getType(), font=tk.font.Font(size=12))

            # here come ressources (want to put that in a function maybe)
            curruniqueres = 0
            townres = town.getRessources()
            shipres = ship.getRessources()
            # iterate through all ressources, plot for town (left) and ship (right)
            for res, amount in townres.items():
                if amount > 0 or shipres[res] > 0:
                    self.reskeys.append(res)
                    # do plotting (town)
                    if amount > 8:
                        xoffset = 0.1
                    elif amount > 4:
                        xoffset = 0.25
                    else:
                        xoffset = 0.5
                    for i in range(amount):
                        img = ImageTk.PhotoImage(Image.open(town.getResIcon(res)))
                        self.canvas.create_image(int(i*xoffset*self.iconxsize), curruniqueres * self.iconysize+35, anchor=tk.NW, image=img, tags="row"+str(curruniqueres))
                        self.imgs.append(img)

                    if shipres[res] > 8:
                        xoffset = 0.1
                    elif shipres[res] > 4:
                        xoffset = 0.25
                    else:
                        xoffset = 0.5
                    # do plotting (ship)
                    for i in range(shipres[res]):
                        img = ImageTk.PhotoImage(Image.open(ship.getResIcon(res)))
                        self.canvas.create_image(int((5-i*xoffset)*self.iconxsize), curruniqueres * self.iconysize+35, anchor=tk.NE, image=img, tags="row"+str(curruniqueres))
                        self.imgs.append(img)
                    curruniqueres += 1

            # Buttons(not really buttons but behave like)
            bBG = self.canvas.create_rectangle(1.5*self.iconxsize, 5, 3.5*self.iconxsize, 30, fill="grey40")
            b = self.canvas.create_text(2.5*self.iconxsize, 5, anchor=tk.N, text="Ende", font=tk.font.Font(size=12))
            self.canvas.tag_bind(bBG, "<Button-1>", lambda event, ship=ship: self.destroy(ship))
            self.canvas.tag_bind(b, "<Button-1>", lambda event, ship=ship: self.destroy(ship))

            # binding to select ressources
            self.canvas.bind("<Button-1>", self.click)

        elif type == "buildandrefine":
            #town here is harbour!
            #here we need to check for seuche:
            for e in town.getEffects():
                eobj = self.getControl().getObjectByUuid(e)
                if eobj.getType() == "seuche" and (eobj.getArgs()["exclude"]=="none" or (eobj.getArgs()["exclude"]=="by" and self._control.getActivePlayer()==eobj.getArgs()["by"])): #TODO logic for ally excludes
                    self.mainwindow.showInfo(text="Keine Interaktion mit diesem Hafen möglich,  da er verseucht ist!", function=lambda ship=self.ship: self.destroy(ship=ship))
                    return
            
            availcountries = []
            for c in globalworld.harbours[town.getPos()]:
                if len(self._control.getObjectsByPos(c, tag="building"))==0:
                    availcountries.append(c)

            self.setSize(3*self.iconxsize, (len(availcountries) + len(town.getRefinements()))*self.iconysize+45)
            AbstractCanvas.create(self, posxy.x, posxy.y, _in=self.mainwindow.canvas)

            self.ship = ship

            # put all the control stuff and the ressources that exist from both sides (somehow selectable with arrows I think)
            # also add a button that is used for exit

            # Buttons(not really buttons but behave like)
            bBG = self.canvas.create_rectangle(0.5*self.iconxsize, 5, 2.5*self.iconxsize, 30, fill="grey40")
            b = self.canvas.create_text(1.5*self.iconxsize, 5, anchor=tk.N, text="Ende", font=tk.font.Font(size=12))
            self.canvas.tag_bind( bBG, "<Button-1>", lambda event, ship=ship: self.destroy(ship))
            self.canvas.tag_bind(b, "<Button-1>", lambda event, ship=ship: self.destroy(ship))

            for idx, c in enumerate(availcountries):
                bBG = self.canvas.create_rectangle( 5, 45+2*idx*self.iconysize//2, 3*self.iconxsize-5, 45+(2*idx+1)*self.iconysize//2, fill="grey40")
                b = self.canvas.create_text(1.5*self.iconxsize, 45+2*idx*self.iconysize//2, anchor=tk.N, text="Siedlung "+globalworld.countries[c], font=tk.font.Font(size=10))
                self.canvas.tag_bind(bBG, "<Button-1>", lambda event, btype="settlement", country=c: self._control.buildTown(self.ship.getUuid(), btype, country))
                self.canvas.tag_bind(b, "<Button-1>", lambda event, btype="settlement", country=c: self._control.buildTown(self.ship.getUuid(), btype, country))

                bBG = self.canvas.create_rectangle(5, 45+(2*idx+1)*self.iconysize//2, 3*self.iconxsize-5, 45+(2*idx+2)*self.iconysize//2, fill="grey40")
                b = self.canvas.create_text(1.5*self.iconxsize, 45+(2*idx+1)*self.iconysize//2, anchor=tk.N, text="Aussenposten "+globalworld.countries[c], font=tk.font.Font(size=10))
                self.canvas.tag_bind(bBG, "<Button-1>", lambda event, btype="outpost", country=c: self._control.buildTown(self.ship.getUuid(), btype, country))
                self.canvas.tag_bind( b, "<Button-1>", lambda event, btype="outpost", country=c: self._control.buildTown(self.ship.getUuid(), btype, country))

            # do other buttons similar to this one for choosing to build something
            for idx, r in enumerate(town.getRefinements()):
                img = ImageTk.PhotoImage(Image.open(r.getGraphic()))
                refinementb = self.canvas.create_image(1.5*self.iconxsize, int((len(availcountries)+idx))*self.iconysize+45, anchor=tk.N, image=img)
                self.imgs.append(img)
                self.canvas.tag_bind(refinementb, "<Button-1>", lambda event, refinement=r, ship=ship: self.refinement(refinement, ship))

            # do all the missions
            for idx, m in enumerate(self.getControl().getActiveMissions()):
                mobj = self.getControl().getObjectByUuid(m)
                if not mobj.getTarget() == town.getUuid():
                    continue
                img = ImageTk.PhotoImage(Image.open(mobj.getGraphic()))
                missionb = self.canvas.create_image(1.5*self.iconxsize, int((len(availcountries)+len(town.getRefinements())+idx))*self.iconysize+45, anchor=tk.N, image=img)
                self.imgs.append(img)
                self.canvas.tag_bind(missionb, "<Button-1>", lambda event, mission=m, ship=ship: self.completeMission(mission, ship))
        else:
            self.mainwindow.printWarning("unknown type "+str(type))

    def repaintRow(self, row):
        # delete old ones (there is a memory leak here for now, since we dont remove them from imgs
        self.canvas.delete("row"+str(row))
        # repaint new
        if self.town.getResAmount(self.reskeys[row]) > 8:
            xoffset = 0.1
        elif self.town.getResAmount(self.reskeys[row]) > 4:
            xoffset = 0.25
        else:
            xoffset = 0.5
        # repaint new
        for i in range(self.town.getResAmount(self.reskeys[row])):
            img = ImageTk.PhotoImage(Image.open(self.town.getResIcon(self.reskeys[row])))
            self.canvas.create_image(int(i*xoffset*self.iconxsize), row * self.iconysize+35, anchor=tk.NW, image=img, tags="row"+str(row))
            self.imgs.append(img)
        # repaint shipstuff
        if self.ship.getResAmount(self.reskeys[row]) > 8:
            xoffset = 0.1
        elif self.ship.getResAmount(self.reskeys[row]) > 4:
            xoffset = 0.25
        else:
            xoffset = 0.5
        # do plotting (ship)
        for i in range(self.ship.getResAmount(self.reskeys[row])):
            img = ImageTk.PhotoImage(Image.open(self.ship.getResIcon(self.reskeys[row])))
            self.canvas.create_image(int((5-i*xoffset)*self.iconxsize), row * self.iconysize+35, anchor=tk.NE, image=img, tags="row"+str(row))
            self.imgs.append(img)

    def click(self, event):
        # calculate row
        row = int((event.y-35)/self.iconysize)
        if row >= len(self.reskeys):
            # otherwise can click to border and crash
            return

        if row == self.selectedrow:
            if event.x < 2*self.iconxsize or event.x > 3*self.iconxsize:
                # if we are in the left or right part we dont do anything
                return
            else:
                # here we need to trade
                if event.y < (row+0.5)*self.iconysize+35:
                    # if in upper half of row we trade from ship to town
                    try:
                        self._control.moveRessourceFromTo(self.reskeys[row], self.ship.getUuid(), self.town.getUuid())
                        self.repaintRow(row)
                    except NotEnoughSpace:
                        #TODO add feedback
                        pass
                    except NotEnoughRessources:
                        #TODO add feedback
                        pass
                    except:
                        self.mainwindow.printError("Unexpected error:", sys.exc_info()[0])
                        raise

                else:
                    # else we trade from town to ship
                    try:
                        self._control.moveRessourceFromTo(self.reskeys[row], self.town.getUuid(), self.ship.getUuid())
                        self.repaintRow(row)
                    except NotEnoughSpace:
                        #TODO add feedback
                        pass
                    except NotEnoughRessources:
                        #TODO add feedback
                        pass
                    except:
                        self.mainwindow.printError("Unexpected error:", sys.exc_info()[0])
                        raise

        else:
            # delete old rectangle
            self.canvas.delete("selection")
            # draw new rectangle
            self.canvas.create_rectangle(5, row*self.iconysize+30, 5*self.iconxsize-5, (row+1)*self.iconysize+40, tags="selection")
            img = ImageTk.PhotoImage(Image.open(os.path.join(imagefolder_rescaled, "leftrightarrow.png")))
            self.canvas.create_image(int(2.5*self.iconxsize), int((row+0.5)*self.iconysize+35), anchor=tk.CENTER, image=img, tags="selection")
            self.imgs.append(img)
            self.selectedrow = row
            return

    def refinement(self, refinement, ship):
        try:
            self._control.refineRessources(refinement, ship.getUuid())
        except NotEnoughRessources:
            pass
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise

    def completeMission(self, mission, ship):
        try:
            self.getControl().completeMission(mission, ship.getUuid())
        except NotEnoughRessources:
            pass
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise
        
    def destroy(self, ship=None):
        if not ship is None:
            self.getControl().completeCurrTradingTown(ship.getUuid())
        AbstractCanvas.destroy(self)
        self.selectedrow = None
        self.ship = None
        self.town = None
        self.reskeys = []
        if ship is None:
            # called to simply destroy
            return
        # if this is not the last harbour passed by the ship need to do next, otherwise last
        
        town = ship.getCurrTradingTown()
        if town == None:
            self.mainwindow.selectedobject = None
        elif town in globalworld.harbours:
            townobj = self._control.getObjectByUuid(town)
            self.create(townobj, ship, type="buildandrefine")
        else:
            townobj = self._control.getObjectByUuid(town)
            self.create(townobj, ship, type="trading")
