from PIL import Image, ImageTk

from schiffli_util.tkimports import *

from schiffli_gui.canvas.abstractcanvas import AbstractCanvas


class BattleShipCanvas(AbstractCanvas):
    def __init__(self, mainwindow):
        AbstractCanvas.__init__(self, mainwindow)

        self.setSize(int(mainwindow.boardxdim/2), mainwindow.windowysize-mainwindow.boardydim)
        sizex, sizey = self.getSize()
        posx = int(mainwindow.boardxdim/2)+sizex//2
        posy = mainwindow.boardydim + sizey//2
        
        AbstractCanvas.create(self, posx, posy)
    
    #not allowed to be called for this
    def create(self):
        pass

    def paint(self):
        self.canvas.delete("all")
        self.imgs=[]
        self.ships=[]
        
        #paint ships that do not belong to a fleet
        nship=0
        playerobj = self._control.getActivePlayerObj()
        for suuid in playerobj.getBattleShips():
            s = self._control.getObjectByUuid(suuid)
            if s.getFleet()==None:
                img = ImageTk.PhotoImage(Image.open(s.getIcon()))
                self.canvas.create_image((nship%3)*self.iconxsize, (nship//3)*self.iconysize, anchor=tk.NW, image=img, tags=tuple([s.getUuid()]+s.getTags()))
                self.imgs.append(img)
                self.ships.append(s)
                nship+=1
                
        #paint ships from fleets (and fleet plan)
        for idx, fuuid in enumerate(playerobj.getFleets()):
            #fleet plan itself
            f = self._control.getObjectByUuid(fuuid)
            img = ImageTk.PhotoImage(Image.open(f.getPlan()))
            self.canvas.create_image((4+3*idx)*self.iconxsize, 0, anchor=tk.NW, image=img, tags=tuple([f.getUuid()]+f.getTags()))
            self.imgs.append(img)
            for i in range(f.getSize()):
                suuid=f.getShip(i)
                if not suuid is None:
                    s = self._control.getObjectByUuid(suuid)
                    img = ImageTk.PhotoImage(Image.open(s.getIcon()))
                    self.canvas.create_image((4.4+3*idx+(1.1*(i%2)))*self.iconxsize, (0.7+(i//2))*self.iconysize, anchor=tk.NW, image=img, tags=tuple([s.getUuid()]+s.getTags())) #probably need to add tags here somehow
                    self.imgs.append(img)
                    self.ships.append(s)
        
        #drag and drop functionality to put ships on fleets (taken from here:https://stackoverflow.com/questions/6740855/board-drawing-code-to-move-an-oval/6789351#6789351)
        self.canvas.tag_bind("battleship", "<ButtonPress-1>", self.on_ship_press)
        self.canvas.tag_bind("battleship", "<ButtonRelease-1>", self.on_ship_release)
        self.canvas.tag_bind("battleship", "<B1-Motion>", self.on_ship_motion)
        self.canvas.tag_bind("fleet", "<Button-1>", self.select_fleet)
    
    def on_ship_press(self, event):
        '''Begining drag of a ship'''
        self.drag_data={}
        # record the item and its location
        self.drag_data["item"] = self.canvas.find_closest(event.x, event.y)[0] #might be able to use find closest again
        self.drag_data["x"] = event.x
        self.drag_data["y"] = event.y
        self.drag_data["totalmovex"] = 0
        self.drag_data["totalmovey"] = 0
        self.canvas.tag_raise(self.drag_data["item"])
        
    def on_ship_release(self, event):
        '''End drag of a ship'''
        activeplayerobj = self._control.getActivePlayerObj()
        if 4*self.iconxsize < event.x < (4+3*len(activeplayerobj.getFleets()))*self.iconxsize and 0 < event.y < 4*self.iconysize:
            #determine fleet we are on
            fleetnr=1+(event.x-4*self.iconxsize)//(3*self.iconxsize)
            #find closest nr on that fleet plan and snap to it
            #left or right col
            col=(event.x-(1+3*fleetnr)*self.iconxsize)//(1.5*self.iconxsize)
            #determine row
            if event.y<1.7*self.iconysize:
                row = 0
            elif event.y<2.7*self.iconysize:
                row=1
            else:
                row=2
            fleetpos = 2*row+col
            #actually add the ship to the fleet
            shipuuid=self.canvas.gettags(self.drag_data["item"])[0]
            if not self._control.assignShipToFleet(shipuuid, activeplayerobj.getFleet(fleetnr), fleetpos):
                #if it failed simply move the ship back
                self.canvas.move(self.drag_data["item"], -self.drag_data["totalmovex"], -self.drag_data["totalmovey"])
        
        else:
            #check if ship belonged to a fleet
            shipuuid=self.canvas.gettags(self.drag_data["item"])[0]
            shipobj = self._control.getObjectByUuid(shipuuid)
            if shipobj.getFleet()==None:
                self.canvas.move(self.drag_data["item"], -self.drag_data["totalmovex"], -self.drag_data["totalmovey"])
            elif not self._control.assignShipToFleet(shipuuid, None):
                #if it failed simply move the ship back
                self.canvas.move(self.drag_data["item"], -self.drag_data["totalmovex"], -self.drag_data["totalmovey"])
        # reset the drag information
        del self.drag_data

    def on_ship_motion(self, event):
        '''Handle dragging of a ship'''
        # compute how much the mouse has moved
        delta_x = event.x - self.drag_data["x"]
        delta_y = event.y - self.drag_data["y"]
        self.drag_data["totalmovex"] += delta_x
        self.drag_data["totalmovey"] += delta_y
        # move the object the appropriate amount
        self.canvas.move(self.drag_data["item"], delta_x, delta_y)
        # record the new position
        self.drag_data["x"] = event.x
        self.drag_data["y"] = event.y
        
    def select_fleet(self, event):
        #TODO
        canvasitem = self.canvas.find_closest(event.x, event.y)[0]
        fleetuuid=self.canvas.gettags(canvasitem)[0]
        self.mainwindow.selectFleet(fleetuuid)

    def updateState(self, action=None):
        self.paint()

    def destroy(self):
        pass