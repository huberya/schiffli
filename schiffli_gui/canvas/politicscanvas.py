import os

from PIL import Image, ImageTk
from shapely.geometry import Point

from schiffli_util import imagefolder_rescaled
from schiffli_util.tkimports import *

from schiffli_gui.canvas.abstractcanvas import AbstractCanvas


class PoliticsCanvas(AbstractCanvas):
    def __init__(self, mainwindow):
        AbstractCanvas.__init__(self, mainwindow)
        self.plan = os.path.join(imagefolder_rescaled, "plans", "politicplan.png")
        #TODO is old image right now
        self.iconuv={ #ordered by relation between and then krieg, neutral, verbündet, mitvertrag (but shifted by one as codes start at -1)
            "blue_green":[]
        }


    def create(self, event=None):
        self.setSize(0.8*self.mainwindow.windowxsize, 0.8*self.mainwindow.windowysize)
        AbstractCanvas.create(self, int(self.mainwindow.windowxsize/2), int(self.mainwindow.windowysize/2))
        #self.canvas.bind("<Leave>", self.destroy) (leave is not the way to go because of opening payrescanvas, maybe offer esc?)
        
        # button
        bBG = self.canvas.create_rectangle(
            int(0.5*self.sizex-self.iconxsize), 0, int(0.5*self.sizex+self.iconxsize), 35, fill="grey40")
        b = self.canvas.create_text(
            0.5*self.sizex, 5, anchor=tk.N, text="Schliessen", font=tk.font.Font(size=12))
        self.canvas.tag_bind(bBG, "<Button-1>", self.destroy)
        self.canvas.tag_bind(b, "<Button-1>", self.destroy)
        self.repaint()

    def repaint(self):
        if self.canvas is None:
            return
        self.imgs=[]
        self.canvas.delete("imgs")
        img = ImageTk.PhotoImage(Image.open(self.plan))
        self.canvas.create_image(0, 45, anchor=tk.NW, image=img)
        self.imgs.append(img)

        #paint positions of players in science
        for puuid in self.getControl().getPlayers():
            p = self.getControl().getObjectByUuid(puuid)
            img = ImageTk.PhotoImage(Image.open(os.path.join(imagefolder_rescaled, p.getColor(), "Krone.png")))
            self.mainwindow.printDev("TODO")
            #TODO get uv coordinates of possible positions+think about how to save relation between players in a smart way
        
        #TODO bind click event

    def click(self, event):
        #for creating uv coords from clicking
        pos = Point(event.x, event.y)
        uv = self.canvasToUv(pos)
        self.mainwindow.printDev(uv)
    
    def destroy(self, event=None):
        AbstractCanvas.destroy(self)