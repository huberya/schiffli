import os

from PIL import Image, ImageTk
from shapely.geometry import Point

from schiffli_util import imagefolder_rescaled, colordictionary
from schiffli_util.tkimports import *

from schiffli_gui.canvas.abstractcanvas import AbstractCanvas


class DiplomacyCanvas(AbstractCanvas):
    def __init__(self, mainwindow):
        AbstractCanvas.__init__(self, mainwindow)
        self.plan = os.path.join(imagefolder_rescaled, "plans", "politicplan.png")#need to change this


    def create(self, event=None):
        self.setSize(0.8*self.mainwindow.windowxsize, 0.8*self.mainwindow.windowysize)
        AbstractCanvas.create(self, int(self.mainwindow.windowxsize/2), int(self.mainwindow.windowysize/2))
        #self.canvas.bind("<Leave>", self.destroy) (leave is not the way to go because of opening payrescanvas, maybe offer esc?)
        
        # button
        bBG = self.canvas.create_rectangle(
            int(0.5*self.sizex-self.iconxsize), 0, int(0.5*self.sizex+self.iconxsize), 35, fill="grey40")
        b = self.canvas.create_text(
            0.5*self.sizex, 5, anchor=tk.N, text="Schliessen", font=tk.font.Font(size=12))
        self.canvas.tag_bind(bBG, "<Button-1>", self.destroy)
        self.canvas.tag_bind(b, "<Button-1>", self.destroy)
        self.repaint()


    def repaint(self):
        if self.canvas is None:
            return
        self.imgs=[]
        self.canvas.delete("imgs")
        #one rectangle per color (where berater will be placed)
        for idx, puuid in enumerate(self._control.players):
            p = self._control.getObjectByUuid(puuid)
            self.canvas.create_rectangle(int(idx%2*(self.sizex*0.5)), int(idx//2*(self.sizey*0.5-22))+45, int((idx%2+1)*(self.sizex*0.5)), int((idx//2+1)*(self.sizey*0.5)+22*(1-idx//2)), fill=colordictionary[p.getColor()], tags="imgs") #size is off

            berater = self._control.politics.getBeraterByColor(p.getColor())
            for b in berater:
                self.mainwindow.printDev("TODO")
                #draw icons and give tags so they are clickable

        #make clickable by tag

    def destroy(self, event=None):
        AbstractCanvas.destroy(self)