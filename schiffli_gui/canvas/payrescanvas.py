import os
import math

from PIL import Image, ImageTk
from shapely.geometry import Point

from schiffli_util import sciencetier_from_cost, NotEnoughRessources, TemporaryInvalidAction
from schiffli_util.tkimports import *

from schiffli_gui.canvas.abstractcanvas import AbstractRessourceCanvas


class PayResCanvas(AbstractRessourceCanvas):
    def __init__(self, mainwindow):
        AbstractRessourceCanvas.__init__(self, mainwindow)

    def create(self, value=1, type="politics", pos=None, **kwargs):
        self.kwargs=kwargs
        self.paidrestypes = []
        self.cost = value
        self.paid = 0
        self._type = type
        # select ressources for each case
        ressourcekeys, nressources = self.getRessourceKeysAndNRes_()

        if nressources == 0:
            self.mainwindow.printInfo("Not enough ressources to buy")
            if self._type=="science":
                self.mainwindow.sciencecanvas.locked=False
            return False  # don't need to open it in case the player has no ressources to buy anything


        if type == "ressource":
            self.success=True
        else:
            self.success = False

        height = int(nressources*self.iconysize+45)  # for abort button
        
        self.setSize(int(3*self.iconxsize), height)
        if pos is None:
            posx = int(self.app.winfo_pointerx())
            posy = int(self.app.winfo_pointery())
        else:
            sizex, sizey = self.getSize()
            posx = int(pos.x+sizex/2)
            posy = int(pos.y+sizey/2)
        
        AbstractRessourceCanvas.create(self, ressourcekeys, posx, posy)

        #there might already be enought points therefore we need to check here aswell
        #TODO deduplicate
        playerobj = self._control.getActivePlayerObj()
        if self._type=="politics" and playerobj.getTmpPoints(type="politics")>=self.cost:
            #TODO buy whatever player wanted to buy and remove cost from tmp points
            #self._control.getActivePlayer().removeTmpPoints(type="politics", num=self.cost) #TODO change to action
            self.success = True
            self.destroy()
            return
        elif self._type=="science":
            try:
                self._control.buyScience(tier = sciencetier_from_cost[str(self.cost)])
                self.success = True
                self.destroy()
            except NotEnoughRessources:
                #TODO give feedback
                pass
            except TemporaryInvalidAction:
                #TODO give feedback
                pass
            except:
                self.mainwindow.printError("Unexpected error:", sys.exc_info()[0])
                raise


    def click(self, event):
        if event.y < 45 or (event.y-45)//self.iconysize >= len(self.reskeys):
            return False  # outside of valid area

        rtype = self.reskeys[(event.y-45)//self.iconysize]
        # value depends on type
        try:
            self._control.sellRessource(rtype=rtype, ptype = self._type)
        except NotEnoughRessources:
            #TODO give information eventually
            pass
        except:
            self.mainwindow.printError("Unexpected error:", sys.exc_info()[0])
            raise

        self.paidrestypes.append(rtype)

        playerobj = self._control.getActivePlayerObj()
        # deleted enough, we are done
        if self._type=="politics" and playerobj.getTmpPoints(type="politics")>=self.cost:
            #TODO buy whatever player wanted to buy and remove cost from tmp points
            #self._control.getActivePlayer().removeTmpPoints(type="politics", num=self.cost) #TODO change to action
            self.success = True
            self.destroy()
            return
        elif self._type=="science":
            try:
                self._control.buyScience(tier = sciencetier_from_cost[str(self.cost)])
                self.success = True
                self.destroy()
            except NotEnoughRessources:
                #TODO give feedback
                pass
            except TemporaryInvalidAction:
                #TODO give feedback
                pass
            except:
                self.mainwindow.printError("Unexpected error:", sys.exc_info()[0])
                raise
        #note there is no exit condition for verkauf 
        self.repaint()

    def repaint(self):
        ressourcekeys, nressources = self.getRessourceKeysAndNRes_()

        self.reskeys = ressourcekeys
        if int(nressources*self.iconysize+45) > self.canvas.winfo_height():
            self.create(value=self.cost, type=self._type, pos=Point(self.canvas.winfo_x(), self.canvas.winfo_y()), **self.kwargs)
            return

        AbstractRessourceCanvas.repaint(self, ressourcekeys)

    def getRessourceKeysAndNRes_(self):
        nressources = 0
        ressourcekeys = []
        playerobj = self._control.getActivePlayerObj()
        ressources = playerobj.getRessources()
        for k, _ in ressources.items():
            if playerobj.getResValue(rtype = k, vtype=self._type) > 0 and playerobj.getResAmount(rtype=k) > 0:
                nressources += 1
                ressourcekeys.append(k)

        return ressourcekeys, nressources

    def destroy(self, event=None):
        # TODO: eventually should give back ressources in case of not successfull buy and already click
        AbstractRessourceCanvas.destroy(self)
        if not self.success:
            # TODO this is not correct atm as is does not reduce tmp points again (which it should)
            for r in self.paidrestypes:
                self._control.getActivePlayerObj().addRessource(r)
            #unlock canvas as there is no need of proceeding
            if self._type=="science":
                self.kwargs["sciencecanvas"]=False

        if self._type == "science":
            self.kwargs["sciencecanvas"].proceedLevel()
            
        self.success = False
        self.paidrestypes = []
