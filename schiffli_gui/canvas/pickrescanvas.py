import os
import math

from PIL import Image, ImageTk
from shapely.geometry import Point

from schiffli_util.tkimports import *
from schiffli_util import NotEnoughRessources

from schiffli_gui.canvas.abstractcanvas import AbstractRessourceCanvas, AbstractCanvas


class PickResCanvas(AbstractRessourceCanvas):
    def __init__(self, mainwindow):
        AbstractRessourceCanvas.__init__(self, mainwindow)

    def create(self, pos=None, boundcanvas=None):
        self.boundcanvas = boundcanvas
        ressourcekeys, nressources = self.getRessourceKeysAndNRes_()
        if nressources == 0:
            return False  # don't need to open it in case the player has no ressources to buy anything

        if not self.canvas is None:
            self.destroy()

        height = int(math.ceil(nressources/5)*self.iconysize+45)  # for abort button
        
        self.setSize(int(5*self.iconxsize), height)
        if pos is None:
            posx = int(self.app.winfo_pointerx())
            posy = int(self.app.winfo_pointery())
        else:
            sizex, sizey = self.getSize()
            posx = int(pos.x+sizex/2)
            posy = int(pos.y+sizey/2)

        AbstractCanvas.create(self, posx, posy)

        self.reskeys = ressourcekeys
        

        # button
        bBG = self.canvas.create_rectangle(
            int(1.75*self.iconxsize), 0, int(3.25*self.iconxsize), 35, fill="grey40")
        b = self.canvas.create_text(
            2.5*self.iconxsize, 5, anchor=tk.N, text="Ende", font=tk.font.Font(size=12))
        self.canvas.tag_bind(bBG, "<Button-1>", self.destroy)
        self.canvas.tag_bind(b, "<Button-1>", self.destroy)
        
        playerobj = self._control.getActivePlayerObj()
        for idx, k in enumerate(ressourcekeys):
            img = ImageTk.PhotoImage(Image.open(playerobj.getResIcon(k)))
            self.canvas.create_image(int((idx%5)*self.iconxsize), (idx//5)*self.iconysize+45, anchor=tk.NW, image=img, tags=("ressource", k))
            self.imgs.append(img)

        self.canvas.tag_bind(
            "ressource", "<Button-1>", lambda event: self.click(event))

    def click(self, event):
        if event.y < 45 or (event.y-45)//self.iconysize >= len(self.reskeys):
            return False  # outside of valid area

        if len(self.canvas.gettags(self.canvas.find_closest(event.x, event.y)))!=3:
            return
        rtype = self.canvas.gettags(self.canvas.find_closest(event.x, event.y))[1]
        try:
            self._control.buyRessource(rtype)
        except NotEnoughRessources:
            #TODO give information
            pass
        except:
            self.mainwindow.printError("Unexpected error:", sys.exc_info()[0])
            raise
        #actually we need to repaint the payrescanvas if this is connected I think
        self.repaint()

    def repaint(self):
        if not self.boundcanvas is None:
            self.boundcanvas.repaint()
        #check if connected to a payrescanvas and repaint it if it is

    def getRessourceKeysAndNRes_(self):
        nressources = 0
        ressourcekeys = []
        ressources = self._control.getActivePlayerObj().getRessources()
        for k, _ in ressources.items():
            nressources += 1
            ressourcekeys.append(k)

        return ressourcekeys, nressources

    def destroy(self, event=None):
        AbstractRessourceCanvas.destroy(self)
