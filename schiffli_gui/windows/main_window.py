# imports
import os
import inspect
import sys

from schiffli_util.util import point_to_pixel

try:
    import IPython
except:
    pass
from typing import List, Any
from PIL import Image, ImageTk
import screeninfo
from shapely.geometry import Point

from schiffli_control import StateObserverI, AbstractMapObject, RessourceTile
from schiffli_util import globalworld, SyncedSelector, playercolors, CreateToolTip, moveAnimation, tradingshiptypes, harbourtypes, randDice, buildingtypes, allrtypes, InvalidTarget, image_base_path, bcolors
from schiffli_util.tkimports import *

from schiffli_gui.canvas import *

# master class to control who is active and actually execute stuff
# should follow the observer pattern
class MainWindow(StateObserverI):
    ###public
    def __init__(self, control, app, useanimation=True, usecache=True, displayfieldnumber=False, responsiblePlayers = None):
        self.control = control
        self.useanimation = useanimation
        self.displayfieldnumber = displayfieldnumber

        self.selectedobject = None
        # this should be set to True if clicking the map is disabled currently
        self.mapblocked = False
        self.objcounter = 0
        self.pointlist = []
        # initialize variables
        self.app = app
        self.placingsettlements = False
        
        self._responsiblePlayers: List[str]
        if responsiblePlayers is None:
            self._responsiblePlayers = []
        else:
            self._responsiblePlayers = responsiblePlayers
        self.imgs = []

        self.windowxsize, self.windowysize = screeninfo.get_monitors()[0].width, screeninfo.get_monitors()[0].height
        self.app.geometry('%dx%d+%d+%d' %
                            (self.windowxsize, self.windowysize, 0, 0))
        self.app.attributes("-fullscreen", True)

        #TODO make this customizeable
        self.titlesize = 16
        self.fontsize = 14
        self.font_px = point_to_pixel(self.fontsize)

        menubar = tk.Menu(self.app)
        self.app.config(menu=menubar)
        spielmenu = tk.Menu(menubar)
        spielmenu.add_command(label="Neues Spiel", command=self.newGameClick, font=tk.font.Font(
            size=self.fontsize), accelerator='ctrl+n')
        self.app.bind_all('<Control-Key-n>', self.newGameClick)
        spielmenu.add_command(label="Speichern", command=self.saveClick, font=tk.font.Font(
            size=self.fontsize), accelerator='ctrl+s')
        self.app.bind_all('<Control-Key-s>', self.saveClick)
        spielmenu.add_command(label="Laden", command=self.loadClick, font=tk.font.Font(
            size=self.fontsize), accelerator='ctrl+l')
        self.app.bind_all('<Control-Key-l>', self.loadClick)
        spielmenu.add_command(label="Exit", command=self.exitClick, font=tk.font.Font(
            size=self.fontsize), accelerator='ctrl+e')
        self.app.bind_all('<Control-Key-e>', self.exitClick)
        menubar.add_cascade(label="Spiel", menu=spielmenu, font=tk.font.Font(size=self.titlesize))
        
        windowmenu = tk.Menu(menubar)
        windowmenu.add_command(label="Refresh", command=self.repaint, font=tk.font.Font(size=self.fontsize))
        menubar.add_cascade(label="Window", menu=windowmenu, font=tk.font.Font(size=self.titlesize))

        planmenu = tk.Menu(menubar)
        planmenu.add_command(label="Schiffe", command=lambda pdf="Schiffe.pdf": self.openpdf(
            pdf), font=tk.font.Font(size=self.fontsize))
        planmenu.add_command(label="Seegebiete", command=lambda pdf="Seegebiete.pdf": self.openpdf(
            pdf), font=tk.font.Font(size=self.fontsize))
        planmenu.add_command(label="Wissenschaft", command=lambda pdf="Wissenschaft.pdf": self.openpdf(
            pdf), font=tk.font.Font(size=self.fontsize))
        menubar.add_cascade(label="Pläne", menu=planmenu, font=tk.font.Font(size=self.titlesize))

        if self.control._devmode:
            self.runningdev = False
            devmenu = tk.Menu(menubar)
            devmenu.add_command(label="Commandwindow", command=self.openCommandWindow, font=tk.font.Font(size=self.fontsize))
            devmenu.add_command(label="IPython Embed", command=self.iPythonEmbed, font=tk.font.Font(size=self.fontsize))
            menubar.add_cascade(label="Dev", menu=devmenu, font=tk.font.Font(size=self.titlesize))
            

        # create an image of the appropriate size (depending on windowsize)
        self.boardxdim = int(0.9*self.windowxsize)
        self.boardydim = int(0.8*self.windowysize)
        im = Image.open(os.path.join(os.environ["SCHIFFLI_BASE_PATH"], "map.png"))
        size = self.boardxdim, self.boardydim  # something seems messed up here
        im = im.resize(size)
        im.save(os.path.join(os.environ["SCHIFFLI_BASE_PATH"], "tmpmap.png"), "PNG")
        self.canvas = tk.Canvas(self.app, width=self.boardxdim, height=self.boardydim, bd=0)
        self.canvas.bind("<Button-1>", self.tileClick)
        self.canvas.place(x=0, y=0, in_=self.app, anchor=tk.NW)
        img = ImageTk.PhotoImage(Image.open(os.path.join(os.environ["SCHIFFLI_BASE_PATH"], "tmpmap.png")))
        self.canvas.create_image(0, 0, anchor=tk.NW, image=img)
        self.imgs.append(img)

        # labels to fields on canvas (removed later but useful for designing to see actual numbers)
        if self.displayfieldnumber:
            for i in range(globalworld.nfields):
                uv = globalworld.mappolygons[i].centroid
                pos = self.uvToCanvas(uv)
                _ = self.canvas.create_text(pos.x, pos.y, text=str(
                    i), tags="label"+str(i), font=tk.font.Font(size=12))

        self.iconxsize = int(self.boardxdim/30)
        self.iconysize = int(self.boardydim/20)

        # canvases that open on events
        self.payrescanvas = PayResCanvas(self)
        self.pickrescanvas = PickResCanvas(self)
        self.tmprescanvas = TmpResCanvas(self)
        self.tmpinfocanvas = TmpInfoCanvas(self)
        self.actioncanvas = ActionCanvas(self)
        self.shipvendorcanvas = ShipVendorCanvas(self)
        self.battleshipcanvas = BattleShipCanvas(self)
        self.sciencecanvas = ScienceCanvas(self)
        self.politicscanvas = PoliticsCanvas(self)
        self.diplomacycanvas = DiplomacyCanvas(self)
        self.missioncanvas = MissionCanvas(self)
        self.roundcontrolcanvas = RoundControlCanvas(self)
        self.confirmationcanvas = ConfirmCanvas(self)
        self.infocanvas = InfoCanvas(self)

        # syncedSelector for selecting targets of certain actions while blocking
        self.syncedSelector = SyncedSelector(self.control, self)

        # rescale all images that might be needed later (if they dont already have appropriate size)
        # check if cache exists and if it does read content
        cachedx = None
        cachedy = None
        if usecache and os.path.isfile(os.path.join(os.path.join(image_base_path, "rescaled"), "cacheinfo.info")):
            f = open(os.path.join(os.path.join(
                image_base_path, "rescaled"), "cacheinfo.info"), 'r')
            cachedx = int(f.readline().rstrip())
            cachedy = int(f.readline().rstrip())
            f.close()

        if cachedx != self.windowxsize or cachedy != self.windowysize:

            #caching is invalid while resizing
            f = open(os.path.join(os.path.join(
                image_base_path, "rescaled"), "cacheinfo.info"), 'w')
            f.write(str(-1)+"\n")
            f.write(str(-1)+"\n")
            f.close()
            # make sure folders exist and create them otherwise:
            if not os.path.isdir(os.path.join(image_base_path, "rescaled")):
                os.makedirs(os.path.join(image_base_path, "rescaled"))

            allfiles = [f for f in os.listdir(
                image_base_path) if os.path.isfile(os.path.join(image_base_path, f))]

            for f in allfiles:
                if f.split('.')[-1] != "png":
                    self.printWarning("skipping file "+str(f) +
                            ". Extension must be png")
                else:
                    im = Image.open(os.path.join(image_base_path, f))
                    # need to think about proper size for now just row, 2*colsize
                    size = self.iconxsize, self.iconysize  # calculate size
                    im = im.resize(size)
                    im.save(os.path.join(image_base_path, "rescaled", f), "PNG")

            # for each color
            for c in playercolors:
                # make sure folders exist and create them otherwise:
                if not os.path.isdir(os.path.join(image_base_path, "rescaled", c)):
                    os.makedirs(os.path.join(image_base_path, "rescaled", c))
                currpath = os.path.join(image_base_path, c)
                allfiles = [f for f in os.listdir(
                    currpath) if os.path.isfile(os.path.join(currpath, f))]
                for f in allfiles:
                    if f.split('.')[-1] != "png":
                        self.printWarning("skipping file "+str(f) +
                                ". Extension must be png")
                    else:
                        im = Image.open(os.path.join(currpath, f))
                        # need to think about proper size for now just row, 2*colsize
                        size = self.iconxsize, self.iconysize  # calculate size
                        im = im.resize(size)
                        im.save(os.path.join(
                            image_base_path, "rescaled", c, f), "PNG")

            # rescale ressource icons to be placed on the map
            currpath = os.path.join(image_base_path, "ressfelder")
            # make sure folders exist and create them otherwise:
            if not os.path.isdir(os.path.join(image_base_path, "rescaled", "ressfelder")):
                os.makedirs(os.path.join(
                    image_base_path, "rescaled", "ressfelder"))
            allfiles = [f for f in os.listdir(
                currpath) if os.path.isfile(os.path.join(currpath, f))]
            for f in allfiles:
                if f.split('.')[-1] != "png":
                    self.printWarning("skipping file "+str(f) +
                            ". Extension must be png")
                else:
                    im = Image.open(os.path.join(currpath, f))
                    # need to think about proper size for now just row, 2*colsize
                    # calculate size
                    size = int(self.iconxsize*0.6), int(self.iconysize*0.8)
                    im = im.resize(size)
                    im.save(os.path.join(
                        image_base_path, "rescaled", "ressfelder", f), "PNG")

            # rescale graphics to display refinement
            currpath = os.path.join(image_base_path, "refinementgraphics")
            # make sure folders exist and create them otherwise:
            if not os.path.isdir(os.path.join(image_base_path, "rescaled", "refinementgraphics")):
                os.makedirs(os.path.join(
                    image_base_path, "rescaled", "refinementgraphics"))
            allfiles = [f for f in os.listdir(
                currpath) if os.path.isfile(os.path.join(currpath, f))]
            for f in allfiles:
                if f.split('.')[-1] != "png":
                    self.printWarning("skipping file "+str(f) +
                            ". Extension must be png")
                else:
                    im = Image.open(os.path.join(currpath, f))
                    # need to think about proper size for now just row, 2*colsize
                    # calculate size
                    size = int(self.iconxsize*3), int(self.iconysize)
                    im = im.resize(size)
                    im.save(os.path.join(image_base_path, "rescaled",
                                            "refinementgraphics", f), "PNG")

            # rescale graphics to display cards
            currpath = os.path.join(image_base_path, "cards")
            # make sure folders exist and create them otherwise:
            if not os.path.isdir(os.path.join(image_base_path, "rescaled", "cards")):
                os.makedirs(os.path.join(image_base_path, "rescaled", "cards"))
            allfiles = [f for f in os.listdir(
                currpath) if os.path.isfile(os.path.join(currpath, f))]
            for f in allfiles:
                if f.split('.')[-1] != "png":
                    self.printWarning("skipping file "+str(f) +
                            ". Extension must be png")
                else:
                    im = Image.open(os.path.join(currpath, f))
                    # need to think about proper size for now just row, 2*colsize
                    size = int(self.iconxsize*1.5), int(3 *
                                                        self.iconysize)  # calculate size
                    im = im.resize(size)
                    im.save(os.path.join(
                        image_base_path, "rescaled", "cards", f), "PNG")

            # rescale graphics to display missions
            currpath = os.path.join(image_base_path, "missions")
            # make sure folders exist and create them otherwise:
            if not os.path.isdir(os.path.join(image_base_path, "rescaled", "missions")):
                os.makedirs(os.path.join(image_base_path, "rescaled", "missions"))
            allfiles = [f for f in os.listdir(
                currpath) if os.path.isfile(os.path.join(currpath, f))]
            for f in allfiles:
                if f.split('.')[-1] != "png":
                    self.printWarning("skipping file "+str(f) +
                            ". Extension must be png")
                else:
                    im = Image.open(os.path.join(currpath, f))
                    # think about size for missions, should be readable but not too large I think, worstcase can make hoverable
                    size = int(self.iconxsize*2.5), int(5 *
                                                        self.iconysize)  # calculate size
                    im = im.resize(size)
                    im.save(os.path.join(
                        image_base_path, "rescaled", "missions", f), "PNG")

            # files in plans get individual sizes, i.e. list all files here
            # plans for fleets
            currpath = os.path.join(image_base_path, "plans")
            # make sure folders exist and create them otherwise:
            if not os.path.isdir(os.path.join(image_base_path, "rescaled", "plans")):
                os.makedirs(os.path.join(image_base_path, "rescaled", "plans"))
            im = Image.open(os.path.join(currpath, "Kampfflotte1.png"))
            # need to think about proper size for now just row, 2*colsize
            size = int(3*self.iconxsize), int(4 *
                                                self.iconysize)  # calculate size
            im = im.resize(size)
            im.save(os.path.join(image_base_path, "rescaled",
                                    "plans", "Kampfflotte1.png"), "PNG")

            im = Image.open(os.path.join(currpath, "Kampfflotte2.png"))
            # need to think about proper size for now just row, 2*colsize
            size = int(3*self.iconxsize), int(4 *
                                                self.iconysize)  # calculate size
            im = im.resize(size)
            im.save(os.path.join(image_base_path, "rescaled",
                                    "plans", "Kampfflotte2.png"), "PNG")

            im = Image.open(os.path.join(currpath, "Kampfflotte3.png"))
            # need to think about proper size for now just row, 2*colsize
            size = int(3*self.iconxsize), int(4 *
                                                self.iconysize)  # calculate size
            im = im.resize(size)
            im.save(os.path.join(image_base_path, "rescaled",
                                    "plans", "Kampfflotte3.png"), "PNG")

            im = Image.open(os.path.join(currpath, "resuebersicht.png"))
            # overview images for ressource ,politics and science cards
            size = int(self.boardxdim/6), self.windowysize - \
                self.boardydim-20  # calculate size
            im = im.resize(size)
            im.save(os.path.join(image_base_path, "rescaled",
                                    "plans", "resuebersicht.png"), "PNG")

            im = Image.open(os.path.join(currpath, "scienceplan.png"))
            # overview images for ressource ,politics and science cards
            size = int(0.8*self.windowxsize), int(0.8 * self.windowysize-3*self.iconysize-45)  # calculate size

            im = im.resize(size)
            im.save(os.path.join(image_base_path, "rescaled",
                                    "plans", "scienceplan.png"), "PNG")

            im = Image.open(os.path.join(currpath, "politicplan.png"))
            im = im.resize(size)
            im.save(os.path.join(image_base_path, "rescaled",
                                    "plans", "politicplan.png"), "PNG")

            #TODO fix this once we have an actual image
            im = Image.open(os.path.join(currpath, "placeholder.png"))
            im = im.resize(size)
            im.save(os.path.join(image_base_path, "rescaled",
                                    "plans", "placeholder.png"), "PNG")


            f = open(os.path.join(os.path.join(
                image_base_path, "rescaled"), "cacheinfo.info"), 'w')
            f.write(str(self.windowxsize)+"\n")
            f.write(str(self.windowysize)+"\n")
            f.close()

        # ressource canvas (clickable for enlarge)
        self.ressourcecanvas = RessourceCanvas(self)
        self.sciencecardcanvas = ScienceCardCanvas(self)

        # overview img
        self.ressourcecanvasoverview = tk.Canvas(self.app, width=int(
            self.boardxdim/6), height=self.windowysize-self.boardydim-20, bd=0)
        self.ressourcecanvasoverview.place(
            x=0, y=self.boardydim, in_=self.app, anchor=tk.NW)
        self.ressourcecanvasoverview.imgs = []

        img = ImageTk.PhotoImage(Image.open(os.path.join(
            image_base_path, "rescaled", "plans", "resuebersicht.png")))
        self.ressourcecanvasoverview.create_image(
            0, 0, anchor=tk.NW, image=img)
        self.ressourcecanvasoverview.imgs.append(img)

        self.ressourcecanvasoverview.bind(
            "<Enter>", self.ressourcecanvas.create)

        # overview img (science)
        self.sciencecanvasoverview = tk.Canvas(self.app, width=int(self.boardxdim/6), height=self.windowysize-self.boardydim-20, bd=0)
        self.sciencecanvasoverview.place(x=int(self.boardxdim/6), y=self.boardydim, in_=self.app, anchor=tk.NW)
        self.sciencecanvasoverview.imgs = []

        img = ImageTk.PhotoImage(Image.open(os.path.join(image_base_path, "rescaled", "plans", "resuebersicht.png")))  # to be changed TODO
        self.sciencecanvasoverview.create_image(0, 0, anchor=tk.NW, image=img)
        self.sciencecanvasoverview.imgs.append(img)

        self.sciencecanvasoverview.bind("<Enter>", self.sciencecardcanvas.create)

        # tmppointcanvas
        self.tmppointcanvas = TmpPointCanvas(self)
        # additional buttons
        # TODO compute size here depending on resolution (need old laptop to calculate fraction)
        # same for offset
        # TODO there might be some other windows that have something like this, but this is probably the biggest contribution
        self.nextturnbutton = tk.Button(
            self.app, width=18, height=2, command=self.endTurnClick, text="Next Turn", font=14)
        #self.nextturnbutton.hover = HoverInfo(self.nextturnbutton, "just a test")
        self.nextturnbutton.hover = CreateToolTip(self.nextturnbutton, "Beendet den Zug und started den Zug des nächsten Spielers. Ist die Runde komplett, i.e alle Spieler waren am Zug beginnt die nächste Runde. Keybinding: TODO")
        self.nextturnbutton.place(x=self.windowxsize, y=0, in_=self.app, anchor=tk.NE)

        btn = tk.Button(self.app, width=18, height=2, command=self.shipvendorcanvas.create, text="Schiff kaufen", font=self.fontsize)
        btn.place(x=self.windowxsize, y=2*self.font_px, in_=self.app, anchor=tk.NE)

        btn = tk.Button(self.app, width=18, height=2, command=self.sciencecanvas.create, text="Wissenschaft", font=self.fontsize)
        btn.place(x=self.windowxsize, y=4*self.font_px, in_=self.app, anchor=tk.NE)

        btn = tk.Button(self.app, width=18, height=2, command=self.politicscanvas.create, text="Politik", font=14)
        btn.place(x=self.windowxsize, y=6*self.font_px, in_=self.app, anchor=tk.NE)

        btn = tk.Button(self.app, width=18, height=2, command=self.diplomacycanvas.create, text="Berater", font=14)
        btn.place(x=self.windowxsize, y=8*self.font_px, in_=self.app, anchor=tk.NE)

        btn = tk.Button(self.app, width=18, height=2, command=self.roundcontrolcanvas.create, text="Rundenrad", font=14)
        btn.place(x=self.windowxsize, y=10*self.font_px, in_=self.app, anchor=tk.NE)

        btn = tk.Button(self.app, width=18, height=2, command=self.createBankCanvas, text="Handel (Bank)", font=14)
        btn.place(x=self.windowxsize, y=12*self.font_px, in_=self.app, anchor=tk.NE)

        btn = tk.Button(self.app, width=18, height=2, command=self.missioncanvas.create, text="Missionen", font=14)
        btn.place(x=self.windowxsize, y=14*self.font_px, in_=self.app, anchor=tk.NE)
        
        self.control.addStateObserver(self)

    #action can be a single action or a list of actions
    def updateState(self, action=None, actions = None, new_game = False):
        #TODO make this more efficient
        #TODO if load we need to set selectedobject to None
        self.printLog("update state called!")
        if not actions is None: #TODO dont repaint just because list (dont want to simply loop either because if there is a reason to repaint simply do that once)
            repainted = False
            for a in actions:
                repainted = self.updateStateSingle(a, repainted=repainted) or repainted
        elif not action is None:
            self.updateStateSingle(action)
        else:
            if new_game:
                self.repaint(force_all=True)
            else:
                self.repaint()
        #update subwindows that are always visible
        self.tmppointcanvas.updateState(action)
        self.battleshipcanvas.updateState(action)

    #returns true if repainting everything and false otherwise
    def updateStateSingle(self, action, repainted = False):
        if action.getType() in ["moveRessourceFromTo"]: #more here (everything the is not visible)
            pass
        elif action.getType() == "init":
            #TODO
            #somehow need to make sure here that the newobject is actually a mapobject (and not another tagged because cant be painter)
            #also what to do about others?
            newobj = self.control.getObjectByUuid(action.getArgs()["uuid"])
            self.paintObject(newobj)
        elif action.getType() == "moveMapObject":
            objuuid = action.getArgs()["objuuid"]
            obj = self.control.getObjectByUuid(objuuid)
            if action.getArgs()["origin"] == -1:
                self.paintObject(obj)
            elif obj.getPos() == -1:
                self.canvas.delete(objuuid)
            elif self.useanimation:
                moveAnimation(self.canvas, obj, self.tileToPos(action.getArgs()["origin"]), self.tileToPos(obj.getPos()), 60)
            else:
                self.canvas.delete(objuuid)
                self.paintObject(obj)

            #target handling
            if obj.getType() in tradingshiptypes:
                if obj.isMoving():
                    # highlight stuff (and remove old highlighting)
                    self.canvas.delete("targets")
                    for v in self.selectedobject.getValidTargets():
                        posxy = self.tileToPos(v)
                        self.canvas.create_oval(posxy.x-int(0.5*self.iconxsize), posxy.y-int(0.5*self.iconysize), posxy.x+int(0.5*self.iconxsize), posxy.y+int(0.5*self.iconysize), tags="targets")
                else:
                    self.canvas.delete("targets")
                    #TODO add lock to disallow any other action
                    townuuid = obj.getCurrTradingTown()
                    if not townuuid is None:
                        townobj = self.control.getObjectByUuid(townuuid)
                        if townobj.getType() in harbourtypes:
                            self.actioncanvas.create(townobj, obj, "buildandrefine")
                        else:
                            self.actioncanvas.create(townobj, obj, "trading")
            elif obj.getType() == "fleet":
                self.canvas.delete("targets")
                for v in self.selectedobject.getValidTargets():
                    posxy = self.tileToPos(v)
                    self.canvas.create_oval(posxy.x-int(0.5*self.iconxsize), posxy.y-int(0.5*self.iconysize), posxy.x+int(0.5*self.iconxsize), posxy.y+int(0.5*self.iconysize), tags="targets")
            elif obj.getType() == "pirate":
                pass
            else:
                self.printWarning("Uncaught moving object type "+obj.getType())
                
        elif action.getType() == "updateRandom":
            #TODO somehow this lags on newround after the animation but before destroying the local canvas
            if self.useanimation:
                randDice(self.app, self, res=[self.control.getRandomState().getPrimaryRandom(), self.control.getRandomState().getSecondaryRandom()])
        elif action.getType() == "temporaryRandomNumbers":
            if self.useanimation:
                randDice(self.app, self, res=action.getArgs()["numbers"])
        else:
            if not repainted:
                self.repaint()
            return True
        return False

    # Idea here is that options contains a list of UUID to be chosen from
    def requestAdditionalArgument(self, options: List[Any]) -> Any:
        #contains uuids to select from
        if type(options[0]) == str:
            return self.syncedSelector.select(options)
        else:
            self.printWarning("Missing implementation to handle list with type " + type(options[0]))

    def getResponsiblePlayers(self) -> List[str]:
        return self._responsiblePlayers

    def setResponsiblePlayers(self, players: List[str]) -> None:
        self._responsiblePlayers = players

    #TODO also log to file for later inspection
    def printLog(self, text: str) -> None:
        print("LOG: " + text)

    def printInfo(self, text: str) -> None:
        print(bcolors.OKGREEN + text + bcolors.ENDC)

    def printWarning(self, text: str) -> None:
        print(bcolors.WARNING + text + bcolors.ENDC)

    def printError(self, text: str) -> None:
        print(bcolors.FAIL + text + bcolors.ENDC)

    def printDev(self, text: str) -> None:
        print(bcolors.OKBLUE + text + bcolors.ENDC)

    def selectFleet(self, fleet):
        #TODO not sure this is the good solution
        self.selectedobject = self.control.getObjectByUuid(fleet)
        self.selectedobject.computeValidTargets()
        self.selectedobject.computeAttackTargets()
        # should be called whenever a fleet is selected, handles painting targets and stuff
        self.canvas.delete("targets")
        # display canvas with the ships (I think tmpcanvas does the job)
        if self.selectedobject.getPos() != -1:
            self.tmpinfocanvas.create(self.selectedobject, type="ships")
        # can only move if it belongs to the currently active player
        if self.selectedobject.getOwner() == self.control.getActivePlayer() and self.selectedobject.getCurrRange() > 0:
            for v in self.selectedobject.getValidTargets():
                posxy = self.tileToPos(v)
                self.canvas.create_oval(posxy.x-int(0.5*self.iconxsize), posxy.y-int(0.5*self.iconysize),
                                        posxy.x+int(0.5*self.iconxsize), posxy.y+int(0.5*self.iconysize), tags="targets")

            for t in self.selectedobject.getAttackTargets():
                posxy = self.tileToPos(t)
                self.canvas.create_oval(posxy.x-int(0.5*self.iconxsize), posxy.y-int(0.5*self.iconysize), posxy.x+int(
                    0.5*self.iconxsize), posxy.y+int(0.5*self.iconysize), tags="targets", outline="red")  # change color
    
    def showInfo(self, text, function=None):
        self.infocanvas.create(text=text, function=function)
        
    ###private
    def repaint(self, force_all = False):
        self.printInfo("repainting, this is expensive...!")
        force_all = True #TODO force_all = False is somehow bugged
        if force_all:
            self.printLog("forcing repaint all!")
            self.canvas.delete("all")   
        else:
            self.canvas.delete("changingInGame")
        self.imgs=[]
        img = ImageTk.PhotoImage(Image.open(os.path.join(os.environ["SCHIFFLI_BASE_PATH"], "tmpmap.png")))
        self.canvas.create_image(0, 0, anchor=tk.NW, image=img)
        self.imgs.append(img)
        if self.displayfieldnumber:
            for i in range(globalworld.nfields):
                uv = globalworld.mappolygons[i].centroid
                pos = self.uvToCanvas(uv)
                _ = self.canvas.create_text(pos.x, pos.y, text=str(
                    i), tags="label"+str(i), font=tk.font.Font(size=12))
        for i in range(globalworld.nfields):
            #get all objects at this position
            objects = self.control.getObjectsByPos(i)
            for obj in objects:
                self.paintObject(obj, force_all = force_all)
        self.nextturnbutton.configure(background=self.control.getActivePlayerObj().getColor())  # , activebackround=self.getActivePlayer().getColor()) #activebackround doesnt work...
        

    def paintObject(self, obj, force_all = False):
        #TODO find proper fix for this
        if not (isinstance(obj, AbstractMapObject) or (force_all and isinstance(obj, RessourceTile))):
            #mainwindow only paints mapobjects directly
            return
        getPosAttr = getattr(obj, "getIcon", None)
        if not callable(getPosAttr):
            return
        if obj.getPos() == -1:
            #this is special for each object so we need to treat them independently
            #which objects can have -1 at all
            pass
        else:
            img = ImageTk.PhotoImage(Image.open(obj.getIcon()))
            if obj.getType() in buildingtypes: 
                posxy = self.uvToCanvas(globalworld.buildingposition[obj.getPos()])   
            elif obj.getType() in allrtypes:
                posxy = self.uvToCanvas(globalworld.ressourceposition[obj.getPos()][obj.getLocalIdx()])
            else:
                posxy = self.tileToPos(obj.getPos())
            
            #TODO better solution for this
            if isinstance(obj, RessourceTile):
                tags = tuple([obj.getUuid()]+obj.getTags())
            else:
                tags = tuple(["changingInGame", obj.getUuid()]+obj.getTags())

            self.canvas.create_image(posxy.x, posxy.y, anchor=tk.CENTER, image=img, tags=tags) #TODO think about wether we want the uuid here top
            self.imgs.append(img)

    def newGameClick(self, event=0):
        self.control.newGame()

    # These functions are for handling the map/canvas
    def uvToCanvas(self, uv):
        return Point(uv.x*self.boardxdim, uv.y*self.boardydim)

    def canvasToUv(self, pos):
        return Point(pos.x/self.boardxdim, pos.y/self.boardydim)

    def uvToTile(self, uv):
        for i in range(globalworld.nfields):
            if globalworld.mappolygons[i].contains(uv):
                return i

        # no polygon was clicked should just ignore this
        return -1

    def tileToUv(self, tile):
        return globalworld.mappolygons[tile].centroid

    def posToTile(self, pos):
        return self.uvToTile(self.canvasToUv(pos))

    def tileToPos(self, tile):
        return self.uvToCanvas(self.tileToUv(tile))

    # This function handles anything that is clicked on the map
    # so it requires a lot of cases on what point the game is currently
    def tileClick(self, event):
        if self.mapblocked:
            return
        # comment is for getting point where clicked in uv coordinates
        # if self.pointlist==[]:
        #    self.pointlist.append(self._posToTile(Point(event.x, event.y)))
        #self.pointlist.append("Point("+str(event.x/self.boardxdim)+", "+str(event.y/self.boardydim)+")")
        # return

        # comment is for getting polygons by clicking
        #self.pointlist.append("("+str(event.x/self.boardxdim)+", "+str(event.y/self.boardydim)+")")
        # return

        # getting the polygon that was clicked
        pos = self.posToTile(Point(event.x, event.y))
        # no valid tile clicked
        if pos == -1:
            return
        if self.control._devmode and self.runningdev:
            #add uuid to text input
            if len(self.control.getObjectsByPos(pos, tag="clickable")) == 1:
                # if there is one object on that tile we know which one is selected (also we know that buildings are only possible here, since cannot be stacked)
                self.selectMapobject(pos)

            elif len(self.control.getObjectsByPos(pos, tag="clickable")) > 1:
                # here we need to give the user an option which one he actually wants
                self.createMultiselectionCanvas(pos)

        # start of the game. this should be false in all other cases
        elif self.control.placingSettlements():
            try:
                self.control.placeSettlement(pos)
            except InvalidTarget:
                pass
            except:
                self.printError("Unexpected error:", sys.exc_info()[0])
                raise

        elif self.syncedSelector.selectsynced:
            if not pos in self.syncedSelector.validtargets.keys():
                # not a valid target
                return
            if len(self.syncedSelector.validtargets[pos]) == 1:
                self.syncedSelector.result = self.syncedSelector.validtargets[pos][0]
                self.syncedSelector.syncvar.set(1)
            else:
                self.printLog(len(self.syncedSelector.validtargets[pos]))
                self.printDev("TODO show multisleectioncanvas with all validtargets at pos. probably need to extend muliselectioncanvas slightly")
                self.syncedSelector.syncvar.set(1)
        # something has been selected previously.
        # If a valid action for the selected object is taken it should perform that action. otherwise it gets unselected
        elif self.selectedobject != None:
            # here we already have selected something so we have different actions
            if self.selectedobject.getType() in tradingshiptypes:
                # check if move is allowed otherwise unselect if its not moving (must complete movement once started)
                if pos in self.selectedobject.getValidTargets():
                    self.control.moveMapObject(self.selectedobject.getUuid(), target=pos)

                elif self.selectedobject.isMoving():
                    # cannot do anything besides move so just ignore
                    return

                else:
                    # if not started and no valid target unselect and remove highlighting
                    self.canvas.delete("targets")
                    self.selectedobject = None

            elif self.selectedobject.getType() == "fleet":
                if pos in self.selectedobject.getValidTargets():
                    self.control.moveMapObject(self.selectedobject.getUuid(), target=pos)
                elif pos in self.selectedobject.getAttackTargets():
                    #TODO create attack function in control
                    
                    # should not just use first element but fleet on that field (in case there is a fleet and a tradingship)
                    self.canvas.delete("targets")
                    self.selectedobject = None
                else:
                    # if not started and no valid target unselect and remove highlighting
                    self.canvas.delete("targets")
                    self.selectedobject = None

        elif len(self.control.getObjectsByPos(pos, tag="clickable")) == 1:
            # if there is one object on that tile we know which one is selected (also we know that buildings are only possible here, since cannot be stacked)
            self.selectMapobject(pos)

        elif len(self.control.getObjectsByPos(pos, tag="clickable")) > 1:
            # here we need to give the user an option which one he actually wants
            self.createMultiselectionCanvas(pos)

        else:
            # mostlikely the user clicked on an empty tile accidentally
            return

    # pos2 is stacked position in case of multiple
    def selectMapobject(self, pos, pos2=0, frommulti=False):
        if frommulti:
            self.multiSelectionCanvasLeave(0)
        
        # might want do destroy multiselectioncanvas here (if existing)
        obj = self.control.getObjectsByPos(pos, tag="clickable")[pos2]
        if self.control._devmode and self.runningdev:
            self.devCmdText.insert(tk.END, obj.getUuid())
        
        elif obj.getType() in buildingtypes:
            self.tmprescanvas.create(obj)

        elif obj.getType() in tradingshiptypes:
            self.tmprescanvas.create(obj)

            # if the owner of the ship is the currently active player make this ship selected (it only starts moving once a valid destination is selected. then however it must be moved until the end
            if obj.getOwner() == self.control.getActivePlayer() and obj.getCurrRange() > 0:
                self.selectedobject = obj
                # highlight stuff (and remove old highlighting)
                self.selectedobject.computeValidTargets()
                self.canvas.delete("targets")
                for v in self.selectedobject.getValidTargets():
                    posxy = self.tileToPos(v)
                    self.canvas.create_oval(posxy.x-int(0.5*self.iconxsize), posxy.y-int(0.5*self.iconysize), posxy.x+int(0.5*self.iconxsize), posxy.y+int(0.5*self.iconysize), tags="targets")

        elif obj.getType() == "fleet":
            self.selectFleet(obj.getUuid())
        
        elif obj.getType() in harbourtypes:
            self.tmpinfocanvas.create(obj, type = "harbourinfo")

    # these function handle multi selection (i.e. multiple objects on single tile)
    def createMultiselectionCanvas(self, pos):
        objects = self.control.getObjectsByPos(pos, tag="clickable")
        nobjects = len(objects)
        self.multiselectioncanvas = tk.Canvas(self.app, width=self.iconxsize, height=nobjects*self.iconysize//2, bd=0)
        self.multiselectioncanvas.bind("<Leave>", self.multiSelectionCanvasLeave)

        # for now like that, might be nicer to position slightly differently
        posxy = self.tileToPos(pos)
        self.multiselectioncanvas.place(
            x=posxy.x, y=posxy.y, in_=self.canvas, anchor=tk.W)

        for i in range(nobjects):
            b1BG = self.multiselectioncanvas.create_rectangle(0, i*self.iconysize//2, self.iconxsize, (i+1)*self.iconysize//2, fill="grey40")
            b1 = self.multiselectioncanvas.create_text(0.5*self.iconxsize, i*self.iconysize//2+5, anchor=tk.N, text=str(objects[i].getType()), font=tk.font.Font(size=10))
            self.multiselectioncanvas.tag_bind(
                b1BG, "<Button-1>", lambda event, pos=pos, pos2=i: self.selectMapobject(pos, pos2=pos2, frommulti=True))
            self.multiselectioncanvas.tag_bind(
                b1, "<Button-1>", lambda event, pos=pos, pos2=i: self.selectMapobject(pos, pos2=pos2, frommulti=True))

    #TODO find a propper solution to this problem eventually
    def multiSelectionCanvasLeave(self, event):
        self.multiselectioncanvas.after(20, self.destroyMultiSelectionCanvas)
    # until here

    #add a little delay to this to make it robust to ordering of this vs click (which is random from what I understand)
    def destroyMultiSelectionCanvas(self):
        self.multiselectioncanvas.delete("all")
        self.multiselectioncanvas.destroy()
    

    # TODO all the buyingstuff to seperate file and make correct
    # functions to buy obliges

    def endTurnClick(self):
        # comment is for getting 2 points for ressources where clicked in uv coordinates
        #print(str(self.pointlist[0])+" : ["+self.pointlist[1]+", "+self.pointlist[2]+"],")
        # self.pointlist=[]
        # return

        # comment is for getting single points where clicked in uv coordinates
        #print(str(self.pointlist[0])+" : "+self.pointlist[1]+",")
        # self.pointlist=[]
        # return

        # comment is for getting polygons by clicking
        # string="p=Polygon(["
        # for i in range(len(self.pointlist)-1):
        #    string+=self.pointlist[i]+", "
        # string+=self.pointlist[-1]+"])"
        # self.pointlist=[]
        # print(string)
        # print("")
        # return
        self.control.nextTurn()

        

    # functions to display info on hover
    def createHoverInfo(self, app, text):
        # think about these numbers
        height = 20
        width = 40
        self.hoverinfocanvas = tk.Canvas(app, width=width, height=height, bd=0)
        # Think about this. problem is clickable(ideally want to click whats behin) and also transparency

    def destroyHoverInfo(self):
        self.hoverinfocanvas.destroy()
    # exit function

    def exitClick(self, event=0):
        # do some saving stuff maybe
        self.control.quit()
        self.app.quit()

    # save function
    def saveClick(self, event=0):
        self.control.save()

    def loadClick(self, event=0):
        filename = tk.filedialog.askopenfilename()
        if filename == '':
            return False
        self.control.load(filename)

    def createBankCanvas(self):
        self.payrescanvas.create(type="ressource", pos=self.uvToCanvas(Point(0.6, 0.3)))
        self.pickrescanvas.create(pos=self.uvToCanvas(Point(0.8, 0.3)), boundcanvas=self.payrescanvas)

    def openpdf(self, filename):
        #subprocess.Popen([os.path.join("files", filename)],shell=True)
        #open(os.path.join("files", filename))
        os.system(os.path.join(os.environ["SCHIFFLI_BASE_PATH"], os.path.join("files", filename)))


###################################################################################################################################################
                #Dev Tools
                #Maybe move them into seperate file eventually
###################################################################################################################################################
    def openCommandWindow(self):
        self.runningdev = True
        self.devwin = tk.Canvas(self.app, width=400, height=100, bd=0)
        self.devwin.place(x=0, y=5, in_=self.app, anchor=tk.NW)

        self.devCmdText = tk.Entry(self.devwin, width=30, font=tk.font.Font(size=12))
        self.devwin.create_window((5,10), window=self.devCmdText, anchor=tk.NW)
        self.devCmdText.bind("<Tab>", self.autoComplete)

        # button
        bBG = self.devwin.create_rectangle(335, 10, 395, 40, fill="grey40")
        b = self.devwin.create_text(365, 15, anchor=tk.N, text="Run", font=tk.font.Font(size=12))
        self.devwin.tag_bind(bBG, "<Button-1>", self.runCommand)
        self.devwin.tag_bind(b, "<Button-1>", self.runCommand)

        # button
        bBG = self.devwin.create_rectangle(175, 60, 225, 90, fill="grey40")
        b = self.devwin.create_text(200, 65, anchor=tk.N, text="Exit", font=tk.font.Font(size=12))
        self.devwin.tag_bind(bBG, "<Button-1>", self.closeCommandWindow)
        self.devwin.tag_bind(b, "<Button-1>", self.closeCommandWindow)
    
    def runCommand(self, event=0):
        Cmd = self.devCmdText.get()
        funcname = Cmd.split('(')[0]
        args = Cmd.split('(')[1].rstrip(')').split(',')
        try:
            func = getattr(self.control, funcname)
        except:
            self.printWarning("Function: " + funcname +" is not a function of game control")
        for idx, a in enumerate(args):
            if a == "None" or a == "none":
                args[idx] = None
            else:
                try:
                    args[idx] = int(a)
                except:
                    args[idx] = a.rstrip(' ').lstrip(' ')
        func(*args)

    def autoComplete(self, event=0):
        #TODO somehow make tab not lose focus on input
        #get the text
        Cmd = self.devCmdText.get()
        if len(Cmd.split('('))>1:
            #no autocompletion for arguments implemented yet
            self.printDev("no autocompletion for arguments implemented yet")
            return 'break'
        tmplist = inspect.getmembers(self.control, predicate=inspect.isroutine)
        funcnames = [t[0] for t in tmplist]
        candidates = []
        for f in funcnames:
            #TODO maybe llok into making this not casesensitive
            if len(f)>=len(Cmd) and f[0:len(Cmd)]==Cmd:
                candidates.append(f)
        if len(candidates) == 0:
            #TODO somehow inform the user that there is no possibility
            return 'break'
        elif len(candidates) == 1:
            self.devCmdText.insert(tk.END, candidates[0][len(Cmd):])
            self.devCmdText.insert(tk.END, '(')
            return 'break'
        else:
            #iterate over candidates and insert a letter when all are equal
            currpos = len(Cmd)
            while True:
                tmp = candidates[0][currpos]
                for c in candidates:
                    if len(c)<=currpos or not c[currpos] == tmp:
                        return 'break'
                self.devCmdText.insert(tk.END, tmp)
                currpos += 1
            return 'break'
    
    def closeCommandWindow(self, event=0):
        self.runningdev = False
        self.devwin.destroy()

    def iPythonEmbed(self, event=0):
        try:
            IPython.embed()
        except:
            self.printError("could not start IPython embedding, most likely you need to install IPython")