import types
import abc

class Base(object):
    def generic(self, text):
        pass
    def special(self, text):
        pass

class A(Base):
    def __init__(self):
        def myfunc(name, *args, **kwargs):
            print("generic " + name + " with: " + str(args) + str(kwargs))
        for name, _ in Base.__dict__.items():
            if not name.startswith("__") and not name in A.__dict__.keys():
                setattr(self, name, lambda self, fname=name, *args, **kwargs: myfunc(fname, *args, **kwargs))

    def special(self, text):
        print("non-generic: " + text)
    
    # def __getattr__(self, name, *args, **kwargs):
    #     def myfunc(self, *args, **kwargs):
    #         return "generic " + name + " with: " + str(args) + str(kwargs)
    #     meth = types.MethodType(myfunc, self)
    #     return meth