#TODO more as needed
class InvalidTarget(Exception):
    pass

class InvalidAction(Exception):
    pass

class TemporaryInvalidAction(InvalidAction):
    pass

class NotEnoughRessources(Exception):
    pass

class NotEnoughSpace(Exception):
    pass

class InvalidConfiguration(Exception):
    pass

class BadCode(Exception):
    pass