#THIS FILE IS REQUIRED TO INCLUDE THIS DIRECTORY AS A MODULE
#DO NOT DELETE THIS FILE

from schiffli_util.world import World
from schiffli_util.definitions import *
from schiffli_util.exceptions import *
from schiffli_util.util import *
from schiffli_util.logger import SchiffliLogger

from schiffli_util.functions import *

globalworld=World()
