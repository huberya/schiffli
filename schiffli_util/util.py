import screeninfo

#taken from https://stackoverflow.com/questions/287871/how-to-print-colored-text-in-terminal-in-python
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class GameConfig:
    def __init__(self):
        self.seed = -1
        self.devmode = False
        #TODO somehow need to assign type of observer to players (probably by color)
        self.nplayers = 4

def mm_to_inch(l: float) -> float:
    return 0.03937008*l

def inch_to_mm(l: float) -> float:
    return l/0.03937008

#TODO optimize chache the divisor
def mm_to_pixel(l: float) -> int:
    m = [x for x in screeninfo.get_monitors() if x.x==0][0]
    return round(l*0.5*(m.height/m.height_mm + m.width/m.width_mm))

def point_to_pixel(p: int) -> int:
    return mm_to_pixel(inch_to_mm(p/72))
