import random
import numpy as np
import os
import time
import re
from PIL import Image, ImageTk
from shapely.geometry import Point
import copy
import math

from typing import Any, List, Optional

#Note: here we need to be a bit careful as it imports from the util folder and might lead to circular
from schiffli_util.definitions import imagefolder_rescaled, diceimages
from schiffli_util.tkimports import *
from schiffli_util.util import bcolors
    
# some general utility functions

def depth(data: Any) -> int:
    level = 0
    while True:
        try:
            if type(data)==str:
                return level
            data=data[0]
        except:
            return level
        level+=1

def argWhereType(data: List[Any], stype: str) -> List[int]:
    res = []
    for idx, d in enumerate(data):
        if d.getType() == stype:
            res.append(idx)
    return res

def polarToCart(r: float = 0, alpha: float = 0) -> Point:
    return Point(math.cos(alpha) * r, math.sin(alpha) * r)
    
# maybe make this efficient (i.e. start from both and use heuristics maybe)


def computeDistance(p1: int, p2: int, connectivity: Any) -> int:
    # what is the maximal reasonable depth?:
    maxdepth = 100
    visited = [p1]
    current = [p1]
    for i in range(maxdepth):
        tmp = current
        current = []
        for t in tmp:
            # I have no clue why this gives a nested list...but it does, so we reshape
            candidates = np.argwhere(connectivity[t, :] == 1).reshape(-1)
            for c in candidates:
                if c in visited:
                    continue
                elif c == p2:
                    return i
                else:
                    current.append(c)
                    visited.append(c)

    return maxdepth

#type of obj shoud be : 'smobjects.AbstractMapObject'
def moveAnimation(canvas: tk.Canvas, obj, origin: Point, target: Point, frames: int, currframe: int = 1, duration: int = 1000) -> bool:
    if obj is None:
        # probably got deleted in the meantime
        return True
    if currframe == 1:
        if obj.isAnimated():
            # wait for 1 frame and try again
            obj.appendAnimation(lambda canvas=canvas, obj=obj, origin=origin, target=target, frames=frames, currframe=currframe, duration=duration: moveAnimation(canvas, obj, origin, target, frames, currframe, duration))
            return False  # not completed yet waiting
        else:
            obj.startAnimation()

    if currframe > frames:
        #check if more animations are pending (careful getNextAnimation has sideeffects):
        animfunc = obj.getNextAnimation()
        if not animfunc is None:
            obj.stopAnimation() #need to call this for next part to start
            animfunc()
            return False
        else:
            obj.stopAnimation()
            return True  # completed

    canvas.coords(obj.getUuid(), int((1-currframe/frames)*origin.x+currframe / frames*target.x), int((1-currframe/frames)*origin.y+currframe/frames*target.y))
    canvas.after(int(duration/frames), lambda canvas=canvas, obj=obj, origin=origin, target=target, frames=frames, currframe=currframe+1, duration=duration: moveAnimation(canvas, obj, origin, target, frames, currframe, duration))
    return False  # not completed yet still running


# other then moveanimation, this is syncronized (i.e. actions only continue AFTER animation is completed)
#TODO what types
def randDice(tkinstance, mainwindow, res=[]):
    length = random.randint(5, 15)
    numbers = np.random.randint(low=1, high=7, size=(len(res), length))
    numbers[:,-1]=np.asarray(res).reshape(-1)
    # build canvas and call rolldice animation
    canvas = tk.Canvas(tkinstance, width=(len(res)+1)*mainwindow.iconxsize, height=2*mainwindow.iconysize, bd=0)
    canvas.place(x=mainwindow.windowxsize//2, y=mainwindow.windowysize // 2, in_=tkinstance, anchor=tk.CENTER)

    var = tk.IntVar()
    diceAnimation(canvas, numbers, 0, mainwindow, var)

    canvas.wait_variable(var)

    var2 = tk.IntVar()
    canvas.after(1800, lambda canvas=canvas, var2=var2: destroyDiceCanvas(canvas, var2))
    canvas.wait_variable(var2)

    return True

#TODO what types
def diceAnimation(canvas, numbers, curridx, mainwindow, var):
    if curridx >= numbers.shape[1]:
        var.set(1)
        return True

    canvas.imgs = []
    canvas.delete("all")

    for i in range(numbers.shape[0]):
        img = ImageTk.PhotoImage(Image.open(os.path.join(
            imagefolder_rescaled, diceimages[numbers[i, curridx]])))
        canvas.create_image((i+1)*mainwindow.iconxsize, mainwindow.iconysize, anchor=tk.CENTER, image=img)
        canvas.imgs.append(img)

    canvas.after(200, lambda canvas=canvas, numbers=numbers, curridx=curridx+1, mainwindow=mainwindow, var=var: diceAnimation(canvas, numbers, curridx, mainwindow, var))
    return False

#TODO what types
def destroyDiceCanvas(canvas, var):
    canvas.imgs = []
    canvas.delete("all")
    canvas.destroy()
    var.set(1)
    return True

#Maybe make this as base aswell TODO
#function is used for science cards (and maybe also oblige later) to select targets when the effect is targeted
#it blocks until the user selected a target and only then returns (it should somehow highlight possible targets)
class SyncedSelector:
    def __init__(self, control, mainwindow):
        self._mainwindow = mainwindow
        self._control=control
        self.selectsynced = False
        self.validtargets = {}
        self.syncvar = None
        self.result = None
        
    def select(self, options: List[str]): #TODO add more options
        self.selectsynced = True
        self.syncvar = tk.IntVar()
        for option in options:
            obj = self._control.getObjectByUuid(option)
            pos = obj.getPos()
            if pos in self.validtargets.keys():
                self.validtargets[pos].append(option)
            else:
                self.validtargets[pos] = [option]

        for k in self.validtargets.keys():
            posxy = self._mainwindow.tileToPos(k)
            self._mainwindow.canvas.create_oval(posxy.x-int(0.5*self._mainwindow.iconxsize), posxy.y-int(0.5*self._mainwindow.iconysize), posxy.x+int(
                0.5*self._mainwindow.iconxsize), posxy.y+int(0.5*self._mainwindow.iconysize), tags="synctargets")

        self._mainwindow.canvas.wait_variable(self.syncvar)
        self._mainwindow.canvas.delete("synctargets")
        self.selectsynced = False
        self.validtargets = {}
        self.syncvar = None
        return self.result

    
#maybe make this as base class TODO
# taken fromhttps://stackoverflow.com/questions/3221956/how-do-i-display-tooltips-in-tkinter
# can we do something like no remove it when we leave the parent just to get inside it and also on Button-1 redirect event to parent?
class CreateToolTip(object):

    # create a tooltip for a given widget
    def __init__(self, widget, text='widget info', parent=None):
        self.waittime = 500  # miliseconds
        self.leavedelay = 50
        self.active = False
        self.wraplength = 180  # pixels
        self.widget = widget
        self.text = text
        self.parent = parent
        if parent is None:
            self.widget.bind("<Enter>", self.enter)
            self.widget.bind("<Leave>", self.leavedelayed)
            self.widget.bind("<ButtonPress>", self.leave)
        else:
            self.parent.tag_bind(widget, "<Enter>", self.enter)
            self.parent.tag_bind(widget, "<Leave>", self.leavedelayed)
            self.parent.tag_bind(widget, "<ButtonPress>", self.leave)

        self.idin = None
        self.idout = None
        self.tw = None

    def enterself(self, event=None):
        #self.unschedule() think about necessity of this
        self.unscheduledelayed()

    def clickself(self, event=None):
        if self.parent is None:
            self.widget.invoke() #is semi-working: button changes colors as if round was changing but nod ice...
            self.leave()
        else:
            print("TODO") #can we do this and how

    def enter(self, event=None):
        if self.active:
            #self.unschedule()
            self.unscheduledelayed()
        else:
            self.schedule()

    def leavedelayed(self, event=None):
        self.unschedule(type="out")
        if self.parent is None:
            self.idout = self.widget.after(self.leavedelay, self.leave)
        else:
            self.idout = self.parent.after(self.leavedelay, self.leave)

    def leave(self, event=None):
        self.unschedule()
        self.hidetip()

    def schedule(self):
        self.unschedule()
        if self.parent is None:
            self.idin = self.widget.after(self.waittime, self.showtip)
        else:
            self.idin = self.parent.after(self.waittime, self.showtip)

    def unscheduledelayed(self): #is because of order of execution, that otherwise might miss by some ms
        if self.parent is None:
            self.widget.after(5, self.unschedule)
        else:
            self.parent.after(5, self.unschedule)

    def unschedule(self, type=None):
        if not self.idin is None:
            id = self.idin
            self.idin = None
            if id:
                if self.parent is None:
                    self.widget.after_cancel(id)
                else:
                    self.parent.after_cancel(id)

        if not self.idout is None and (type is None or type=="out"):
            id = self.idout
            self.idout = None
            if id:
                if self.parent is None:
                    self.widget.after_cancel(id)
                else:
                    self.parent.after_cancel(id)

    def showtip(self, event=None):
        self.active = True
        # TODO think about this in case of tag_bind (canvas)
        x = y = 0
        x, y, _, _ = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        # creates a toplevel window
        self.tw = tk.Toplevel(self.widget)
        # Leaves only the label and removes the app window
        self.tw.wm_overrideredirect(True)
        self.tw.wm_geometry("+%d+%d" % (x, y))
        label = tk.Label(self.tw, text=self.text, justify='left',
                         background="#ffffff", relief='solid', borderwidth=1,
                         wraplength=self.wraplength)
        label.pack(ipadx=1)
        self.tw.bind("<Enter>", self.enterself)
        self.tw.bind("<Leave>", self.leavedelayed)
        self.tw.bind("<Button-1>", self.clickself)

    def hidetip(self):
        self.active = False
        tw = self.tw
        self.tw = None
        if tw:
            tw.destroy()


def combineDicts(dict1, dict2):
    res = copy.deepcopy(dict1)
    for k, v in dict2.items():
        if k in res.keys():
            if type(res[k])==list:
                if type(v)==list:
                    res[k]+=v
                    continue
                elif depth(v)==0:
                    res[k].append(v)
                    continue
            elif type(res[k])==dict and type(v)==dict:
                res[k]=combineDicts(res[k], v)
            #more types TODO?

            
            print("unknown way to combine types "+str(type(res[k]))+" and "+str(type(v)))
        else:
            res[k]=v
    return res
