# here come general imports
import os
import numpy as np
from shapely.geometry.polygon import Polygon
from typing import List, Dict, Any


# existing ressource types
allrtypes: List[str] = ["Holz", "Erz", "Schaf", "Feuerwerk", "Elfenbein", "Kristall-Erz", "Totem", "Eisen", "Werkzeug", "Segel", "Stahl",
             "Pergament", "Silber", "Lektüre", "Quarz", "Juwelen", "Schmuck", "Rau-Gold", "Diamant", "Waffen", "Panzerung", "Fein-Gold", "Messgerät"]

# ressource trading
ankaufvalues: Dict[str, int] = {
    "Holz": 1,
    "Erz": 1,
    "Schaf": 1,
    "Feuerwerk": 2,
    "Elfenbein": 3,
    "Kristall-Erz": 2,
    "Totem": 5,
    "Eisen": 4,
    "Werkzeug": 4,
    "Segel": 4,
    "Stahl": 5,
    "Pergament": 3,
    "Silber": 4,
    "Lektüre": 5,
    "Quarz": 8,
    "Juwelen": 5,
    "Schmuck": 6,
    "Rau-Gold": 7,
    "Diamant": 8,
    "Waffen": 8,
    "Panzerung": 8,
    "Fein-Gold": 10,
    "Messgerät": 12
}

verkaufvalues: Dict[str, int] = {
    "Holz": 3,
    "Erz": 3,
    "Schaf": 3,
    "Feuerwerk": 5,
    "Elfenbein": 5,
    "Kristall-Erz": 4,
    "Totem": 8,
    "Eisen": 7,
    "Werkzeug": 7,
    "Segel": 7,
    "Stahl": 8,
    "Pergament": 5,
    "Silber": 8,
    "Lektüre": 10,
    "Quarz": 15,
    "Juwelen": 9,
    "Schmuck": 12,
    "Rau-Gold": 13,
    "Diamant": 14,
    "Waffen": 12,
    "Panzerung": 12,
    "Fein-Gold": 16,
    "Messgerät": 18
}

sciencevalues: Dict[str, int] = {
    "Kristall-Erz": 2,
    "Feuerwerk": 3,
    "Pergament": 5,
    "Silber": 6,
    "Messgerät": 15,
    "Lektüre": 20,
    "Quarz": 30
}

politicsvalues: Dict[str, int] = {
    "Elfenbein": 1,
    "Totem": 2,
    "Juwelen": 2,
    "Rau-Gold": 3,
    "Schmuck": 4,
    "Diamant": 5,
    "Fein-Gold": 5

}

image_base_path: str = os.path.join(os.environ["SCHIFFLI_BASE_PATH"], "images")
imagefolder_rescaled: str = os.path.join(image_base_path, "rescaled")

allicons: Dict[str, str] = {
    # put the name of the files for the icons of all the stuff here (need to replace for now everything is just a sail)
    # ressources
    "Holz": "Icon_Ress_Holz.png",
    "Erz": "Icon_Ress_Erz.png",
    "Schaf": "Icon_Ress_Schaf.png",
    "Feuerwerk": "Icon_Ress_Feuerwerk.png",
    "Elfenbein": "Icon_Ress_Elfenbein.png",
    "Kristall-Erz": "Icon_Ress_Kristallerz.png",
    "Totem": "Icon_Ress_Totem.png",
    "Eisen": "Icon_Ress_Eisen.png",
    "Werkzeug": "Icon_Ress_Werkzeug.png",
    "Segel": "Icon_Ress_Segel.png",
    "Stahl": "Icon_Ress_Stahl.png",
    "Pergament": "Icon_Ress_Pergament.png",
    "Silber": "Icon_Ress_Silber.png",
    "Lektüre": "Icon_Ress_Lektüre.png",
    "Quarz": "Icon_Ress_Quarz.png",
    "Juwelen": "Icon_Ress_Juwelen.png",
    "Schmuck": "Icon_Ress_Schmuck.png",
    "Rau-Gold": "Icon_Ress_RauGold.png",
    "Diamant": "Icon_Ress_Diamant.png",
    "Waffen": "Icon_Ress_Waffen.png",
    "Panzerung": "Icon_Ress_Panzerung.png",
    "Fein-Gold": "Icon_Ress_Feingold.png",
    "Messgerät": "Icon_Ress_Messgeräte.png",
    # ships
    "Kogge": "H1_Kogge_Icon.png",
    "Brigantine": "H2_Brigantine_Icon.png",
    "Schoner": "H3_Schoner_Icon.png",
    "Panzerschiff": "placeholder.png",
    "Brigg": "H5_Brigg_Icon.png",
    "Schweres Handels Schiff": "placeholder.png",
    "Kanonenboot": "1Kanonenboot_Icon.png",
    "Kleines Kriegsschiff": "2KlKriegsSchiff_Icon.png",
    "Belagerungs-Schiff": "3BelSchiff_Icon.png",
    "Fregatte": "4Fregatte_Icon.png",
    "Karavelle": "5Karavelle_Icon.png",
    "Korvette": "6Korvette_Icon.png",
    "Linienshiff": "7Linienschiff_Icon.png",
    "Mörserschiff": "8Mörserschiff_Icon.png",
    # buildings
    "settlement": "settlement.png",
    "outpost": "outpost.png",
    "fort": "fort.png",
    "metropole": "metropole.png",
    # fleet
    "fleet": "placeholder.png",
    # harbours
    "Giesserei": "Icon_HafStd_Giesserei.png",
    "Werkstatt": "Icon_HafStd_Werkstatt.png",
    "Docks": "Icon_HafStd_Docks.png",
    "Juwelier": "Icon_HafStd_Juwelier.png",
    "Goldschmied": "Icon_HafStd_Goldschmied.png",
    # pirates
    "pirate": "pirate.png"
}
allcards: Dict[str, str] = {
    # ressources
    "Holz": "Holz.png",
    "Erz": "Erz.png",
    "Schaf": "Schaf.png",
    "Feuerwerk": "Feuerwerk.png",
    "Elfenbein": "Elfenbein.png",
    "Kristall-Erz": "Kristallerz.png",
    "Totem": "Totem.png",
    "Eisen": "Eisen.png",
    "Werkzeug": "Werkzeug.png",
    "Segel": "Segel.png",
    "Stahl": "Stahl.png",
    "Pergament": "Pergament.png",
    "Silber": "Silber.png",
    "Lektüre": "Lektuere.png",
    "Quarz": "Qarz.png",
    "Juwelen": "Juwelen.png",
    "Schmuck": "Schmuck.png",
    "Rau-Gold": "RauGold.png",
    "Diamant": "Diamant.png",
    "Waffen": "Waffen.png",
    "Panzerung": "Panzerung.png",
    "Fein-Gold": "Feingold.png",
    "Messgerät": "Messgeräte.png",
}
# ship stats
# this is for trading ships
shipspeed: Dict[str, int] = {
    "Kogge": 3,
    "Brigantine": 6,
    "Schoner": 4,
    "Panzerschiff": 3,
    "Brigg": 8,
    "Schweres Handels Schiff": 4
}

shipboost: Dict[str, int] = {
    "Kogge": 3,
    "Brigantine": 3,
    "Schoner": 5,
    "Panzerschiff": 6,
    "Brigg": 3,
    "Schweres Handels Schiff": 7
}

shipcapacity: Dict[str, int] = {
    "Kogge": 5,
    "Brigantine": 6,
    "Schoner": 10,
    "Panzerschiff": 12,
    "Brigg": 8,
    "Schweres Handels Schiff": 20
}

# this is for both
shiparmor: Dict[str, int] = {
    "Kogge": 1,
    "Brigantine": 1,
    "Schoner": 3,
    "Panzerschiff": 5,
    "Brigg": 2,
    "Schweres Handels Schiff": 4,
    "Kanonenboot": 3,
    "Kleines Kriegsschiff": 4,
    "Belagerungs-Schiff": 2,
    "Fregatte": 5,
    "Karavelle": 1,
    "Korvette": 2,
    "Linienshiff": 4,
    "Mörserschiff": 2
}

shipcost: Dict[str, Dict[str, int]] = {
    "Kogge": {"Holz": 1, "Erz": 1},
    "Brigantine": {"Erz": 1, "Segel": 1, "Werkzeug": 1},
    "Schoner": {"Holz": 2, "Segel": 1},
    "Panzerschiff": {"Werkzeug": 1, "Eisen": 1},
    "Brigg": {"Werkzeug": 1, "Segel": 1, "Eisen": 1, "Stahl": 1},
    "Schweres Handels Schiff": {"Werkzeug": 2, "Segel": 1, "Panzerung": 1},
    "Kanonenboot": {"Holz": 2, "Erz": 1},
    "Kleines Kriegsschiff": {"Erz": 2, "Holz": 1, "Schaf": 1},
    "Belagerungs-Schiff": {"Schaf": 1, "Holz": 1, "Stahl": 1, "Eisen": 1},
    "Fregatte": {"Holz": 1, "Werkzeug": 1, "Segel": 1, "Eisen": 1},
    "Karavelle": {"Erz": 1, "Segel": 1, "Werkzeug": 1, "Stahl": 1},
    "Korvette": {"Holz": 2, "Segel": 1, "Eisen": 1},
    "Linienshiff": {"Eisen": 1, "Werkzeug": 1, "Waffen": 1, "Panzerung": 1},
    "Mörserschiff": {"Waffen": 2, "Werkzeug": 2}
}

# this is for war ships
shiphealth: Dict[str, int] = {
    "Kanonenboot": 1,
    "Kleines Kriegsschiff": 1,
    "Belagerungs-Schiff": 1,
    "Fregatte": 2,
    "Karavelle": 2,
    "Korvette": 3,
    "Linienshiff": 4,
    "Mörserschiff": 2
}

shipdamage: Dict[str, int] = {
    "Kanonenboot": 2,
    "Kleines Kriegsschiff": 3,
    "Belagerungs-Schiff": 2,
    "Fregatte": 0,
    "Karavelle": 4,
    "Korvette": 1,
    "Linienshiff": 4,
    "Mörserschiff": 2
}

shiprange: Dict[str, int] = {
    "Kanonenboot": 3,
    "Kleines Kriegsschiff": 3,
    "Belagerungs-Schiff": 5,
    "Fregatte": 4,
    "Karavelle": 4,
    "Korvette": 3,
    "Linienshiff": 2,
    "Mörserschiff": 6
}

buildingcost: Dict[str, Dict[str, int]] = {
    "outpost": {"Holz": 1, "Erz": 1, "Schaf": 1},
    "settlement": {"Holz": 2, "Erz": 2, "Schaf": 1},
    "metropole": {"Holz": 2, "Erz": 2, "Schaf": 1, "Werkzeug": 2, "Stahl": 1},
    "fort": {"Holz": 2, "Erz": 2, "Schaf": 1, "Eisen": 1}
}

#TODO what are correct values here?
buildingcapacity: Dict[str, int] = {
    "outpost": 5,
    "settlement": 5,
    "metropole": 5,
    "fort": 5
}

buildingdefense: Dict[str, int] = {
    "outpost": 8,
    "settlement": 9,
    "metropole": 9,
    "fort": 10
}

buildingattack: Dict[str, int] = {
    "outpost": 1,
    "settlement": 1,
    "metropole": 1,
    "fort": 4
}

buildingnattacks: Dict[str, int] = {
    "outpost": 1,
    "settlement": 2,
    "metropole": 3,
    "fort": 2
}

#TODO what are correct buildtimes
buildtime: Dict[str, int] = {
    "Kogge": 3,
    "Brigantine": 3,
    "Schoner": 3,
    "Panzerschiff": 3,
    "Brigg": 3,
    "Schweres Handels Schiff": 3,
    "Kanonenboot": 3,
    "Kleines Kriegsschiff": 4,
    "Belagerungs-Schiff": 2,
    "Fregatte": 5,
    "Karavelle": 1,
    "Korvette": 2,
    "Linienshiff": 4,
    "Mörserschiff": 2   
}

# information for refinements
refinementinressources: Dict[str, Dict[str, int]] = {
    "HolzWerkstatt": {"Holz": 2},
    "ErzGiesserei1": {"Erz": 2},
    "ErzGiesserei2": {"Erz": 1},
    "EisenWerkstatt": {"Eisen": 1},
    "RauGoldGoldschmied": {"Rau-Gold": 1},
    "TotemGiesserei": {"Totem": 2},
    "TotemGoldschmied": {"Totem": 1},
    "ElfenbeinGoldschmied": {"Elfenbein": 2},
    "SchafDocks": {"Schaf": 2},
    "SchafWerkstatt": {"Schaf": 1},
    "PergamentDocks": {"Pergament": 2},
    "FeuerwerkJuwelier": {"Feuerwerk": 1},
    "QuarzGoldschmied": {"Quarz": 1},
    "KristallErzJuwelier": {"Kristall-Erz": 1},
    "KristallErzGiesserei": {"Kristall-Erz": 1},
    "JuwelenGoldschmied": {"Juwelen": 1},
    "StahlWerkstatt": {"Stahl": 1}
}

refinementoutressources: Dict[str, Dict[str, List[int]]] = {
    "HolzWerkstatt": {"Werkzeug": [1, 2, 3, 4, 5, 6]},
    "ErzGiesserei1": {"Eisen": [1, 2, 3, 4, 5, 6]},
    "ErzGiesserei2": {"Silber": [4, 5], "Rau-Gold": [6]},
    "EisenWerkstatt": {"Waffen": [1, 2, 3, 4, 5, 6]},
    "RauGoldGoldschmied": {"Fein-Gold": [1, 2, 3, 4, 5, 6]},
    "TotemGiesserei": {"Fein-Gold": [1, 2, 3, 4, 5, 6]},
    "TotemGoldschmied": {"Schmuck": [1, 2, 3, 4, 5, 6]},
    "ElfenbeinGoldschmied": {"Schmuck": [1, 2, 3, 4, 5, 6]},
    "SchafDocks": {"Segel": [1, 2, 3, 4, 5, 6]},
    "SchafWerkstatt": {"Pergament": [1, 2, 3, 4, 5, 6]},
    "PergamentDocks": {"Lektüre": [1, 2, 3, 4, 5, 6]},
    "FeuerwerkJuwelier": {"Lektüre": [3, 4], "Quarz": [5, 6]},
    "QuarzGoldschmied": {"Messgerät": [1, 2, 3, 4, 5, 6]},
    "KristallErzJuwelier": {"Juwelen": [3, 4, 5], "Diamant": [6]},
    "KristallErzGiesserei": {"Stahl": [1, 2, 3, 4, 5, 6]},
    "JuwelenGoldschmied": {"Messgerät": [1, 2, 3, 4, 5, 6]},
    "StahlWerkstatt": {"Panzerung": [1, 2, 3, 4, 5, 6]}
}

refinementgraphics: Dict[str, str] = {
    "HolzWerkstatt": "placeholder.png",
    "ErzGiesserei1": "placeholder.png",
    "ErzGiesserei2": "placeholder.png",
    "EisenWerkstatt": "placeholder.png",
    "RauGoldGoldschmied": "placeholder.png",
    "TotemGiesserei": "placeholder.png",
    "TotemGoldschmied": "placeholder.png",
    "ElfenbeinGoldschmied": "placeholder.png",
    "SchafDocks": "placeholder.png",
    "SchafWerkstatt": "placeholder.png",
    "PergamentDocks": "placeholder.png",
    "FeuerwerkJuwelier": "placeholder.png",
    "QuarzGoldschmied": "placeholder.png",
    "KristallErzJuwelier": "placeholder.png",
    "KristallErzGiesserei": "placeholder.png",
    "JuwelenGoldschmied": "placeholder.png",
    "StahlWerkstatt": "placeholder.png"
}

refinementbyharbour: Dict[str, List[str]] = {
    "Giesserei": ["ErzGiesserei1", "ErzGiesserei2", "TotemGiesserei", "KristallErzGiesserei"],
    "Werkstatt": ["HolzWerkstatt", "EisenWerkstatt", "SchafWerkstatt", "StahlWerkstatt"],
    "Docks": ["SchafDocks", "PergamentDocks"],
    "Juwelier": ["FeuerwerkJuwelier", "KristallErzJuwelier"],
    "Goldschmied": ["RauGoldGoldschmied", "TotemGoldschmied", "ElfenbeinGoldschmied", "QuarzGoldschmied", "JuwelenGoldschmied"]
}

# these are start ressources/ships, the corresponding classes should be created and assigned
# to each player at the beginning of the game
startressources: List[str] = ["Holz", "Erz", "Schaf"]

# these ressources go directly to your home, all other stay in the building
basisressources: List[str] = ["Holz", "Erz", "Schaf"]

startships: List[str] = ["Kogge"]

playercolors: List[str] = ["red", "green", "blue", "yellow"]

buildingtypes: List[str] = ["fort", "settlement", "outpost", "metropole"]

tradingshiptypes: List[str] = ["Kogge", "Brigantine", "Schoner",
                    "Panzerschiff", "Brigg", "Schweres Handels Schiff"]

battleshiptypes: List[str] = ["Kanonenboot", "Kleines Kriegsschiff", "Belagerungs-Schiff",
                   "Fregatte", "Karavelle", "Korvette", "Linienshiff", "Mörserschiff"]

harbourtypes: List[str] = ["Giesserei", "Werkstatt", "Docks", "Juwelier", "Goldschmied"]

harbouramount: Dict[str, int] = {
    "Giesserei": 3,
    "Werkstatt": 3,
    "Docks": 3,
    "Juwelier": 2,
    "Goldschmied": 2
}

baseroundtriggers: Dict[int, List[str]] = {
    0: ["König", "Expeditionen"],
    1: [],
    2: ["Piraten"],
    3: [],
    4: [],
    5: []
}

diceimages: Dict[int, str] = {
    1: "Symb_Würfel1.png",
    2: "Symb_Würfel2.png",
    3: "Symb_Würfel3.png",
    4: "Symb_Würfel4.png",
    5: "Symb_Würfel5.png",
    6: "Symb_Würfel6.png"
}

#TODO W3 (does not exist I believe)????
sciencetypes: Dict[str, str] = {
    "W1": "Windrad",
    "W2": "Nebelmaschine",
    "W4": "Automatisierung",
    "W5": "Feinmechanik",
    "W6": "Seuche",
    "W7": "Genie",
    "W8": "Synthese",
    "W9": "Resilienz",
    "W10": "Kongress",
    "W11": "Plagiat"
}

sciencetier_from_cost: Dict[str, str] = {
    "10": "T1",
    "25": "T2",
    "50": "T3"
}

#TODO lookup table for science type+tier to arguments!
# science_karguments: Dict[str, Dict[str, Dict[str, Any]]] = {
#     "W1": {"T1": {"duration": 2, "tradingship": 1, "fleet": 2, "inheritable": True}, "T2": {"duration": 2, "tradingship": 2, "fleet": 3, "inheritable": True}, "T3": {"duration": 2, "tradingship": 3, "fleet": 4, "inheritable": True}},
#     "W2": {"T1": {"duration": 2, "protection": "partial", "inheritable": False}, "T2": {}, "T3": {}},
#     "W4": {"T1": {}, "T2": {}, "T3": {}},
#     "W5": {"T1": {}, "T2": {}, "T3": {}},
#     "W6": {"T1": {}, "T2": {}, "T3": {}},
#     "W7": {"T1": {}, "T2": {}, "T3": {}},
#     "W8": {"T1": {}, "T2": {}, "T3": {}},
#     "W9": {"T1": {}, "T2": {}, "T3": {}},
#     "W10": {"T1": {}, "T2": {}, "T3": {}},
#     "W11": {"T1": {}, "T2": {}, "T3": {}}
# }

effecttypes: List[str] = ["windrad", "protection", "feinmechanik", "seuche", "defense"]

colordictionary: Dict[str, str] = {
    "red": "#ed3b3b",
    "green": "#25ba1c",
    "blue": "#475ae1",
    "yellow": "#d9d03a",
}

#missions
missiontargets: Dict[int, str] = {
    1 : "Boston",
    2 : "San Salvador",
    3 : "Kairo",
    4 : "Kapstadt",
    5 : "Abidjan",
    6 : "Kapstadt",
    7 : "Tokyo",
    8 : "Rio de Janeiro",
    9 : "San Salvador",
    10 : "Rio de Janeiro",
    11 : "Novgorod",
    12 : "Bombay",
    13 : "Tokyo",
    14 : "Kapstadt",
    15 : "Santiago",
    16 : "Perth",
    17 : "Abidjan",
    18 : "San Salvador",
    19 : "San Diego",
    20 : "Santiago de Chile",
    21 : "Rio de Janeiro",
    22 : "Tokyo",
    23 : "Rio de Janeiro",
    24 : "Novgorod",
    25 : "Novgorod",
    26 : "Kapstadt",
    27 : "Boston",
    28 : "Kairo",
    29 : "Abidjan",
    30 : "Bombay",
    31 : "Rio de Janeiro",
    32 : "Santiago de Chile",
    33 : "Cairns",
    34 : "Boston",
    35 : "San Diego",
    36 : "Bombay",
    37 : "Kairo",
    38 : "Novgorod",
    39 : "Kapstadt",
    40 : "Santiago de Chile",
    41 : "Cairns",
    42 : "Abidjan",
    43 : "San Salvador",
    44 : "Bombay",
    45 : "San Diego",
    46 : "Tokyo",
    47 : "Perth",
    48 : "Rio de Janeiro",
    49 : "Cairns",
    50 : "Boston",
    51 : "San Salvador",
    52 : "San Diego",
    53 : "Tokyo",
    54 : "Abidjan",
    55 : "Kapstadt",
    56 : "Novgorod",
    57 : "Bombay",
    58 : "Perth",
    59 : "Santiago de Chile"
}

missionobjectives: Dict[int, Dict[str, int]] = {
    1 : {"Holz":2},
    2 : {"Holz":3},
    3 : {"Schaf":2},
    4 : {"Kapstadt":2},
    5 : {"Erz":2},
    6 : {"Erz":3},
    7 : {"Erz":2},
    8 : {"Feuerwerk":1},
    9 : {"Feuerwerk":2},
    10 : {"Elfenbein":1},
    11 : {"Elfenbein":1},
    12 : {"Elfenbein":1},
    13 : {"Elfenbein":2},
    14 : {"Kristall-Erz":2},
    15 : {"Kristall-Erz":1},
    16 : {"Elfenbein":1},
    17 : {"Totem":1},
    18 : {"Totem":1},
    19 : {"Totem":2},
    20 : {"Totem":1},
    21 : {"Eisen":1},
    22 : {"Eisen":1},
    23 : {"Segel":1},
    24 : {"Segel":1},
    25 : {"Stahl":1},
    26 : {"Stahl":1},
    27 : {"Werkzeug":1},
    28 : {"Pergament":1},
    29 : {"Pergament":1},
    30 : {"Pergament":1},
    31 : {"Silber":2},
    32 : {"Silber":1},
    33 : {"Silber":1},
    34 : {"Lektüre":1},
    35 : {"Lektüre":1},
    36 : {"Quarz":1},
    37 : {"Kristall":1},
    38 : {"Kristall":1},
    39 : {"Kristall":1},
    40 : {"Kristall":1},
    41 : {"Kristall":1},
    42 : {"Schmuck":1},
    43 : {"Schmuck":1},
    44 : {"Schmuck":1},
    45 : {"Schmuck":1},
    46 : {"Schmuck":1},
    47 : {"Schmuck":2},
    48 : {"Rau-Gold":1},
    49 : {"Rau-Gold":1},
    50 : {"Diamant":1},
    51 : {"Diamant":1},
    52 : {"Diamant":1},
    53 : {"Diamant":1},
    54 : {"Waffen":1},
    55 : {"Panzerung":1},
    56 : {"Fein-Gold":1},
    57 : {"Fein-Gold":1},
    58 : {"Fein-Gold":1},
    59 : {"Messgerät":1}
}

missionrewards: Dict[int, int] = {
    1 : 2,
    2 : 3,
    3 : 2,
    4 : 2,
    5 : 2,
    6 : 3,
    7 : 3,
    8 : 3,
    9 : 4,
    10 : 3,
    11 : 3,
    12 : 4,
    13 : 5,
    14 : 5,
    15 : 4,
    16 : 3,
    17 : 4,
    18 : 4,
    19 : 7,
    20 : 5,
    21 : 4,
    22 : 4,
    23 : 4,
    24 : 3,
    25 : 4,
    26 : 4,
    27 : 4,
    28 : 3,
    29 : 3,
    30 : 3,
    31 : 6,
    32 : 4,
    33 : 4,
    34 : 4,
    35 : 5,
    36 : 7,
    37 : 5,
    38 : 5,
    39 : 6,
    40 : 7,
    41 : 7,
    42 : 6,
    43 : 7,
    44 : 7,
    45 : 8,
    46 : 8,
    47 : 10,
    48 : 6,
    49 : 7,
    50 : 9,
    51 : 9,
    52 : 10,
    53 : 11,
    54 : 8,
    55 : 9,
    56 : 11,
    57 : 12,
    58 : 12,
    59 : 12
}

missionimages: Dict[int, str] = {}
for i in range(1,60):
    missionimages[i] = "Mission"+str(i)+".png"

nActiveMissions = 8