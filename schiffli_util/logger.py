# A very basic logger that saves all printed output (by control) and writes it to file(s) periodically

import datetime
import os
import atexit

from typing import List

#TODO handle multisession eventually
#TODO tune save frequency depending on caused load
#TODO register atexit
class SchiffliLogger:
    def __init__(self, verbosity: int = 3, save_block_size: int = 100) -> None:
        self.verbosity = verbosity
        self.save_block_size = save_block_size
        currtime = datetime.now()
        filename = "schiffli_"+str(currtime.year)+"_"+str(currtime.month)+"_"+str(currtime.day)+"_"+str(currtime.hour)+"_"+str(currtime.minute)+".log"
        log_folder = os.path.join(os.environ["SCHIFFLI_BASE_PATH"], "logging")
        self.output_file = os.path.join(log_folder, filename)
        if not os.path.isdir(log_folder):
            os.makedirs(log_folder)
        self.nblocks: int = 0
        self.log: List[str] = []
        atexit.register(self.writeToFile)

    def logOutput(self, text, level):
        if level > verbosity:
            return
        self.nblocks+=1
        output_text = "[" + str(datetime.datetime.now().isoformat())+"]: " + text
        #TODO how to distinguish levels
        if level == -1: #dev
            self.log.append(output_text)
        elif level == 0: #error
            self.log.append(output_text)
        elif level == 1: #warning
            self.log.append(output_text)
        elif level == 2: #info
            self.log.append(output_text)
        if level == 3: #log
            self.log_log.append(output_text)
        if self.nblocks>=self.save_block_size:
            self.writeToFile()

    def writeToFile(self):
        if os.path.isfile(self.output_file):
            mode = "a"
        else:
            mode = "w"
        with open(self.output_file, mode) as output_file:
            for line in self.log:
                output_file.write(line)
        #TODO safety against atexit double write maybe
        self.log = []
        self.nblocks = 0
