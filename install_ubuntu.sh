PKG_OK=$(dpkg-query -W --showformat='${Status}\n' virtualenv|grep "install ok installed")
echo Checking for virtualenv: $PKG_OK
if [ "" == "$PKG_OK" ]; then
  echo "No virtualenv installation found. Setting up virtualenv."
  sudo apt-get --force-yes --yes install virtualenv
fi

PKG_OK=$(dpkg-query -W --showformat='${Status}\n' python3-tk|grep "install ok installed")
echo Checking for python3-tk: $PKG_OK
if [ "" == "$PKG_OK" ]; then
  echo "No tkinter installation found. Setting up tkinter."
  sudo apt-get --force-yes --yes install python3-tk
fi

mkdir ../virtualenv
virtualenv ../virtualenv --python=/usr/bin/python3
echo "export PYTHONPATH=\"$PYTHONPATH:$PWD\"" >> ../virtualenv/bin/activate
echo "export SCHIFFLI_BASE_PATH=\"$PWD\"" >> ../virtualenv/bin/activate

source ./../virtualenv/bin/activate

pip install numpy pillow Shapely argcomplete django channels pyOpenSSL djangorestframework channels_redis screeninfo pyyaml
activate-global-python-argcomplete --user

source /etc/profile
source ./../virtualenv/bin/activate